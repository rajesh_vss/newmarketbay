//
//  DBConnection.swift
//  MarketBay
//
//  Created by Apple on 19/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import FMDB

let sharedInstance = DBConnection()

class DBConnection: NSObject
{
    var database: FMDatabase? = nil
    
    var ObjGeneral:General =  General()
    
    // MARK: - General
    
    class var instance: DBConnection
    {
        let Util : DBUtil = DBUtil()
        
        let isInserted =  Util.checkDBExists(Util.getPath("MarketBay.sqlite"))
        
        if(isInserted)
        {
            sharedInstance.database = FMDatabase(path: Util.getPath("MarketBay.sqlite"))
        }
        else
        {
            Util.createDatabase()
            sharedInstance.database = FMDatabase(path: Util.getPath("MarketBay.sqlite"))
        }
        
        return sharedInstance
    }
    
    
    func CurrentDateTime() -> String
    {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        
        let dataString : String =  NSUserDefaults.standardUserDefaults().stringForKey("TimeDiff")!;
        
        let doub : Double = (dataString as NSString).doubleValue
        
        let currentDateTime : NSDate = NSDate().dateByAddingTimeInterval(doub)
        
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        
        let strCurrentDate : String = dateFormatter.stringFromDate(currentDateTime)
        
        return strCurrentDate
    }
    
    // MARK: - SyncSettings Table
    
    func GetLastSyncDateTime(strStype: String) -> String
    {
        var resultString : String = ""
        
        let query : String = "select SyncTime from SyncSettings where TableName = '" + strStype + "'"
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery(query, withArgumentsInArray: nil)
        
        if (resultSet != nil && resultSet.next())
        {
            resultString = resultSet.stringForColumn("SyncTime")
        }
        
        sharedInstance.database!.close()
        
        return resultString;
    }
    
    
    func UpdateCurrentSyncTime(strStype: String, PrimayrKey: Int)
    {
        let currentdate : String = CurrentDateTime()
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO SyncSettings (ID, SyncTime, TableName) values(?,?,?)", withArgumentsInArray:[PrimayrKey, currentdate, strStype])
        sharedInstance.database!.close()
    } 
    
    func DeleteRecordsFromAllTables()
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("DELETE FROM SyncSettings; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.executeUpdate("DELETE FROM UserInRoles; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.executeUpdate("DELETE FROM EntityType; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.executeUpdate("DELETE FROM Entities; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.executeUpdate("DELETE FROM PropertyTypes; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.executeUpdate("DELETE FROM PropertyVarieties; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.executeUpdate("DELETE FROM Containers; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.executeUpdate("DELETE FROM DocLines; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.executeUpdate("DELETE FROM ProdSizeCount; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.executeUpdate("DELETE FROM TransPost; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.executeUpdate("DELETE FROM SystemMessage; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.executeUpdate("DELETE FROM SignalRStatus; VACUUM;", withArgumentsInArray:nil)
        sharedInstance.database!.close()
    }
    
     // MARK: - UserInRoles
    
    func insertUserInRoles(uRole: UserRole)
    {
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO UserInRoles(RoleId, RoleName, EntityId, LicenceExpiredDate) values (?,?,?,?)", withArgumentsInArray: [uRole.RoleId, uRole.RoleName, uRole.EntityId, uRole.LicenceExpiredDate])
    }
    
    func getBuyerList() -> [UserRole]
    {
        var arrOfRecords : [UserRole] = [UserRole]()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select UR.*, E.MarketCode as MarketCode from UserInRoles UR Left join Entities E on UR.EntityId = E.EntityId where Date(UR.LicenceExpiredDate) > datetime() group by E.EntityId", withArgumentsInArray:nil)
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                let objRole : UserRole = UserRole()
                
                
                objRole.EntityId = resultSet.longForColumn("EntityId")
                
                objRole.RoleId = resultSet.stringForColumn("RoleId") != nil ? resultSet.stringForColumn("RoleId") : ""
                
                objRole.RoleName = resultSet.stringForColumn("RoleName") != nil ? resultSet.stringForColumn("RoleName") : ""
                
                objRole.MarketCode = resultSet.stringForColumn("MarketCode") != nil ? resultSet.stringForColumn("MarketCode") : ""
                
                arrOfRecords.append(objRole)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords
    }
    
    // MARK: - EntityType
    
    func insertEntityType(entityType: EntityType)
    {
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO EntityType(EntityTypeId, EntityType1) values (?,?)", withArgumentsInArray: [entityType.EntityTypeId, entityType.EntityType1 ])
    }
    
    func getSupplierDetails(supplierId: Int) -> Entities
    {
        let objEntities : Entities = Entities()
        
        sharedInstance.database!.open()
        
        let strQuery = "select * from Entities Where EntityId = " + String(supplierId) + " order by MarketCode ASC"
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery(strQuery, withArgumentsInArray: nil)
        
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                objEntities.EntityId = resultSet.longForColumn("EntityId")
                
                objEntities.EntityTypeID = resultSet.longForColumn("EntityTypeID")
                
                objEntities.EntityType = resultSet.stringForColumn("EntityType") as String
                
                objEntities.Name1 = resultSet.stringForColumn("Name1") as String
                
                objEntities.Name2 = resultSet.stringForColumn("Name2") as String
                
                objEntities.MarketCode = resultSet.stringForColumn("MarketCode") as String
            }
        }
        
        sharedInstance.database!.close()
        
        return objEntities;
    }
    
    func getAllSupplier() -> [Entities]
    {
        var arrOfRecords : [Entities] = [Entities]()
        
        
        sharedInstance.database!.open()
        //  sharedInstance.database?.beginTransaction()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from Entities Where EntityTypeID in (3,1,4) order by MarketCode ASC", withArgumentsInArray: nil)
        
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                let objEntities : Entities = Entities()
                
                objEntities.EntityId = resultSet.longForColumn("EntityId")
                
                objEntities.EntityTypeID = resultSet.longForColumn("EntityTypeID")
                
                objEntities.EntityType = resultSet.stringForColumn("EntityType") as String
                
                objEntities.Name1 = resultSet.stringForColumn("Name1") as String
                
                objEntities.Name2 = resultSet.stringForColumn("Name2") as String
                
                objEntities.MarketCode = resultSet.stringForColumn("MarketCode") as String
                
                objEntities.IsActive = resultSet.stringForColumn("IsActive") as String
                
                objEntities.RebatePercentage = resultSet.doubleForColumn("RebatePercentage")
                
                objEntities.HandlingFeePer = resultSet.doubleForColumn("HandlingFeePer")
                
                objEntities.Representative = resultSet.longForColumn("Representative")
                
                arrOfRecords .append(objEntities)
            }
        }
        
        // sharedInstance.database?.commit()
        sharedInstance.database!.close()
        
        return arrOfRecords;
    }
    
    func getSupplier(strFilter: String) -> [Entities]
    {
        print(strFilter)
        
        var arrOfRecords : [Entities] = [Entities]()
        
        sharedInstance.database!.open()
        
        let strQuery = "select * from Entities Where  MarketCode LIKE '%" + strFilter + "%' and EntityTypeID = 3  order by MarketCode ASC"
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery(strQuery, withArgumentsInArray: nil)
        
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                let objEntities : Entities = Entities()
                
                // , , , , , ,
                
                objEntities.EntityId = resultSet.longForColumn("EntityId")
                
                objEntities.EntityTypeID = resultSet.longForColumn("EntityTypeID")
                
                objEntities.EntityType = resultSet.stringForColumn("EntityType") as String
                
                objEntities.Name1 = resultSet.stringForColumn("Name1") as String
                
                objEntities.Name2 = resultSet.stringForColumn("Name2") as String
                
                objEntities.MarketCode = resultSet.stringForColumn("MarketCode") as String
                
                objEntities.IsActive = resultSet.stringForColumn("IsActive") as String
                
                objEntities.RebatePercentage = resultSet.doubleForColumn("RebatePercentage")
                
                objEntities.HandlingFeePer = resultSet.doubleForColumn("HandlingFeePer")
                
                objEntities.Representative = resultSet.longForColumn("Representative")
                
                arrOfRecords .append(objEntities)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords;
    }
    
    func truncateEntityType()
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeStatements("DELETE FROM EntityType; VACUUM;")
        sharedInstance.database!.close()
    }
    
    // MARK: - Entities
    
    func insertEntities(entity: Entities)
    {
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO Entities (ClientId, EntityId, EntityTypeID, EntityType, Name1, Name2, MarketCode, IsActive, RepId, LastModified, RebatePercentage, HandlingFeePer,Representative,ClientEntity) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsInArray: [entity.ClientId, entity.EntityId, entity.EntityTypeID, entity.EntityType, entity.Name1, entity.Name2, entity.MarketCode, entity.IsActive, entity.RepId, entity.LastModified, entity.RebatePercentage, entity.HandlingFeePer, entity.Representative, entity.ClientEntity])
    }
    
    func truncateEntities()
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeStatements("DELETE FROM Entities; VACUUM;")
        sharedInstance.database!.close()
    }
    
    // MARK: - Product Type
 
    func insertPrdouctType(pType : ProductType)
    {
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO PropertyTypes (ProdTypeId, ProdTypeDesc, ProdTypeShortName, ClassId, CreatedBy, LastModified) values (?,?,?,?,?,?)", withArgumentsInArray: [pType.ProdTypeId, pType.ProdTypeDesc, pType.ProdTypeShortName, pType.ClassId, pType.CreatedBy, pType.LastModified])
    }
    
    func getAllProductType() -> [ProductType]
    {
        var arrOfRecords : [ProductType] = [ProductType]()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from PropertyTypes order by ProdTypeShortName ASC", withArgumentsInArray: nil)
        
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                let objProductType : ProductType = ProductType()
                
                objProductType.ProdTypeId = resultSet.longForColumn("ProdTypeId")
                
                objProductType.ProdTypeDesc = resultSet.stringForColumn("ProdTypeDesc") as String
                
                objProductType.ProdTypeShortName = resultSet.stringForColumn("ProdTypeShortName") as String
                
                objProductType.ClassId = resultSet.longForColumn("ClassId")
                
                arrOfRecords .append(objProductType)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords
    }
    
    // MARK: - Product Varieties
    
    func insertPropertyVarieties(pType : ProductVarieties)
    {
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO PropertyVarieties (VarietyId, VarietyDesc, ProdTypeId, EntityId, EntityReference, EntityGroup, CtnCid, UnitQty, UnitCid, TI, HI, PalletQty, GTIN, PackDate_Days, UseByDate_Days, ProdStandardId, CreatedBy, LastModified, ProdStandardCode) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsInArray: [pType.VarietyId, pType.VarietyDesc, pType.ProdTypeId, pType.EntityId, pType.EntityReference, pType.EntityGroup, pType.CtnCid, pType.UnitQty, pType.UnitCid, pType.TI, pType.HI, pType.PalletQty, pType.GTIN, pType.PackDate_Days, pType.UseByDate_Days, pType.ProdStandardId, pType.CreatedBy, pType.LastModified, pType.ProdStandardCode])
    }
    
    func getProductVarieties(prodTypeId: Int) -> [ProductVarieties]
    {
        var arrOfRecords : [ProductVarieties] = [ProductVarieties]()
        
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from PropertyVarieties Where ProdTypeId = ? order by VarietyDesc ASC", withArgumentsInArray: [prodTypeId])
        
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                let objProductVarieties : ProductVarieties = ProductVarieties()
                
                objProductVarieties.VarietyId = resultSet.longForColumn("VarietyId")
                
                objProductVarieties.EntityGroup = resultSet.stringForColumn("EntityGroup") as String
                
                objProductVarieties.PalletQty = resultSet.longForColumn("UseByDate_Days")
                
                objProductVarieties.ProdTypeId = resultSet.longForColumn("ProdTypeId")
                
                objProductVarieties.EntityReference = resultSet.stringForColumn("EntityReference") as String
                
                objProductVarieties.VarietyDesc = resultSet.stringForColumn("VarietyDesc")  as String
                
                objProductVarieties.HI = resultSet.longForColumn("HI")
                
                objProductVarieties.ProdStandardId = resultSet.longForColumn("ProdStandardId")
                
                objProductVarieties.ProdStandardCode = resultSet.stringForColumn("ProdStandardCode") as String
                
                objProductVarieties.CtnCid = resultSet.longForColumn("CtnCid")
                
                objProductVarieties.UnitCid = resultSet.longForColumn("UnitCid")
                
                objProductVarieties.UnitQty = Double(resultSet.stringForColumn("UnitQty"))!
                
                objProductVarieties.TI = resultSet.longForColumn("TI")
                
                objProductVarieties.GTIN = resultSet.stringForColumn("GTIN")  as String
                
                objProductVarieties.PackDate_Days = resultSet.stringForColumn("PackDate_Days") as String
                
                
                arrOfRecords .append(objProductVarieties)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords;
    }
    
    // MARK: - Containers
    
    func insertContainers(continers : Containers)
    {
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO Containers (Cid, CidDesc, Deposit, CreatedBy, LastModified) values (?,?,?,?,?)", withArgumentsInArray: [continers.Cid, continers.CidDesc, continers.Deposit, continers.CreatedBy, continers.LastModified])
    }
    
    func getAllContainers() -> [Containers]
    {
        var arrOfRecords : [Containers] = [Containers]()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from Containers order by CidDesc ASC", withArgumentsInArray: nil)
        
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                let objContainers : Containers = Containers()
                
                objContainers.Cid = resultSet.longForColumn("Cid")
                
                objContainers.CidDesc = resultSet.stringForColumn("CidDesc") as String
                
                arrOfRecords .append(objContainers)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords;
    }
    
    func getContainerbyId(cId: String) -> Containers!
    {
        let objContainers : Containers = Containers()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from Containers Where Cid = ?  order by CidDesc ASC", withArgumentsInArray: [cId])
        
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                objContainers.Cid = resultSet.longForColumn("Cid")
                
                objContainers.CidDesc = resultSet.stringForColumn("CidDesc") as String
            }
        }
        
        sharedInstance.database!.close()
        
        return objContainers
    }
    
    // MARK: - Document Type
    
    func insertDocType(objDT : DocType)
    {
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO DocType (DocTypeId, DocTypeDesc, XML_DocType, XSL_Stylesheet, isActive, IsCheckbox, LastModified) values (?,?,?,?,?,?,?)", withArgumentsInArray: [objDT.DocTypeId, objDT.DocTypeDesc, objDT.XML_DocType, objDT.XSL_Stylesheet, objDT.isActive, objDT.IsCheckbox, objDT.LastModified])
    }
    
    func getDocTypeById(DocTypeId : Int) -> DocType
    {
        let objDT : DocType = DocType()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from DocType where DocTypeId = ? order by DocTypeDesc ASC", withArgumentsInArray: [DocTypeId])
        
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                objDT.DocTypeId = resultSet.longForColumn("DocTypeId")
                
                objDT.DocTypeDesc = resultSet.stringForColumn("DocTypeDesc") as String
            }
        }
        
        sharedInstance.database!.close()
        
        return objDT;
    }
    
    func getAllDocType() -> [DocType]
    {
        var arrOfRecords : [DocType] = [DocType]()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from DocType order by DocTypeDesc ASC", withArgumentsInArray: nil)
        
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                let objDT : DocType = DocType()
                
                objDT.DocTypeId = resultSet.longForColumn("DocTypeId")
                
                objDT.DocTypeDesc = resultSet.stringForColumn("DocTypeDesc") as String
                
                arrOfRecords .append(objDT)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords;
    }
    
    func FilterDocType(filter : String) -> [DocType]
    {
        var arrOfRecords : [DocType] = [DocType]()
        
        sharedInstance.database!.open()
        
        let queryString : String = "select * from DocType where DocTypeDesc Like '%" + filter + "%' order by DocTypeDesc ASC"
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery(queryString, withArgumentsInArray: nil)
        
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                let objDT : DocType = DocType()
                
                objDT.DocTypeId = resultSet.longForColumn("DocTypeId")
                
                objDT.DocTypeDesc = resultSet.stringForColumn("DocTypeDesc") as String
                
                arrOfRecords .append(objDT)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords;
    }
    
    // MARK: - DocLines
    
    func insertDocLines(docline : DocLines)
    {
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO DocLines (LineId, DocId, DocTypeId, ProdTypeId, VarietyId, SupplierRef, DeliveryRef, FreeformDesc, CountSize, Grade, Colour, BuyCtnQty, BuyCtnCid, BuyUnitQty, BuyUnitCid, SellPrice, SellPriceCid, SupplierID, EntityReference, BuyPrice, BuyPriceCid, SellCtnQty, SellCtnCid, SellUnitQty, SellUnitCid, PackDate, UseByDate, GrowerId, IndustryRegionId, Status, GTIN, TI, HI, PalletQty, ParentDocLineId, CreatedBy, LastModified, OwnerId, locStatusId, OrderId, FreeformBuyCtn, FreeformBuyUnit, FreeformPriceUnit) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsInArray: [docline.LineId, docline.DocId, docline.DocTypeId, docline.ProdTypeId, docline.VarietyId, docline.SupplierRef, docline.DeliveryRef, docline.FreeformDesc, docline.CountSize, docline.Grade, docline.Colour, docline.BuyCtnQty, docline.BuyCtnCid, docline.BuyUnitQty, docline.BuyUnitCid, docline.SellPrice, docline.SellPriceCid, docline.SupplierID, docline.EntityReference, docline.BuyPrice, docline.BuyPriceCid, docline.SellCtnQty, docline.SellCtnCid, docline.SellUnitQty, docline.SellUnitCid, docline.PackDate, docline.UseByDate, docline.GrowerId, docline.IndustryRegionId, docline.Status, docline.GTIN, docline.TI, docline.HI, docline.PalletQty, docline.ParentDocLineId, docline.CreatedBy, docline.LastModified, docline.OwnerId, docline.Status == true ? 4 : 0, docline.OrderId, docline.FreeformBuyCtn, docline.FreeformBuyUnit, docline.FreeformPriceUnit])
    }
    
    func insertDocLinesInLocal(docline : DocLines)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO DocLines (LineId, DocId, DocTypeId, ProdTypeId, VarietyId, SupplierRef, DeliveryRef, FreeformDesc, CountSize, Grade, Colour, BuyCtnQty, BuyCtnCid, BuyUnitQty, BuyUnitCid, SellPrice, SellPriceCid, SupplierID, EntityReference, BuyPrice, BuyPriceCid, SellCtnQty, SellCtnCid, SellUnitQty, SellUnitCid, PackDate, UseByDate, GrowerId, IndustryRegionId, Status, GTIN, TI, HI, PalletQty, ParentDocLineId, CreatedBy, LastModified, OwnerId, locStatusId, OrderId, FreeformBuyCtn, FreeformBuyUnit, FreeformPriceUnit) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsInArray: [docline.LineId, docline.DocId, docline.DocTypeId, docline.ProdTypeId, docline.VarietyId, docline.SupplierRef, docline.DeliveryRef, docline.FreeformDesc, docline.CountSize, docline.Grade, docline.Colour, docline.BuyCtnQty, docline.BuyCtnCid, docline.BuyUnitQty, docline.BuyUnitCid, docline.SellPrice, docline.SellPriceCid, docline.SupplierID, docline.EntityReference, docline.BuyPrice, docline.BuyPriceCid, docline.SellCtnQty, docline.SellCtnCid, docline.SellUnitQty, docline.SellUnitCid, docline.PackDate, docline.UseByDate, docline.GrowerId, docline.IndustryRegionId, docline.Status, docline.GTIN, docline.TI, docline.HI, docline.PalletQty, docline.ParentDocLineId, docline.CreatedBy, docline.LastModified, docline.OwnerId, docline.Status == true ? 4 : 0, docline.OrderId, docline.FreeformBuyCtn, docline.FreeformBuyUnit, docline.FreeformPriceUnit])
        sharedInstance.database!.close()
    }
    
    
    func updateDocLinesLineId(LineId : Int, LineSId : Int, DocId: Int, OrderId: Int, sOrderId: Int)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("UPDATE DocLines SET LineId = ?, DocId =?, OrderId =?, isUpdatedToOnline = 0 Where LineId = ?",  withArgumentsInArray: [LineSId, DocId, sOrderId, LineId])
        // Update SLR Status
        sharedInstance.database!.executeUpdate("UPDATE SignalRStatus SET DLineID = ? Where DLineID = ?",  withArgumentsInArray: [sOrderId, OrderId])
        sharedInstance.database!.close()
    }
    
    func getDocLinesDetails(DocId : Int, SupplierID : Int) -> [DocLines]
    {
        var arrOfRecords : [DocLines] = [DocLines]()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select E.Name1 as SupplierName, E.MarketCode as MarketCode, PT.ProdTypeDesc as ProdTypeName, PV.VarietyDesc as ProdVarityName, CU.CidDesc as BuyCtnQtyUnit, CC.CidDesc as BuyCtnUnit, CP.CidDesc as BuyPriceUnit, DL.* from TransPost TP left join DocLines DL on TP.LineId = DL.LineId Left join Entities E on TP.SupplierId = E.EntityId Left join PropertyTypes PT on DL.ProdTypeId = PT.ProdTypeId Left join PropertyVarieties PV on DL.VarietyId = PV.VarietyId Left join containers CU on DL.BuyCtnCid = CU.Cid Left join containers CC on DL.BuyUnitCid = CC.Cid Left join containers CP on DL.BuyPriceCid = CP.Cid where TP.DocId = ? AND TP.SupplierId = ? AND TP.IsActive = 1 order by TP.CreateTime DESC", withArgumentsInArray: [DocId, SupplierID])
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                let objDoc : DocLines = DocLines()
                
                objDoc.LineId = resultSet.longForColumn("LineId")
                
                objDoc.DocId = resultSet.longForColumn("DocId")
                
                objDoc.DocTypeId = resultSet.longForColumn("DocTypeId")
                
                objDoc.ProdTypeId = resultSet.longForColumn("ProdTypeId")
                
                objDoc.VarietyId = resultSet.longForColumn("VarietyId")
                
                objDoc.SupplierRef = resultSet.stringForColumn("SupplierRef")
                
                objDoc.DeliveryRef = resultSet.stringForColumn("DeliveryRef")
                
                objDoc.FreeformDesc = resultSet.stringForColumn("FreeformDesc")
                
                objDoc.CountSize = resultSet.longForColumn("CountSize")
                
                objDoc.Grade = resultSet.stringForColumn("Grade")
                
                objDoc.Colour = resultSet.stringForColumn("Colour")
                
                objDoc.BuyCtnQty = resultSet.longForColumn("BuyCtnQty")
                objDoc.BuyCtnCid = resultSet.longForColumn("BuyCtnCid")
                objDoc.BuyUnitQty = resultSet.longForColumn("BuyUnitQty")
                objDoc.BuyUnitCid = resultSet.longForColumn("BuyUnitCid")
                objDoc.BuyPrice = Double(resultSet.stringForColumn("BuyPrice"))!
                objDoc.BuyPriceCid = resultSet.longForColumn("BuyPriceCid")
                
                objDoc.ProdTypeName = resultSet.stringForColumn("ProdTypeName")
                objDoc.ProdVarityName = resultSet.stringForColumn("ProdVarityName")
                objDoc.SupplierName = resultSet.stringForColumn("SupplierName")
                objDoc.MarketCode = resultSet.stringForColumn("MarketCode")
                
                objDoc.BuyCtnQtyUnit = resultSet.stringForColumn("BuyCtnQtyUnit") != nil ? resultSet.stringForColumn("BuyCtnQtyUnit") : ""
                objDoc.BuyCtnUnit = resultSet.stringForColumn("BuyCtnUnit") != nil ? resultSet.stringForColumn("BuyCtnUnit") : ""
                objDoc.BuyPriceUnit = resultSet.stringForColumn("BuyPriceUnit") != nil ? resultSet.stringForColumn("BuyPriceUnit") : ""
                
                objDoc.locStatusId = resultSet.longForColumn("locStatusId")

                objDoc.OrderId = resultSet.longForColumn("OrderId")
                
                objDoc.FreeformBuyCtn = resultSet.stringForColumn("FreeformBuyCtn") != nil ? resultSet.stringForColumn("FreeformBuyCtn") : ""
                
                objDoc.FreeformBuyUnit = resultSet.stringForColumn("FreeformBuyUnit") != nil ? resultSet.stringForColumn("FreeformBuyUnit") : ""
                
                objDoc.FreeformPriceUnit = resultSet.stringForColumn("FreeformPriceUnit") != nil ? resultSet.stringForColumn("FreeformPriceUnit") : ""
                
                arrOfRecords .append(objDoc)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords;
    }
    
    
    func getDocLinesDetailsToUploadServer(LineId : Int) -> DocLines
    {
         let objDoc : DocLines = DocLines()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * From DocLines Where LineId = ?", withArgumentsInArray: [LineId])
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                objDoc.LineId = resultSet.longForColumn("LineId")
                
                objDoc.DocId = resultSet.longForColumn("DocId")
                
                objDoc.DocTypeId = resultSet.longForColumn("DocTypeId")
                
                objDoc.ProdTypeId = resultSet.longForColumn("ProdTypeId")
                
                objDoc.VarietyId = resultSet.longForColumn("VarietyId")
                
                objDoc.SupplierRef = resultSet.stringForColumn("SupplierRef")
                
                objDoc.DeliveryRef = resultSet.stringForColumn("DeliveryRef")
                
                objDoc.FreeformDesc = resultSet.stringForColumn("FreeformDesc")
                
                objDoc.CountSize = resultSet.longForColumn("CountSize")
                
                objDoc.Grade = resultSet.stringForColumn("Grade")
                
                objDoc.Colour = resultSet.stringForColumn("Colour")
                
                objDoc.BuyCtnQty = resultSet.longForColumn("BuyCtnQty")
                objDoc.BuyCtnCid = resultSet.longForColumn("BuyCtnCid")
                objDoc.BuyUnitQty = resultSet.longForColumn("BuyUnitQty")
                objDoc.BuyUnitCid = resultSet.longForColumn("BuyUnitCid")
                objDoc.BuyPrice = Double(resultSet.stringForColumn("BuyPrice"))!
                objDoc.BuyPriceCid = resultSet.longForColumn("BuyPriceCid")
                
                objDoc.SellCtnQty = resultSet.longForColumn("SellCtnQty")
                objDoc.SellCtnCid = resultSet.longForColumn("SellCtnCid")
                objDoc.SellUnitQty = resultSet.longForColumn("SellUnitQty")
                objDoc.SellUnitCid = resultSet.longForColumn("SellUnitCid")
                objDoc.SellPrice = resultSet.longForColumn("SellPrice")
                objDoc.SellPriceCid = resultSet.longForColumn("SellPriceCid")
                
                objDoc.SupplierID = resultSet.longForColumn("SupplierID")
                objDoc.EntityReference = resultSet.stringForColumn("EntityReference")
                
                objDoc.PackDate = (resultSet.stringForColumn("PackDate")  as String ) != "" ? ObjGeneral.ConvertDateFromLocalDB2App(resultSet.stringForColumn("PackDate"))  : ""
                objDoc.UseByDate = (resultSet.stringForColumn("UseByDate")  as String ) != "" ? ObjGeneral.ConvertDateFromLocalDB2App(resultSet.stringForColumn("UseByDate"))  : ""
                objDoc.GrowerId = resultSet.longForColumn("GrowerId")
                objDoc.IndustryRegionId = resultSet.longForColumn("IndustryRegionId")
                objDoc.Status = (resultSet.stringForColumn("Status") as NSString).boolValue
                objDoc.GTIN = resultSet.stringForColumn("GTIN")
                objDoc.TI = resultSet.longForColumn("TI")
                objDoc.HI = resultSet.longForColumn("HI")
                objDoc.PalletQty = resultSet.longForColumn("PalletQty")
                objDoc.ParentDocLineId = resultSet.longForColumn("ParentDocLineId")
                objDoc.CreatedBy = resultSet.stringForColumn("CreatedBy")
                objDoc.LastModified = (resultSet.stringForColumn("LastModified")  as String ) != "" ? ObjGeneral.ConvertDateFromLocalDB2App(resultSet.stringForColumn("LastModified"))  : ""
                objDoc.OwnerId = resultSet.longForColumn("SupplierID")
                objDoc.IsActive = resultSet.stringForColumn("IsActive") != nil ? (resultSet.stringForColumn("IsActive") as NSString).boolValue : false
                
                objDoc.OrderId = resultSet.longForColumn("OrderId")
                
                objDoc.FreeformBuyCtn = resultSet.stringForColumn("FreeformBuyCtn")
                
                objDoc.FreeformBuyUnit = resultSet.stringForColumn("FreeformBuyUnit")
                
                objDoc.FreeformPriceUnit = resultSet.stringForColumn("FreeformPriceUnit")
                
            }
        }
        
        sharedInstance.database!.close()
        
        return objDoc;
    }
    
    
    
    func updateDocLineStatus(StatusId : Int, LineId : Int, bStatus : Int)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("Update DocLines Set locStatusId = ?, Status = ? Where OrderId = ?", withArgumentsInArray: [StatusId, String(bStatus), LineId])
        sharedInstance.database!.close()
    }
    
    func updateDocLineDetails(objDocline : DocLines)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("Update DocLines Set DeliveryRef = ?, SupplierRef = ?, BuyCtnQty = ?, BuyUnitQty = ?, BuyCtnCid = ?, BuyUnitCid = ?, BuyPrice = ?, BuyPriceCid = ?, isUpdatedToOnline = ?, CountSize = ?, Grade = ?, Colour = ?, FreeformBuyCtn = ?, FreeformBuyUnit = ?, FreeformPriceUnit = ?, FreeformDesc = ? Where LineId = ?", withArgumentsInArray: [objDocline.DeliveryRef, objDocline.SupplierRef, objDocline.BuyCtnQty, objDocline.BuyUnitQty, objDocline.BuyCtnCid, objDocline.BuyUnitCid, objDocline.BuyPrice, objDocline.BuyPriceCid, objDocline.isUpdatedToOnline, objDocline.CountSize, objDocline.Grade, objDocline.Colour, objDocline.FreeformBuyCtn, objDocline.FreeformBuyUnit, objDocline.FreeformPriceUnit, objDocline.FreeformDesc, objDocline.LineId])
        sharedInstance.database!.close()
    }
    
    func deleteDocLine(LineId : Int)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("Update DocLines Set IsActive = 0 Where LineId = ?", withArgumentsInArray: [LineId])
        sharedInstance.database!.executeUpdate("Update TransPost Set IsActive = 0 Where LineId = ?", withArgumentsInArray: [LineId])
        sharedInstance.database!.close()
    }
    
    func getOrderDocLineId()  -> Int
    {
        var arrOfRecords : [DocLines] = [DocLines]()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from DocLines Where LineId < 0  order by LineId ASC", withArgumentsInArray: nil)
        
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                let objTP : DocLines = DocLines()
                
                objTP.LineId = resultSet.longForColumn("LineId")
                
                arrOfRecords .append(objTP)
            }
        }
        
        sharedInstance.database!.close()
        
        if(arrOfRecords.count > 0)
        {
            return (arrOfRecords[0].LineId  - 1)
        }
        else
        {
            return -1
        }
    }
    
    // MARK: - TransPost
    
    func insertTransPost(transPost : TransPost)
    {
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO TransPost (TransPostId, SupplierId, LineId, DocTypeId, DocId, CreateTime, SendTime, RespondTime, Flag, Status, DocSendId, CreatedBy, LastModified, BuyerId, isPublished, isSendToSupplier, isUpdatedToOnline) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsInArray: [transPost.TransPostId, transPost.SupplierId, transPost.LineId, transPost.DocTypeId, transPost.DocId, transPost.CreateTime, transPost.SendTime, transPost.RespondTime, transPost.Flag, transPost.Status, transPost.DocSendId, transPost.CreatedBy, transPost.LastModified, transPost.BuyerId, transPost.isPublished, transPost.isSendToSupplier, transPost.isUpdatedToOnline])
    }
    
    func insertTransPostInLocal(transPost : TransPost)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO TransPost (TransPostId, SupplierId, LineId, DocTypeId, DocId, CreateTime, SendTime, RespondTime, Flag, Status, DocSendId, CreatedBy, LastModified, BuyerId, isPublished, isSendToSupplier, isUpdatedToOnline) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsInArray: [transPost.TransPostId, transPost.SupplierId, transPost.LineId, transPost.DocTypeId, transPost.DocId, transPost.CreateTime, transPost.SendTime, transPost.RespondTime, transPost.Flag, transPost.Status, transPost.DocSendId, transPost.CreatedBy,transPost.LastModified, transPost.BuyerId, transPost.isPublished, transPost.isSendToSupplier, transPost.isUpdatedToOnline])
         sharedInstance.database!.close()
    }
    
    func updateTransPostDetails(LineId : Int)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("UPDATE TransPost SET isUpdatedToOnline = 1 where LineId = ?", withArgumentsInArray: [LineId])
        sharedInstance.database!.close()
    }
    
    func updateTransPostId(TransPostId: Int, TransPostSId: Int, DocId: Int, DocSId : Int, LineId : Int)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("UPDATE TransPost SET TransPostId = ?, DocId = ?, LineId =? where TransPostId = ?", withArgumentsInArray: [TransPostSId, DocSId, LineId, TransPostId, DocId])
        sharedInstance.database!.close()
    }
    
    func updateTransPostsSendToSupplier(LineId : Int, isPublished : Int, isSendToSupplier : Int)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("UPDATE TransPost SET isPublished = ?, isSendToSupplier = ?, isUpdatedToOnline = 0 Where LineId = ?",  withArgumentsInArray: [isPublished, isSendToSupplier, LineId])
        sharedInstance.database!.close()
    }
    
    func updateTransPostsToSaveDocument(objData : SaveDocument)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("UPDATE TransPost SET isPublished = ?, isSendToSupplier = ?, isUpdatedToOnline = ? Where SupplierId = ? AND DocId = ? AND isSendToSupplier != 1",  withArgumentsInArray: [objData.isPublished, objData.isSendToSupplier, objData.isUpdatedToOnline, objData.SupplierID, objData.DocID])
        sharedInstance.database!.close()
    }
    
    func getTransPostDetails(BuyerId: Int) -> [TransPost]
    {
        var arrOfRecords : [TransPost] = [TransPost]()
        
        sharedInstance.database!.open()
        
//        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("Select CreateTime, TP.SupplierId, E.MarketCode as SupplierMarketCode, TP.DocId, count(TP.LineId) as lineCount, min(CAST(DL.Status as integer)) as locStatusId from TransPost TP inner join DocLines DL on TP.LineId = DL.LineId Left join Entities E on TP.SupplierId = E.EntityId where TP.BuyerId = ? AND TP.SupplierId != 0 AND TP.IsActive = 1 group by TP.DocId, TP.SupplierId order by TP.CreateTime", withArgumentsInArray: [BuyerId])

//        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("Select CreateTime, TP.SupplierId, E.MarketCode as SupplierMarketCode, TP.DocId, count(TP.LineId) as lineCount, min(CAST(DL.locStatusId as integer)) as locStatusId from TransPost TP inner join DocLines DL on TP.LineId = DL.LineId Left join Entities E on TP.SupplierId = E.EntityId where TP.BuyerId = ? AND TP.SupplierId != 0 AND TP.IsActive = 1 group by TP.DocId, TP.SupplierId order by TP.CreateTime", withArgumentsInArray: [BuyerId])
        
        
        let defaults = NSUserDefaults.standardUserDefaults()
        var subQuery : String = ""
        
        
        if defaults.objectForKey("LifeUnfilledOrders") != nil
        {
            let days :String = defaults.objectForKey("LifeUnfilledOrders") as! String
            
            if days != ""
            {
                let dateFormatter = NSDateFormatter()
                dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
                dateFormatter.dateFormat = "yyyy-MM-dd"
                
                let currentDate : NSDate = NSDate()
                
                let numberofdays : Double = Double(days)!
                
                let endDate : NSDate = currentDate.dateByAddingTimeInterval(60*60*24*numberofdays)
                
                let strCurrentDate : String = dateFormatter.stringFromDate(currentDate)
                let strEndDate : String = dateFormatter.stringFromDate(endDate)
                
                subQuery = "AND (Date(TP.CreateTime) BETWEEN Date('" + strCurrentDate + "') AND Date('" + strEndDate + "'))"
            }
        }
        
        
        let query = "Select CreateTime, TP.SupplierId, E.MarketCode as SupplierMarketCode, TP.DocId, count(TP.LineId) as lineCount, min(CAST(DL.locStatusId as integer)) as locStatusId from TransPost TP inner join DocLines DL on TP.LineId = DL.LineId Left join Entities E on TP.SupplierId = E.EntityId where TP.BuyerId = " + String(BuyerId) + " AND TP.SupplierId != 0 AND TP.IsActive = 1 " + subQuery + " group by TP.DocId, TP.SupplierId order by TP.CreateTime"
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery(query, withArgumentsInArray: nil)
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                let objTrans : TransPost = TransPost()
                
                objTrans.CreateTime = resultSet.stringForColumn("CreateTime") as String
                
                objTrans.SupplierId = resultSet.longForColumn("SupplierId")
                
                objTrans.SupplierMarketCode = resultSet.stringForColumn("SupplierMarketCode") != nil ? resultSet.stringForColumn("SupplierMarketCode") as String : ""
                
                objTrans.DocId = resultSet.longForColumn("DocId")
                
                objTrans.LineCount = resultSet.longForColumn("lineCount")
                
                objTrans.locStatusId = resultSet.longForColumn("locStatusId")
                
                arrOfRecords .append(objTrans)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords;
    }
    
    func getTransPostDetailsToUploadServer(DocId: Int, SupplerId: Int) -> [TransPost]
    {
        var arrOfRecords : [TransPost] = [TransPost]()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from TransPost where DocId = ? AND SupplierId = ? AND (isSendToSupplier = 0 OR isPublished = 0)", withArgumentsInArray: [DocId, SupplerId])
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                let objTrans : TransPost = TransPost()
                
                objTrans.TransPostId = resultSet.longForColumn("TransPostId")
                objTrans.SupplierId = resultSet.longForColumn("SupplierId")
                objTrans.LineId = resultSet.longForColumn("LineId")
                objTrans.DocTypeId = resultSet.longForColumn("DocTypeId")
                objTrans.DocId = resultSet.longForColumn("DocId")
                objTrans.CreateTime = (resultSet.stringForColumn("CreateTime")  as String ) != "" ? ObjGeneral.ConvertDateFromLocalDB2App(resultSet.stringForColumn("CreateTime"))  : ""
                objTrans.SendTime = (resultSet.stringForColumn("SendTime")  as String ) != "" ? ObjGeneral.ConvertDateFromLocalDB2App(resultSet.stringForColumn("SendTime"))  : ""
                objTrans.RespondTime = (resultSet.stringForColumn("RespondTime")  as String ) != "" ? ObjGeneral.ConvertDateFromLocalDB2App(resultSet.stringForColumn("RespondTime"))  : ""
                objTrans.Flag = resultSet.stringForColumn("Flag")
                objTrans.Status = (resultSet.stringForColumn("Status") as NSString).boolValue
                objTrans.DocSendId = resultSet.longForColumn("DocSendId")
                objTrans.CreatedBy = resultSet.stringForColumn("CreatedBy")
                objTrans.LastModified = (resultSet.stringForColumn("LastModified")  as String ) != "" ? ObjGeneral.ConvertDateFromLocalDB2App(resultSet.stringForColumn("LastModified"))  : ""
                objTrans.BuyerId = resultSet.longForColumn("BuyerId")
                
                arrOfRecords .append(objTrans)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords;
    }
    
    func getTransPostToUploadServer() -> [TransPost]
    {
        var arrOfRecords : [TransPost] = [TransPost]()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select TP.* from DocLines DL Left join TransPost TP on DL.LineId = TP.LineId Where TP.TransPostId < 0 OR DL.isUpdatedToOnline = 1", withArgumentsInArray: nil)
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                let objTrans : TransPost = TransPost()
                
                objTrans.TransPostId = resultSet.longForColumn("TransPostId")
                objTrans.SupplierId = resultSet.longForColumn("SupplierId")
                objTrans.LineId = resultSet.longForColumn("LineId")
                objTrans.DocTypeId = resultSet.longForColumn("DocTypeId")
                objTrans.DocId = resultSet.longForColumn("DocId")
                objTrans.CreateTime = (resultSet.stringForColumn("CreateTime")  as String ) != "" ? ObjGeneral.ConvertDateFromLocalDB2App(resultSet.stringForColumn("CreateTime"))  : ""
                objTrans.SendTime = (resultSet.stringForColumn("SendTime")  as String ) != "" ? ObjGeneral.ConvertDateFromLocalDB2App(resultSet.stringForColumn("SendTime"))  : ""
                objTrans.RespondTime = (resultSet.stringForColumn("RespondTime")  as String ) != "" ? ObjGeneral.ConvertDateFromLocalDB2App(resultSet.stringForColumn("RespondTime"))  : ""
                objTrans.Flag = resultSet.stringForColumn("Flag")
                objTrans.Status = (resultSet.stringForColumn("Status") as NSString).boolValue
                objTrans.DocSendId = resultSet.longForColumn("DocSendId")
                objTrans.CreatedBy = resultSet.stringForColumn("CreatedBy")
                objTrans.LastModified = (resultSet.stringForColumn("LastModified")  as String ) != "" ? ObjGeneral.ConvertDateFromLocalDB2App(resultSet.stringForColumn("LastModified"))  : ""
                objTrans.BuyerId = resultSet.longForColumn("BuyerId")
                
                arrOfRecords .append(objTrans)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords;
    }
    
    func getOrderDocId()  -> Int
    {
        
        var arrOfRecords : [TransPost] = [TransPost]()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from TransPost Where DocId < 0  order by DocId ASC", withArgumentsInArray: nil)
        
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                let objTP : TransPost = TransPost()
                
                objTP.TransPostId = resultSet.longForColumn("TransPostId")
                
                objTP.DocId = resultSet.longForColumn("DocId")
                
                objTP.LineId = resultSet.longForColumn("LineId")
                
                arrOfRecords .append(objTP)
            }
        }
        
        sharedInstance.database!.close()
        
        if(arrOfRecords.count > 0)
        {
            return (arrOfRecords[0].DocId  - 1)
        }
        else
        {
            return -1
        }
    }
    
    func getOrderTransPostId()  -> Int
    {
        
        var arrOfRecords : [TransPost] = [TransPost]()
        
        sharedInstance.database!.open()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from TransPost Where TransPostId < 0  order by TransPostId ASC", withArgumentsInArray: nil)
        
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                let objTP : TransPost = TransPost()
                
                objTP.TransPostId = resultSet.longForColumn("TransPostId")
                
                objTP.DocId = resultSet.longForColumn("DocId")
                
                objTP.LineId = resultSet.longForColumn("LineId")
                
                arrOfRecords .append(objTP)
            }
        }
        
        sharedInstance.database!.close()
        
        if(arrOfRecords.count > 0)
        {
            return (arrOfRecords[0].TransPostId  - 1)
        }
        else
        {
            return -1
        }
    }
    
    
    func getTransPostStatus(DocId: Int, SupplierId: Int) -> SaveDocument
    {
        sharedInstance.database!.open()
        
        let objSD : SaveDocument = SaveDocument()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select TP.DocId, TP.SupplierId, min(TP.isPublished) as isPublished, min(TP.isSendToSupplier) as isSendToSupplier, min(DL.locStatusId) as locStatusId from TransPost TP Left join DocLines DL on TP.LineId = DL.LineId where TP.DocId = ? and TP.SupplierId = ? group by TP.DocId", withArgumentsInArray: [DocId, SupplierId])
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                objSD.SupplierID = resultSet.longForColumn("SupplierID")
                objSD.DocID = resultSet.longForColumn("DocID")
                objSD.isPublished = resultSet.longForColumn("isPublished")
                objSD.isSendToSupplier = resultSet.longForColumn("isSendToSupplier")
                objSD.locStatusId = resultSet.longForColumn("locStatusId")
            }
        }
        
        sharedInstance.database!.close()
        
        return objSD
    }
    
    // MARK: - SaveDocument
    
    func getSaveDocumentDetailsToUploadServer() -> [SaveDocument]
    {
        sharedInstance.database!.open()
        
        var arrOfRecords : [SaveDocument] = [SaveDocument]()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select TP.DocId, TP.SupplierId, min(TP.isPublished) as isPublished, min(TP.isSendToSupplier) as isSendToSupplier, min(DL.locStatusId) as locStatusId, Count(TP.LineId), max(TP.isUpdatedToOnline) from TransPost TP Left join DocLines DL on TP.LineId = DL.LineId group by TP.DocId having min(DL.locStatusId) = 4 and max(TP.isUpdatedToOnline) = 1", withArgumentsInArray: nil)
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                let objSD : SaveDocument = SaveDocument()
                
                objSD.SupplierID = resultSet.longForColumn("SupplierID")
                objSD.DocID = resultSet.longForColumn("DocID")
                objSD.isPublished = resultSet.longForColumn("isPublished")
                objSD.isSendToSupplier = resultSet.longForColumn("isSendToSupplier")
                
                arrOfRecords.append(objSD)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfRecords
    }
    
    // MARK: - SizeCount
    
    func insertProdSizeCount(pSizeCount : ProdSizeCount)
    {
        sharedInstance.database!.executeUpdate("INSERT OR REPLACE INTO ProdSizeCount (ProdSizeCountId, ProdTypeId, SizeCount, IsActive, Cid, ContainerCode, CreatedBy, LastModified) values (?,?,?,?,?,?,?,?)", withArgumentsInArray: [pSizeCount.ProdSizeCountId, pSizeCount.ProdTypeId, pSizeCount.SizeCount, pSizeCount.IsActive, pSizeCount.Cid, pSizeCount.ContainerCode, pSizeCount.CreatedBy, pSizeCount.LastModified])
    }
    
    func getPSCContiners(ProdTypeId : Int) -> [ProdSizeCount]
    {
        sharedInstance.database!.open()
        
        var arrOfSizeCount : [ProdSizeCount] = [ProdSizeCount]()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from ProdSizeCount where ProdTypeId = ? Group by ContainerCode", withArgumentsInArray: [ProdTypeId])
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                let objSD : ProdSizeCount = ProdSizeCount()
                
                objSD.ProdSizeCountId = resultSet.stringForColumn("ProdSizeCountId")
                objSD.ProdTypeId = resultSet.longForColumn("ProdTypeId")
                objSD.SizeCount = resultSet.longForColumn("SizeCount")
                objSD.Cid = resultSet.longForColumn("Cid")
                objSD.ContainerCode = resultSet.stringForColumn("ContainerCode")
                
                arrOfSizeCount.append(objSD)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfSizeCount
    }
    
    func getProductSizeCount(ProdTypeId : Int, cid : Int) -> [ProdSizeCount]
    {
        sharedInstance.database!.open()
        
        var arrOfSizeCount : [ProdSizeCount] = [ProdSizeCount]()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from ProdSizeCount where ProdTypeId = ? AND Cid = ? Order by SizeCount ASC", withArgumentsInArray: [ProdTypeId, cid])
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                let objSD : ProdSizeCount = ProdSizeCount()
                
                objSD.ProdSizeCountId = resultSet.stringForColumn("ProdSizeCountId")
                objSD.ProdTypeId = resultSet.longForColumn("ProdTypeId")
                objSD.SizeCount = resultSet.longForColumn("SizeCount")
                objSD.Cid = resultSet.longForColumn("Cid")
                objSD.ContainerCode = resultSet.stringForColumn("ContainerCode")
                
                arrOfSizeCount.append(objSD)
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfSizeCount
    }
    
    
    func insertSystemMessage(objDetails: SystemMessage)
    {
        sharedInstance.database!.executeUpdate("insert or replace into SystemMessage (MessageNo, ModuleName, SystemMessageType, SystemMessageDetails, Selection1, Selection2, Selection3) values (?,?,?,?,?,?,?)", withArgumentsInArray: [objDetails.MessageNo, objDetails.ModuleName, objDetails.SystemMessageType, objDetails.SystemMessageDetails, objDetails.Selection1, objDetails.Selection2, objDetails.Selection3])
    }
    
    func getMessageUsingId(Id: Int) -> SystemMessage
    {
        sharedInstance.database!.open()
        
        let message : SystemMessage = SystemMessage()
        
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select * from SystemMessage where MessageNo = ?", withArgumentsInArray: [Id])
        
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                message.SystemMessageDetails = resultSet.stringForColumn("SystemMessageDetails")
                message.SystemMessageType = resultSet.stringForColumn("SystemMessageType")
            }
        }
        
        sharedInstance.database!.close()
        
        return message
    }
    
    // MARK: - SignalRStatus
    
    func insertSignalRStatus(DLineId: Int)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("insert or replace into SignalRStatus (DLineID) values (?)", withArgumentsInArray: [DLineId])
        sharedInstance.database!.close()
    }
    
    func updateSignalRStatus(DLineId: Int, LogId : Int, Status : Int)
    {
        sharedInstance.database!.open()
        sharedInstance.database!.executeUpdate("Update SignalRStatus Set LogId = ?, SLRStatus = ? where DLineID = ?", withArgumentsInArray: [LogId, Status, DLineId])
        sharedInstance.database!.close()
    }
    
    func getLineIdFromOrder(OrderId : Int) -> Int
    {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select LineId From DocLines Where OrderId = ?", withArgumentsInArray: [OrderId])
        
        var lineId : Int = 0
        
        if (resultSet != nil)
        {
            while(resultSet.next())
            {
                 lineId = resultSet.longForColumn("LineId")
            }
        }
        
        sharedInstance.database!.close()
        
        return lineId
    }
     
    
    func getSignalRStatusToUpdateServer() -> [DocLines]
    {
        sharedInstance.database!.open()
        
        var arrOfDocLines : [DocLines] = [DocLines]()
        
//        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select DL.* From SignalRStatus SS Left join DocLines DL on SS.DLineID = DL.LineId Where SLRStatus = 0", withArgumentsInArray: nil)

        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("select DL.*, E.MarketCode as SupplierMarketCode, E.RepId as SupplierRepId, CQ.CidDesc as BuyCtnUnits, CC.CidDesc as BuyUnitUnits, CP.CidDesc as BuyPriceUnits, PT.ProdTypeDesc as ProdTypeDesc, PV.VarietyDesc as VarietyDesc from SignalRStatus SS Left join DocLines DL on SS.DLineID = DL.OrderId Left join Entities E on DL.SupplierID = E.EntityId Left join Containers CQ on DL.BuyCtnCid = CQ.Cid Left join Containers CC on DL.BuyUnitCid = CC.Cid Left join Containers CP on DL.BuyPriceCid = CP.Cid Left join PropertyTypes PT on DL.ProdTypeId = PT.ProdTypeId Left join PropertyVarieties PV on DL.VarietyId = PV.VarietyId Where SLRStatus = 0 AND SS.DLineID > 0", withArgumentsInArray: nil)
        
        if (resultSet != nil)
        {
            
            while(resultSet.next())
            {
                let objDoc : DocLines = DocLines()
                
                objDoc.LineId = resultSet.longForColumn("LineId")
                
                objDoc.DocId = resultSet.longForColumn("DocId")
                objDoc.OrderId = resultSet.longForColumn("OrderId")
                
                objDoc.ProdTypeId = resultSet.longForColumn("ProdTypeId")
                objDoc.ProdTypeDesc = resultSet.stringForColumn("ProdTypeDesc") != nil ? resultSet.stringForColumn("ProdTypeDesc") : ""
                
                objDoc.FreeformDesc = resultSet.stringForColumn("FreeformDesc")
                objDoc.FreeformBuyCtn = resultSet.stringForColumn("FreeformBuyCtn")
                objDoc.FreeformBuyUnit = resultSet.stringForColumn("FreeformBuyUnit")
                objDoc.FreeformPriceUnit = resultSet.stringForColumn("FreeformPriceUnit")
                
                objDoc.VarietyId = resultSet.longForColumn("VarietyId")
                objDoc.VarietyDesc = resultSet.stringForColumn("VarietyDesc")
                
                objDoc.BuyCtnQty = resultSet.longForColumn("BuyCtnQty")
                objDoc.BuyCtnCid = resultSet.longForColumn("BuyCtnCid")
                objDoc.BuyCtnUnits = resultSet.stringForColumn("BuyCtnUnits") != nil ? resultSet.stringForColumn("BuyCtnUnits") : ""
                
                objDoc.BuyUnitQty = resultSet.longForColumn("BuyUnitQty")
                objDoc.BuyUnitCid = resultSet.longForColumn("BuyUnitCid")
                objDoc.BuyUnitUnits = resultSet.stringForColumn("BuyCtnUnits") != nil ? resultSet.stringForColumn("BuyCtnUnits") : ""
                
                objDoc.BuyPrice = Double(resultSet.stringForColumn("BuyPrice"))!
                objDoc.BuyPriceCid = resultSet.longForColumn("BuyPriceCid")
                objDoc.BuyPriceUnits = resultSet.stringForColumn("BuyCtnUnits") != nil ? resultSet.stringForColumn("BuyCtnUnits") : ""
                
                objDoc.SupplierRef = resultSet.stringForColumn("SupplierRef")
                objDoc.Colour = resultSet.stringForColumn("Colour")
                objDoc.Grade = resultSet.stringForColumn("Grade")
                objDoc.CountSize = resultSet.longForColumn("CountSize")
                 
                objDoc.SupplierMarketCode = resultSet.stringForColumn("SupplierMarketCode")
                objDoc.SupplierRepId = resultSet.stringForColumn("SupplierRepId")
                
                arrOfDocLines.append(objDoc)
                
            }
        }
        
        sharedInstance.database!.close()
        
        return arrOfDocLines
    }
}





