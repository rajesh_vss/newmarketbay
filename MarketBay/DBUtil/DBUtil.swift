//
//  DBUtil.swift
//  MarketBay
//
//  Created by Apple on 19/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import FMDB

class DBUtil: NSObject
{
    var database: FMDatabase? = nil
    
    func getPath(fileName: String) -> String
    {
        let str = NSURL( fileURLWithPath: NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0])
        
        return str.URLByAppendingPathComponent(String(fileName)).path!
    }
    
    func checkDBExists(dbPath: String) -> Bool
    {
        let fileManager = NSFileManager.defaultManager()
        
        if !fileManager.fileExistsAtPath(dbPath)
        {
            return false;
        }
        
        return true;
    }
    
    func createDatabase()
    {
        let dbPath: String = getPath("MarketBay.sqlite")
        
        print(dbPath, terminator: "")
        
        if(checkDBExists(dbPath))
        {
            database = FMDatabase(path: dbPath as String)
        }
        else
        {
            database = FMDatabase(path: dbPath as String)
            CreateTables()
        }
    }
    
    func CreateTables()
    {
        database!.open()
        
        database!.executeStatements("CREATE TABLE [SyncSettings] (ID integer primary key, TableName text, SyncTime text)")
        
        database!.executeStatements("CREATE TABLE [UserInRoles] (RoleId text, RoleName text, EntityId integer, LicenceExpiredDate DateTime)")
        
        database!.executeStatements("CREATE TABLE [EntityType] (EntityTypeId integer primary key, EntityType1 text)")
        
        database!.executeStatements("CREATE TABLE [Entities] (ClientId integer, EntityId integer primary key, EntityTypeID integer, EntityType text, Name1 text, Name2 text, MarketCode text, IsActive text, RepId integer, LastModified DateTime, RebatePercentage NUMERIC, HandlingFeePer NUMERIC, Representative integer, ClientEntity integer)")
        
        database!.executeStatements("CREATE TABLE [PropertyTypes] (ProdTypeId integer, ProdTypeDesc text, ProdTypeShortName text, ClassId integer, CreatedBy text, LastModified DateTime)")
        
        database!.executeStatements("CREATE TABLE [PropertyVarieties] (VarietyId integer primary key, VarietyDesc text, ProdTypeId integer, EntityId integer, EntityReference text, EntityGroup text, CtnCid integer, UnitQty text, UnitCid integer, TI integer, HI integer, PalletQty integer, GTIN text, PackDate_Days DateTime, UseByDate_Days DateTime, ProdStandardId integer, CreatedBy text, LastModified DateTime, ProdStandardCode text)")
        
        database!.executeStatements("CREATE TABLE [Containers] (Cid integer primary key, CidDesc text, Deposit text, CreatedBy text, LastModified DateTime)")
        
        database!.executeStatements("CREATE TABLE [DocType] (DocTypeId integer primary key, DocTypeDesc text, XML_DocType text, XSL_Stylesheet text, isActive text, IsCheckbox text,  LastModified DateTime)")
        
        database!.executeStatements("CREATE TABLE [DocLines] (LineId integer primary key, DocId integer, DocTypeId integer, ProdTypeId integer, VarietyId integer, SupplierRef text, DeliveryRef text, FreeformDesc text, CountSize integer, Grade text, Colour text, BuyCtnQty integer, BuyCtnCid integer, BuyUnitQty integer, BuyUnitCid integer, SellPrice text, SellPriceCid integer, SupplierID integer, EntityReference, BuyPrice text, BuyPriceCid integer, SellCtnQty text, SellCtnCid integer, SellUnitQty integer, SellUnitCid integer, PackDate DateTime, UseByDate DateTime, GrowerId integer, IndustryRegionId integer, Status text, GTIN integer, TI integer, HI integer, PalletQty integer, ParentDocLineId integer, CreatedBy text, LastModified DateTime, OwnerId integer, locStatusId integer DEFAULT 0, IsActive text DEFAULT 1, isUpdatedToOnline integer DEFAULT 0, OrderId integer, FreeformBuyCtn text, FreeformBuyUnit text, FreeformPriceUnit text)")
        
        database!.executeStatements("CREATE TABLE [TransPost] (TransPostId integer primary key, SupplierId integer, LineId integer, DocTypeId integer, DocId integer, CreateTime DateTime, SendTime DateTime, RespondTime DateTime, Flag text, Status text, DocSendId integer, CreatedBy text, LastModified DateTime, BuyerId integer, IsActive text DEFAULT 1, isPublished integer DEFAULT -1, isSendToSupplier integer DEFAULT -1, isUpdatedToOnline integer DEFAULT 0)")
        
        database!.executeStatements("CREATE TABLE [ProdSizeCount] (ProdSizeCountId text, ProdTypeId integer, SizeCount integer, IsActive text, Cid integer, ContainerCode text, CreatedBy text, LastModified text)")
        
        database!.executeStatements("CREATE TABLE [SystemMessage] (MessageNo integer primary key, ModuleName text, SystemMessageType text, SystemMessageDetails text, Selection1 text, Selection2 text, Selection3 text)")
        
        database!.executeStatements("CREATE TABLE [SignalRStatus] (statusID integer primary key AUTOINCREMENT, DLineID integer, LogId integer DEFAULT 0, SLRStatus integer DEFAULT 0)")
        // DLineID -> consider as OrderId (DocLines)
        database!.close()
    }
}
