//
//  FilterViewController.swift
//  MarketBay
//
//  Created by Apple on 18/10/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {

    @IBOutlet weak var txtDays: customTextField!
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Filter"
        
        let backButton: UIButton = UIButton(type: UIButtonType.Custom)
        backButton.setImage(UIImage(named: "icBackArrow"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: #selector(self.BackButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        backButton.frame = CGRectMake(0, 0, 50, 40)
        backButton.contentHorizontalAlignment = .Left
        let barbackButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = barbackButton

        if defaults.objectForKey("LifeUnfilledOrders") != nil
        {
            txtDays.text = defaults.objectForKey("LifeUnfilledOrders") as? String
        }
    }
    
    func BackButtonPressed()
    {
            self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func btnSave_click(sender: AnyObject)
    {
        defaults.setObject(txtDays.text!, forKey: "LifeUnfilledOrders")
        defaults.synchronize()
        txtDays.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
