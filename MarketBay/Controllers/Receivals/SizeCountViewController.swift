//
//  SizeCountViewController.swift
//  MarketBay
//
//  Created by Apple on 23/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class SizeCountViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIGestureRecognizerDelegate
{

    let dbConn : DBConnection = DBConnection.instance
    
    var arrPSCContainers : [ProdSizeCount] = [ProdSizeCount]()
    
    var arrProdSizeCount : [ProdSizeCount] = [ProdSizeCount]()
    
    var prodTypeId : Int = 0
    
    var selectedContainerId : Int = 0
    
    var pickerView: UIPickerView!
    
    var mytoolbar: UIToolbar!
    
    @IBOutlet weak var vwSizeCount: UIView!
    
    @IBOutlet weak var txtContainers: UITextField!
    
    @IBOutlet weak var txtSizeCount: UITextField!
    
    @IBOutlet weak var txtCustomSizeCount: UITextField!
    
    @IBOutlet weak var vwSCHeightCons: NSLayoutConstraint!
    
    @IBOutlet weak var vwMVHeightCons: NSLayoutConstraint!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.title = "ORDER DETAIL"
        
        let backButton: UIButton = UIButton(type: UIButtonType.Custom)
        backButton.setImage(UIImage(named: "icBackArrow"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: #selector(self.BackButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        backButton.frame = CGRectMake(0, 0, 50, 40)
        backButton.contentHorizontalAlignment = .Left
        let barbackButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = barbackButton
        
        vwMVHeightCons.constant = self.view.frame.height - 64
        
        txtCustomSizeCount.autocorrectionType = .No
        
        getPSCContainers()
    }

     // MARK: - Functions
    
    func BackButtonPressed()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func getPSCContainers()
    {
        arrPSCContainers = dbConn.getPSCContiners(prodTypeId)
        
        if arrPSCContainers.count > 0
        {
            vwSizeCount.hidden = false
            vwSCHeightCons.constant = 69
            vwSizeCount.layoutIfNeeded()
        }
        else
        {
            vwSizeCount.hidden = true
            vwSCHeightCons.constant = 0
            vwSizeCount.layoutIfNeeded()
        }
    }
    
    func getProductSizeCount()
    {
        arrProdSizeCount = dbConn.getProductSizeCount(prodTypeId, cid: selectedContainerId)
    }
    
    func createinputpicker()
    {
        pickerView = UIPickerView()
        mytoolbar = UIToolbar(frame: CGRectMake(0,0,320,44))
        
        pickerView.delegate = self
        pickerView.showsSelectionIndicator = true
        pickerView.dataSource = self
        
        let labPickerTitle  = UILabel(frame: CGRectMake(10,12,200,20))
        
        labPickerTitle.backgroundColor = UIColor.clearColor()
        labPickerTitle.font = UIFont(name: "Ubuntu", size: 14)
        labPickerTitle.textColor = UIColor.blackColor()
        
        labPickerTitle.text = "Units"
        
        mytoolbar.addSubview(labPickerTitle)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.Cancelled))
        cancelButton.setTitleTextAttributes(GlobalConstants.barButtonAttribute, forState: UIControlState.Normal)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.Selected))
        doneButton.setTitleTextAttributes(GlobalConstants.barButtonAttribute, forState: UIControlState.Normal)
        
        mytoolbar.setItems([spaceButton, cancelButton, doneButton], animated: true)
        
        if(txtContainers.isFirstResponder())
        {
            txtContainers.inputView = pickerView;
            txtContainers.inputAccessoryView = mytoolbar;
        }
        else if(txtSizeCount.isFirstResponder())
        {
            txtSizeCount.inputView = pickerView;
            txtSizeCount.inputAccessoryView = mytoolbar;
        }
        
        let pickerGR = UITapGestureRecognizer(target: self, action: #selector(self.selectByTouch))
        pickerGR.delegate = self;
        pickerView.addGestureRecognizer(pickerGR);
        
    }
    
    func showAlertWindow(title: String, message: String, tag: Int)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        if tag == 1
        {
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler:  { (action: UIAlertAction!) in
                self.txtContainers.becomeFirstResponder()
            }))
        }
        
        self.presentViewController(alertController, animated: true, completion:nil)
    }
    
    // MARK: - Events
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func selectByTouch() {
        Selected();
    }
    
    func Cancelled()
    {
        if txtContainers.isFirstResponder()
        {
            txtContainers.resignFirstResponder()
        }
        else if txtSizeCount.isFirstResponder()
        {
            txtSizeCount.resignFirstResponder()
        }
        else if txtCustomSizeCount.isFirstResponder()
        {
            txtCustomSizeCount.resignFirstResponder()
        }
    }
    
    func Selected()
    {
        if txtContainers.isFirstResponder()
        {
            if arrPSCContainers.count > 0
            {
                let details : ProdSizeCount = arrPSCContainers[pickerView.selectedRowInComponent(0)]
                txtContainers.text = details.ContainerCode
                selectedContainerId = details.Cid
                getProductSizeCount()
                
                txtSizeCount.text = ""
            }
            
            txtContainers.resignFirstResponder()
        }
        else if txtSizeCount.isFirstResponder()
        {
            let details : ProdSizeCount = arrProdSizeCount[pickerView.selectedRowInComponent(0)]
            txtSizeCount.text = String(details.SizeCount)
            txtSizeCount.resignFirstResponder()
        }
    }
    
    @IBAction func txtContainers_EditDidBegin(sender: AnyObject)
    {
        createinputpicker()
    }
    
    @IBAction func txtSizeCount_EditDidBegin(sender: AnyObject)
    {
        if selectedContainerId != 0
        {
            createinputpicker()
        }
        else
        {
            let sysMsg : SystemMessage = dbConn.getMessageUsingId(205)
            showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 1)
        }
    }
    
    @IBAction func btnDone_Click(sender: AnyObject)
    {
        let parentViewController = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2]
        
        if parentViewController!.isKindOfClass(CreateOrderViewController) {
            print("Yes")
            
            let parentViewController1 : CreateOrderViewController = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2] as! CreateOrderViewController
            parentViewController1.fillSizeCount((txtCustomSizeCount.text != "" ? txtCustomSizeCount.text : txtSizeCount.text)!)
            
            self.navigationController?.popViewControllerAnimated(true)
        }
        else
        {
            print("No")
        }
    }
    
    // MARK: - Pickerview Delegate
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if txtContainers.isFirstResponder()
        {
            return arrPSCContainers.count
        }
        else
        {
            return arrProdSizeCount.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if(txtContainers.isFirstResponder())
        {
            let details : ProdSizeCount = arrPSCContainers[row]
            return details.ContainerCode
        }
        else
        {
            let details : ProdSizeCount = arrProdSizeCount[row]
            return String(details.SizeCount)
        }
    }
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return false
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if(textField == txtContainers || textField == txtSizeCount)
        {
            return false
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
