//
//  SupplierViewController.swift
//  MarketBay
//
//  Created by Apple on 06/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import MRProgress

class SupplierViewController: UIViewController
{

    @IBOutlet weak var tvSuppliverView: UITableView!
    @IBOutlet weak var txtSearchView: UITextField!
    @IBOutlet weak var segSelectType: UISegmentedControl!
    
    let getData: DBConnection = DBConnection()
    
    var strPageTilte = "NEW ORDER"
    
    var arrSupplier:[Entities] = [Entities]()
    
    var selectedSupplierType : Int = 1
    
    let sections:Array<AnyObject> = ["0", "1",  "2",  "3",  "4",  "5",  "6",  "7",  "8",  "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    
    var filteredArrSupplierlist:NSMutableArray = NSMutableArray()
    
    var filteredArrofletters:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let backButton: UIButton = UIButton(type: UIButtonType.Custom)
        backButton.setImage(UIImage(named: "icBackArrow"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: #selector(self.BackButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        backButton.frame = CGRectMake(0, 0, 50, 40)
        backButton.contentHorizontalAlignment = .Left
        let barbackButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = barbackButton
        
        
        self.title = strPageTilte
        
        loadSupplierCodes()
    }
    
    
    // MARK: - Function
    
    func BackButtonPressed()
    {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func loadSupplierCodes()
    {
        
        ShowProgress()
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            // do your task
            
            self.arrSupplier.removeAll();
            self.arrSupplier = [Entities]()
            
            self.arrSupplier =  self.getData.getAllSupplier()
            
            self.groupSupplier()
            
            dispatch_async(dispatch_get_main_queue()) {
                // update some UI
                self.hideProgress()
                self.tvSuppliverView.reloadData()
            }
        }
        
    }
    
//    func loadSupplierCodes(filterString: String)
//    {
//        ShowProgress()
//        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
//            // do your task
//            
//            self.arrSupplier =  self.getData.getSupplier(filterString)
//            
//            self.groupSupplier()
//            
//            dispatch_async(dispatch_get_main_queue()) {
//                // update some UI
//                
//                self.tvSuppliverView.reloadData()
//            }
//        }
//    }
    
    func loadSupplierCodes(strText:String)
    {
        let arr: NSMutableArray  =  NSMutableArray(array: arrSupplier)
        
        filteredArrSupplierlist = NSMutableArray()
        filteredArrofletters = NSMutableArray()
        
        if strText != ""
        {
            let preRef : NSPredicate = NSPredicate(format: "(MarketCode beginswith[c] %@) And (EntityTypeID = %d)", strText, selectedSupplierType);
            let arrFilter : NSArray = arr.filteredArrayUsingPredicate(preRef)
            
            if(arrFilter.count > 0)
            {
                for contacts in sections
                {
                    let preRef1 : NSPredicate = NSPredicate(format: "MarketCode beginswith[c] %@", contacts as! String);
                    let groupnames : NSArray = arrFilter.filteredArrayUsingPredicate(preRef1)
                    
                    if(groupnames.count > 0)
                    {
                        filteredArrSupplierlist.addObject(groupnames)
                        filteredArrofletters.addObject(contacts)
                    }
                }
            }
        }
        else
        {
            groupSupplier()
        }
        
        self.tvSuppliverView.reloadData()
    }
    
    func groupSupplier()
    {
        let arr: NSMutableArray = NSMutableArray(array: arrSupplier)
        
        filteredArrSupplierlist = NSMutableArray()
        filteredArrofletters = NSMutableArray()
        
        for contacts in sections
        {
            let preRef1 : NSPredicate = NSPredicate(format: "(MarketCode beginswith[c] %@) And (EntityTypeID = %d)", contacts as! String,  selectedSupplierType);
            let groupnames : NSArray = arr.filteredArrayUsingPredicate(preRef1)
            
            if(groupnames.count > 0)
            {
                filteredArrSupplierlist.addObject(groupnames)
                filteredArrofletters.addObject(contacts)
            }
        }
    }
    
    func ShowProgress()
    {
        let progressView = MRProgressOverlayView.showOverlayAddedTo(self.navigationController!.view, animated: true)
        
        progressView.mode = .IndeterminateSmall
        
        progressView.titleLabelText = "Loading ...";
        
        progressView.titleLabel.textColor = GlobalConstants.progressTitleColor
        
        progressView.tintColor = GlobalConstants.progressTitleColor
    }
    
    func hideProgress()
    {
        MRProgressOverlayView.dismissAllOverlaysForView(self.navigationController!.view, animated: true)
    }

    @IBAction func txtSupplier_EditChanged(sender: AnyObject)
    {
//        if(txtSearchView.text == "")
//        {
//            loadSupplierCodes()
//        }
//        else
//        {
            loadSupplierCodes(String(txtSearchView.text!))
//        }
    }
    
    @IBAction func segSelectType_ValueChanged(sender: AnyObject)
    {
        switch segSelectType.selectedSegmentIndex
        {
        case 0:
            selectedSupplierType = 1 // Agents
            break;
        case 1:
            selectedSupplierType = 3 // Suppliers
            break;
        case 2:
            selectedSupplierType = 4 // Wholesalers
            break;
        default:
            selectedSupplierType = 1
            break;
        }
        
        groupSupplier()
        
        self.tvSuppliverView.reloadData()
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return filteredArrSupplierlist.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return filteredArrSupplierlist[section].count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
        
        let arrEntity = filteredArrSupplierlist[indexPath.section] as! NSArray
        
        let entity = arrEntity[indexPath.row] as! Entities
        
        cell.textLabel?.text = String(entity.MarketCode).uppercaseString
        
        cell.textLabel?.font = UIFont(name: "Ubuntu", size: 13.0)
        cell.textLabel?.textColor = UIColor(red: 52/255, green: 52/255, blue: 52/255, alpha: 1.0)
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let arrEntity = filteredArrSupplierlist[indexPath.section] as! NSArray
        let entity = arrEntity[indexPath.row] as! Entities
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
            let saleEntryViewController = storyboard.instantiateViewControllerWithIdentifier("vcDocTypeID") as! DocTypeViewController
            saleEntryViewController.selectedSupplier = entity
            self.navigationController?.pushViewController(saleEntryViewController, animated: true)
        
    }
    
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [AnyObject]!{
        
        return self.filteredArrofletters as [AnyObject]
    }
    
    func tableView(tableView: UITableView,
                   sectionForSectionIndexTitle title: String,
                                               atIndex index: Int) -> Int{
        
        return index
    }
    
    // MARK: - UITextField Delegate
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
