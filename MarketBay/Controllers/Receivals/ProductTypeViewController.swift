//
//  ProductTypeViewController.swift
//  MarketBay
//
//  Created by Apple on 31/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import MRProgress

class ProductTypeViewController: UIViewController {

    @IBOutlet weak var tvPrdouctType: UITableView!
    
    @IBOutlet weak var txtSearchBox: UITextField!
    
    let getData: DBConnection = DBConnection()
    
    var arrProdType:[ProductType] = [ProductType]()
    
    var selectedSupplier : Entities!
    
    var selectedDocType : DocType!
    
    var filteredArrProductTypelist:NSMutableArray = NSMutableArray()
    
    var filteredArrofletters:NSMutableArray = NSMutableArray()
    
    let sections:Array<AnyObject> = [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    
    var moveToRoot : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let backButton: UIButton = UIButton(type: UIButtonType.Custom)
        backButton.setImage(UIImage(named: "icBackArrow"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: #selector(self.BackButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        backButton.frame = CGRectMake(0, 0, 50, 40)
        backButton.contentHorizontalAlignment = .Left
        let barbackButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = barbackButton
        
        self.title = "NEW ORDER"
        
        loadProductTypes()
    }
    
    func BackButtonPressed()
    {
        if(!moveToRoot)
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
        else
        {
            moveToRoot = false
            let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcOrderListID") as! OrderListViewController
            setViewController.DocId = appDelegate.selectedDocId
            setViewController.SupplierId = selectedSupplier.EntityId
            setViewController.moveToRoot = true
            self.navigationController?.pushViewController(setViewController, animated: true)
        }
    }

    func loadProductTypes()
    {
        
        ShowProgress()
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            // do your task
            
            self.arrProdType =  self.getData.getAllProductType()
            
            self.grouProductTypes()
            
            dispatch_async(dispatch_get_main_queue()) {
                // update some UI
                
                self.hideProgress()
                
                self.tvPrdouctType.reloadData()
            }
        }
    }
    
    func loadProductTypes(filterString: String)
    {
        
        ShowProgress()
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            self.fiterProductTypes(filterString)
            
            dispatch_async(dispatch_get_main_queue()) {
                // update some UI
                
                self.hideProgress()
                
                self.tvPrdouctType.reloadData()
            }
        }
        
    }
    
    func fiterProductTypes(strText:String)
    {
        let arr: NSMutableArray  =  NSMutableArray(array: arrProdType)
        
        filteredArrProductTypelist = NSMutableArray()
        filteredArrofletters = NSMutableArray()
        
        
        let preRef : NSPredicate = NSPredicate(format: "ProdTypeShortName CONTAINS[c] %@", strText);
        let arrFilter : NSArray = arr.filteredArrayUsingPredicate(preRef)
        
        if(arrFilter.count > 0)
        {
            for contacts in sections
            {
                let preRef1 : NSPredicate = NSPredicate(format: "ProdTypeShortName beginswith[c] %@", contacts as! String);
                let groupnames : NSArray = arrFilter.filteredArrayUsingPredicate(preRef1)
                
                if(groupnames.count > 0)
                {
                    filteredArrProductTypelist.addObject(groupnames)
                    filteredArrofletters.addObject(contacts)
                }
            }
        }
        
    }
    
    func grouProductTypes()
    {
        let arr: NSMutableArray  =  NSMutableArray(array: arrProdType)
        
        filteredArrProductTypelist = NSMutableArray()
        filteredArrofletters = NSMutableArray()
        
        for contacts in sections
        {
            let preRef1 : NSPredicate = NSPredicate(format: "ProdTypeShortName beginswith[c] %@", contacts as! String);
            let groupnames : NSArray = arr.filteredArrayUsingPredicate(preRef1)
            
            if(groupnames.count > 0)
            {
                filteredArrProductTypelist.addObject(groupnames)
                filteredArrofletters.addObject(contacts)
            }
        }
    }
    
    func ShowProgress()
    {
        let progressView = MRProgressOverlayView.showOverlayAddedTo(self.navigationController!.view, animated: true)
        
        progressView.mode = .IndeterminateSmall
        
        progressView.titleLabelText = "Loading ...";
        
        progressView.titleLabel.textColor = GlobalConstants.progressTitleColor
        
        progressView.tintColor = GlobalConstants.progressTitleColor
    }
    
    func hideProgress()
    {
        MRProgressOverlayView.dismissAllOverlaysForView(self.navigationController!.view, animated: true)
    }

    
    @IBAction func txtSearchBox_Valuechangd(sender: AnyObject)
    {
        if(txtSearchBox.text == "")
        {
            loadProductTypes()
        }
        else
        {
            loadProductTypes(String(txtSearchBox.text!))
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return  filteredArrProductTypelist.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return filteredArrProductTypelist[section].count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
        
        let arrProdutType = filteredArrProductTypelist[indexPath.section] as! NSArray
        let produtType = arrProdutType[indexPath.row] as! ProductType
        cell.textLabel?.text = String(produtType.ProdTypeShortName + " - " + produtType.ProdTypeDesc).uppercaseString
        
        cell.textLabel?.font = UIFont(name: "Ubuntu", size: 13.0)
        cell.textLabel?.textColor = UIColor(red: 52/255, green: 52/255, blue: 52/255, alpha: 1.0)
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let arrProdutType = filteredArrProductTypelist[indexPath.section] as! NSArray
        let produtType = arrProdutType[indexPath.row] as! ProductType
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productVarietyViewController = storyboard.instantiateViewControllerWithIdentifier("vcProductVarietyID") as! ProductVarietyViewController
        productVarietyViewController.prodType = produtType
        productVarietyViewController.selectedDocType = selectedDocType
        productVarietyViewController.selectedSupplier = selectedSupplier
        self.navigationController?.pushViewController(productVarietyViewController, animated: true)
    }
    
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [AnyObject]!{
        
        return self.filteredArrofletters as [AnyObject]
    }
    
    func tableView(tableView: UITableView,
                   sectionForSectionIndexTitle title: String,
                                               atIndex index: Int) -> Int{
        
        return index
    }
    
    // MARK: - UITextField Delegate
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
