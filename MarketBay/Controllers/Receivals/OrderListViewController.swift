//
//  OrderListViewController.swift
//  MarketBay
//
//  Created by Apple on 31/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import DrawerController
import KCFloatingActionButton
import MRProgress

class OrderListViewController: UIViewController, KCFloatingActionButtonDelegate
{

    @IBOutlet weak var tvOrderList: UITableView!
    
    @IBOutlet weak var lblSupplierName: UILabel!
    
    var fab: KCFloatingActionButton!
    
    var arrDocLinestList : [DocLines]  = [DocLines]()
    
    var appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var general : General = General()
    
    var DocId : Int = 0
    
    var SupplierId : Int = 0
    
    let dbConn : DBConnection = DBConnection.instance
    
    var moveToRoot : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.title = "ORDER DETAIL"
        
        let backButton: UIButton = UIButton(type: UIButtonType.Custom)
        backButton.setImage(UIImage(named: "icBackArrow"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: #selector(self.BackButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        backButton.frame = CGRectMake(0, 0, 50, 40)
        backButton.contentHorizontalAlignment = .Left
        let barbackButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = barbackButton
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "RefreshOrderlist", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.RefreshOrderlist), name:"RefreshOrderlist", object: nil)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        getOrderList()
        
        checkAllLinesConfirmed()
        
        loadFloatingActionButtons()
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        if fab != nil
        {
            fab.removeFromSuperview()
            fab = nil
        }
    }
    
    func RefreshOrderlist()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "RefreshOrderlist", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.RefreshOrderlist), name:"RefreshOrderlist", object: nil)
        
        getOrderList()
    }
    
    func getOrderList()
    {
        arrDocLinestList = dbConn.getDocLinesDetails(DocId, SupplierID: SupplierId)
        tvOrderList.reloadData()
        
        if arrDocLinestList.count > 0
        {
            lblSupplierName.text = "Supplier - " + arrDocLinestList[0].SupplierName + " ["  + arrDocLinestList[0].MarketCode + "]"
        }
    }
    
    func checkAllLinesConfirmed()
    {
        let docStatus : SaveDocument = dbConn.getTransPostStatus(DocId, SupplierId: SupplierId)
        
        if docStatus.locStatusId == 4
        {
            if docStatus.isPublished == -1
            {
                let sysMsg : SystemMessage = dbConn.getMessageUsingId(202)
                showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 1)
            }
            else if docStatus.isSendToSupplier == -1
            {
                let sysMsg : SystemMessage = dbConn.getMessageUsingId(203)
                showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 3)
            }
        }
    }
    
    func showAlertWindow(title: String, message: String, tag: Int)
    {
       let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        if(tag == 1) // Show alert for all lines confirmed
        {
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                self.UpdatePublish()
            }))
            
            alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                
            }))
        }
        else if(tag == 2) // Show alert for all lines confirmed
        {
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                self.UpdateSendToSupplier()
            }))
            
            alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                self.sendDataToOnline()
            }))
        }
        else if(tag == 3) // Show alert for all lines confirmed
        {
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                self.UpdateSendToSupplier()
            }))
            
            alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                
            }))
        }
        
        self.presentViewController(alertController, animated: true, completion:nil)
    }
    
    func UpdatePublish()
    {
        let objData : SaveDocument = SaveDocument()
        
        objData.DocID = DocId
        objData.SupplierID = SupplierId
        objData.isPublished = 0
        objData.isSendToSupplier = -1
        objData.isUpdatedToOnline = 1
        
        dbConn.updateTransPostsToSaveDocument(objData)
        let sysMsg : SystemMessage = dbConn.getMessageUsingId(203)
        showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 2)
    }
    
    func UpdateSendToSupplier()
    {
        let objData : SaveDocument = SaveDocument()
        
        objData.DocID = DocId
        objData.SupplierID = SupplierId
        objData.isPublished = 0
        objData.isSendToSupplier = 0
        objData.isUpdatedToOnline = 1
        
        dbConn.updateTransPostsToSaveDocument(objData)
        
        sendDataToOnline()
    }
    
    func ShowProgress()
    {
        let progressView = MRProgressOverlayView.showOverlayAddedTo(self.view, animated: true)
        progressView.mode = .IndeterminateSmall
        progressView.titleLabelText = "Uploading ...";
        progressView.titleLabel.textColor = GlobalConstants.progressTitleColor
        progressView.tintColor = GlobalConstants.progressTitleColor
    }
    
    func hideProgress()
    {
        MRProgressOverlayView.dismissAllOverlaysForView(self.view, animated: true)
    }
    
    func sendDataToOnline()
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
        else
        {
            ShowProgress()
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.SendToSupplierDetailSyncCompleted), name:"SendToSupplierDetailSyncCompleted", object: nil)
            let uploadData : UploadDatas = UploadDatas()
            uploadData.updateOfflineDataToServer(true)
        }
    }
    
    func SendToSupplierDetailSyncCompleted()
    {
        hideProgress()
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "SendToSupplierDetailSyncCompleted", object: nil)
        getOrderList()
    }
    
    func loadFloatingActionButtons()
    {
        if(fab == nil)
        {
            fab = KCFloatingActionButton()
            
            fab.buttonColor = GlobalConstants.progressTitleColor
            fab.plusColor = UIColor.whiteColor()
            fab.overlayColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255, alpha: 0.8)
            
            fab.size = 40
            fab.itemSize = 40
            
            fab.itemShadowColor = UIColor.whiteColor()
            
            
            let itemSaveNewLine = KCFloatingActionButtonItem()
                itemSaveNewLine.buttonColor = general.hexStringToUIColor("#16A826")
                itemSaveNewLine.circleShadowColor = UIColor.whiteColor()
                itemSaveNewLine.titleShadowColor = UIColor.whiteColor()
                itemSaveNewLine.icon = UIImage(named: "icNewLine")
                itemSaveNewLine.iconImageView.frame = CGRectMake(11, 13, 16.5, 14.5)
                itemSaveNewLine.title = "New Line"
                itemSaveNewLine.titleLabel.textColor = general.hexStringToUIColor("#343434")
                itemSaveNewLine.titleLabel.font =  UIFont(name: "Ubuntu", size: 12)
                itemSaveNewLine.titleLabel.textAlignment = .Right
                
                itemSaveNewLine.handler = { item in
                    self.fab.close()
                    
                    self.appDelegate.isNewOrder = false
                    self.appDelegate.isNewLine = true
                    
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: "SendToSupplierDetailSyncCompleted", object: nil)
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcProductTypeID") as! ProductTypeViewController
                    
                    setViewController.selectedSupplier = self.dbConn.getSupplierDetails(self.SupplierId)
                    setViewController.selectedDocType = self.dbConn.getDocTypeById(self.arrDocLinestList[0].DocTypeId)
                    self.appDelegate.selectedDocId = self.DocId
                    self.navigationController?.pushViewController(setViewController, animated: true)
                }
                
            fab.addItem(item: itemSaveNewLine)
            
            let itemDeleteLine = KCFloatingActionButtonItem()
            itemDeleteLine.buttonColor = general.hexStringToUIColor("#16A826")
            itemDeleteLine.circleShadowColor = UIColor.whiteColor()
            itemDeleteLine.titleShadowColor = UIColor.whiteColor()
            itemDeleteLine.icon = UIImage(named: "icNewOrder")
            itemDeleteLine.iconImageView.frame = CGRectMake(11, 13, 16.5, 14.5)
            itemDeleteLine.title = "New Order"
            itemDeleteLine.titleLabel.textColor = general.hexStringToUIColor("#343434")
            itemDeleteLine.titleLabel.font =  UIFont(name: "Ubuntu", size: 12)
            itemDeleteLine.titleLabel.textAlignment = .Right
            
            itemDeleteLine.handler = { item in
                self.fab.close()
                
                self.appDelegate.isNewOrder = true
                self.appDelegate.isNewLine = true
                self.appDelegate.selectedDocId = 0
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcSupplierID") as! SupplierViewController
                self.navigationController?.pushViewController(setViewController, animated: true)
            }
            
            fab.addItem(item: itemDeleteLine)
            
            fab.fabDelegate = self
            self.navigationController?.view.addSubview(fab)
        }
    }
    
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        print("FAB Opened")
    }
    
    func KCFABClosed(fab: KCFloatingActionButton) {
        print("FAB Closed")
    }
    
    func BackButtonPressed()
    {
        if(!moveToRoot)
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
        else
        {
            moveToRoot = false
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDocLinestList.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ordercell", forIndexPath: indexPath) as! OrderTVCell
        
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        let objDoc = arrDocLinestList[indexPath.row]
        
        cell.lblSupplier.text = String(indexPath.row + 1)
        
        cell.lblProdType.text = objDoc.ProdTypeName
        
        cell.lblProdVarity.text = objDoc.FreeformDesc
        
        cell.lblQty.text = String(objDoc.BuyCtnQty)
        
        cell.lblQtyUnit.text = objDoc.FreeformBuyCtn != "" ? objDoc.FreeformBuyCtn : " - "
        
        cell.lblContent.text = String(objDoc.BuyUnitQty)
        
        cell.lblContentUnit.text = objDoc.FreeformBuyUnit != "" ? objDoc.FreeformBuyUnit : " - "
        
        cell.lblPrice.text = String(format: "$%.2f", Double(objDoc.BuyPrice))
        
        cell.lblPriceUnit.text = objDoc.FreeformPriceUnit != "" ? objDoc.FreeformPriceUnit : " - "
        
        switch objDoc.locStatusId {
        case AppDelegate.DLStatus.BlankYellow.rawValue:
             cell.ivStatus.image = UIImage(named: "icStatusYellow")
             cell.lblStatus.text = ""
            break;
        case AppDelegate.DLStatus.YellowWithP.rawValue:
             cell.ivStatus.image = UIImage(named: "icStatusYellow")
             cell.lblStatus.text = "P"
            break;
        case AppDelegate.DLStatus.PingWithP.rawValue:
             cell.ivStatus.image = UIImage(named: "icStatusPing")
             cell.lblStatus.text = "P"
            break;
        case AppDelegate.DLStatus.PingWithC.rawValue:
            cell.ivStatus.image = UIImage(named: "icStatusPing")
            cell.lblStatus.text = "C"
            break;
        case AppDelegate.DLStatus.GreenWithC.rawValue:
             cell.ivStatus.image = UIImage(named: "icStatusGreen")
             cell.lblStatus.text = "C"
            break;
        default:
            cell.ivStatus.image = UIImage(named: "icStatusYellow")
            cell.lblStatus.text = ""
             break;
        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
         let objDoc = arrDocLinestList[indexPath.row]
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let createDocViewController = storyboard.instantiateViewControllerWithIdentifier("vcCreateOrderID") as! CreateOrderViewController
        createDocViewController.selectedDocLine = objDoc
        createDocViewController.pageMode = false
        createDocViewController.selectedArrIndex = indexPath.row
        createDocViewController.arrDocLinestList = arrDocLinestList
        appDelegate.selectedDocId = DocId
        createDocViewController.selectedSupplier = dbConn.getSupplierDetails(SupplierId)
        createDocViewController.selectedDocType = dbConn.getDocTypeById(objDoc.DocTypeId)
        createDocViewController.existingsDocLine = ConvertClassToClass.convertDocLineToDocLine(objDoc)
        
        appDelegate.isNewLine = false
        appDelegate.isNewOrder = false
        
        self.navigationController?.pushViewController(createDocViewController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
