//
//  ProductVarietyViewController.swift
//  MarketBay
//
//  Created by Apple on 31/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import MRProgress

class ProductVarietyViewController: UIViewController {

    
    @IBOutlet weak var txtSearchBox: customTextField!
    
    @IBOutlet weak var tvProductVeriety: UITableView!
    
    var arrProdVarity:[ProductVarieties] = [ProductVarieties]()
    
    var prodType: ProductType!
    
    var selectedSupplier : Entities!
    
    var selectedDocType : DocType!
    
    var dateFormatter = NSDateFormatter()
    
    var getData : DBConnection = DBConnection()
    
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    let sections:Array<AnyObject> = [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    
    var filteredArrProductVaritylist:NSMutableArray = NSMutableArray()
    
    var filteredArrofletters:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let backButton: UIButton = UIButton(type: UIButtonType.Custom)
        backButton.setImage(UIImage(named: "icBackArrow"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: #selector(self.BackButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        backButton.frame = CGRectMake(0, 0, 50, 40)
        backButton.contentHorizontalAlignment = .Left
        let barbackButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = barbackButton
        
        self.title = "NEW ORDER"
        
        self.loadProductVariety()
    }

    // MARK: - Function
    
    func loadProductVariety()
    {
        ShowProgress()
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            self.arrProdVarity =  self.getData.getProductVarieties(self.prodType.ProdTypeId)
            
            self.grouProductVariety()
            
            dispatch_async(dispatch_get_main_queue()) {
                
                self.hideProgress()
                
                self.tvProductVeriety.reloadData()
            }
        }
    }
    
    
    func fiterProductVariety(strText:String)
    {
        let arr: NSMutableArray  =  NSMutableArray(array: arrProdVarity)
        
        filteredArrProductVaritylist = NSMutableArray()
        filteredArrofletters = NSMutableArray()
        let preRef : NSPredicate = NSPredicate(format: "VarietyDesc CONTAINS[c] %@", strText);
        let arrFilter : NSArray = arr.filteredArrayUsingPredicate(preRef)
        
        if(arrFilter.count > 0)
        {
            for contacts in sections
            {
                let preRef1 : NSPredicate = NSPredicate(format: "VarietyDesc beginswith[c] %@", contacts as! String);
                let groupnames : NSArray = arrFilter.filteredArrayUsingPredicate(preRef1)
                
                if(groupnames.count > 0)
                {
                    filteredArrProductVaritylist.addObject(groupnames)
                    filteredArrofletters.addObject(contacts)
                }
            }
        }
        
    }
    
    func loadProductVariety(filterString: String)
    {
        ShowProgress()
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            self.fiterProductVariety(filterString)
            
            dispatch_async(dispatch_get_main_queue()) {
                
                self.hideProgress()
                
                self.tvProductVeriety.reloadData()
            }
        }
    }
    
    func grouProductVariety()
    {
        let arr: NSMutableArray  =  NSMutableArray(array: arrProdVarity)
        
        filteredArrProductVaritylist = NSMutableArray()
        filteredArrofletters = NSMutableArray()
        
        for contacts in sections
        {
            let preRef1 : NSPredicate = NSPredicate(format: "VarietyDesc beginswith[c] %@", contacts as! String);
            let groupnames : NSArray = arr.filteredArrayUsingPredicate(preRef1)
            
            if(groupnames.count > 0)
            {
                filteredArrProductVaritylist.addObject(groupnames)
                filteredArrofletters.addObject(contacts)
            }
        }
    }
    
    func ShowProgress(loadingTxt:String = "")
    {
        let progressView = MRProgressOverlayView.showOverlayAddedTo(self.navigationController!.view, animated: true)
        
        progressView.mode = .IndeterminateSmall
        
        if(loadingTxt == "")
        {
            progressView.titleLabelText = "Loading ...";
        }
        else
        {
            progressView.titleLabelText = loadingTxt;
        }
        
        progressView.titleLabel.textColor = GlobalConstants.progressTitleColor
        
        progressView.tintColor = GlobalConstants.progressTitleColor
        
    }
    
    func hideProgress()
    {
        MRProgressOverlayView.dismissAllOverlaysForView(self.navigationController!.view, animated: true)
    }
    
    func BackButtonPressed()
    {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    @IBAction func txtSearchBox_Valuechanged(sender: AnyObject)
    {
        if(txtSearchBox.text == "")
        {
            loadProductVariety()
        }
        else
        {
            loadProductVariety(String(txtSearchBox.text!))
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return filteredArrProductVaritylist.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return filteredArrProductVaritylist[section].count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
        
        let arrProdutVarity = filteredArrProductVaritylist[indexPath.section] as! NSArray
        let produtVarity = arrProdutVarity[indexPath.row] as! ProductVarieties
        
        cell.textLabel?.text = String(produtVarity.VarietyDesc).uppercaseString
        
        cell.textLabel?.font = UIFont(name: "Ubuntu", size: 13.0)
        cell.textLabel?.textColor = UIColor(red: 52/255, green: 52/255, blue: 52/255, alpha: 1.0)
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let arrProdutVarity = filteredArrProductVaritylist[indexPath.section] as! NSArray
        let produtVarity = arrProdutVarity[indexPath.row] as! ProductVarieties
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let createDocViewController = storyboard.instantiateViewControllerWithIdentifier("vcCreateOrderID") as! CreateOrderViewController
        createDocViewController.selectedProdType = prodType
        createDocViewController.selectedProdVerity = produtVarity
        createDocViewController.selectedDocType = selectedDocType
        createDocViewController.selectedSupplier = selectedSupplier
        
        if !appDelegate.isNewOrder
        {
            createDocViewController.arrDocLinestList = getData.getDocLinesDetails(appDelegate.selectedDocId, SupplierID: selectedSupplier.EntityId)
        }
        
        self.navigationController?.pushViewController(createDocViewController, animated: true)
    }
    
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [AnyObject]!{
        
        return self.filteredArrofletters as [AnyObject]
    }
    
    func tableView(tableView: UITableView,
                   sectionForSectionIndexTitle title: String,
                                               atIndex index: Int) -> Int{
        
        return index
    }
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
