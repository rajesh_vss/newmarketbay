//
//  DocTypeViewController.swift
//  MarketBay
//
//  Created by Apple on 06/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import MRProgress

class DocTypeViewController: UIViewController
{
    
    @IBOutlet weak var tvDocTypeView: UITableView!
    
    var selectedSupplier : Entities!
    
    let getData: DBConnection = DBConnection()
    
    var strPageTilte = "NEW ORDER"
    
    var arrDocType:[DocType] = [DocType]()
    
    @IBOutlet weak var txtSearchBox: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let backButton: UIButton = UIButton(type: UIButtonType.Custom)
        backButton.setImage(UIImage(named: "icBackArrow"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: #selector(self.BackButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        backButton.frame = CGRectMake(0, 0, 50, 40)
        backButton.contentHorizontalAlignment = .Left
        let barbackButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = barbackButton
        
        self.title = strPageTilte
        
        getDocTypes()
    }

    func BackButtonPressed()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func getDocTypes()
    {
        arrDocType = getData.getAllDocType()
        
        self.tvDocTypeView.reloadData()
    }
    
    func FilterDocTypes(filterString: String)
    {
        ShowProgress()
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            // do your task
            
            self.arrDocType =  self.getData.FilterDocType(filterString)
            
//            self.groupSupplier()
            
            dispatch_async(dispatch_get_main_queue()) {
                // update some UI
                
                self.hideProgress()
                
                self.tvDocTypeView.reloadData()
            }
        }
    }
    
    func ShowProgress()
    {
        let progressView = MRProgressOverlayView.showOverlayAddedTo(self.navigationController!.view, animated: true)
        
        progressView.mode = .IndeterminateSmall
        
        progressView.titleLabelText = "Loading ...";
        
        progressView.titleLabel.textColor = GlobalConstants.progressTitleColor
        
        progressView.tintColor = GlobalConstants.progressTitleColor
    }
    
    func hideProgress()
    {
        MRProgressOverlayView.dismissAllOverlaysForView(self.navigationController!.view, animated: true)
    }
    
    @IBAction func txtSearch_EditChanged(sender: AnyObject)
    {
        if(txtSearchBox.text == "")
        {
            getDocTypes()
        }
        else
        {
            FilterDocTypes(String(txtSearchBox.text!))
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrDocType.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell
        
        cell.textLabel?.text = String(arrDocType[indexPath.row].DocTypeDesc).uppercaseString
        
        cell.textLabel?.font = UIFont(name: "Ubuntu", size: 13.0)
        cell.textLabel?.textColor = UIColor(red: 52/255, green: 52/255, blue: 52/255, alpha: 1.0)
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let docType = arrDocType[indexPath.row]
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let saleEntryViewController = storyboard.instantiateViewControllerWithIdentifier("vcProductTypeID") as! ProductTypeViewController
        saleEntryViewController.selectedSupplier = selectedSupplier
        saleEntryViewController.selectedDocType = docType
        self.navigationController?.pushViewController(saleEntryViewController, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
