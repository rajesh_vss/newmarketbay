 //
//  CreateOrderViewController.swift
//  MarketBay
//
//  Created by Apple on 24/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import KCFloatingActionButton
import MRProgress

class CreateOrderViewController: UIViewController, KCFloatingActionButtonDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIGestureRecognizerDelegate,UIPopoverPresentationControllerDelegate
{
    
    @IBOutlet weak var txtProdType: UITextField!
    
    @IBOutlet weak var txtProdVarity: UITextField!
    
    @IBOutlet weak var txtQty: UITextField!
    
    @IBOutlet weak var txtQtyUnit: UITextField!
    
    @IBOutlet weak var txtContents: UITextField!
    
    @IBOutlet weak var txtContentUnit: UITextField!
    
    @IBOutlet weak var txtPrice: UITextField!
    
//    @IBOutlet weak var txtPriceUnit: UITextField!
    
    @IBOutlet weak var txtDeliveryRef: UITextField!
    
    @IBOutlet weak var txtSupplierRef: UITextField!
    
    @IBOutlet weak var btnPurchase: UIButton!
    
    @IBOutlet weak var btnConfirmLine: UIButton!
    
    @IBOutlet weak var scrlView: UIScrollView!
    
    @IBOutlet weak var txtPriceUnit: UILabel!
    
    @IBOutlet weak var vwHeader: UIView!
    
    @IBOutlet weak var btnPrev: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var lblLineNumber: UILabel!
    
    @IBOutlet weak var vwHeadHeightCons: NSLayoutConstraint!
    
    @IBOutlet weak var vwMainView: UIView!
    
    @IBOutlet weak var vwMVHeightCons: NSLayoutConstraint!
    
    @IBOutlet weak var txtGrade: UITextField!
    
    @IBOutlet weak var txtColour: UITextField!
    
    @IBOutlet weak var txtSizeCount: UITextField!
    
    
    
    var fab: KCFloatingActionButton!
    
    var general : General = General()
    
    var pageMode : Bool = true
    // pageMode = false - Update || = true - Insert
    
    var isSave: Bool = true
    
    var selectedDocLine : DocLines!
    var existingsDocLine : DocLines!
    
    var arrDocLinestList : [DocLines]  = [DocLines]()
    
    var arrContainers:[Containers]! = [Containers]()
    var arrPriceContainers:[Containers]! = [Containers]()
    var arrProdVarity : [ProductVarieties] = [ProductVarieties]()
    
    let dbConn : DBConnection = DBConnection.instance
    
    var selectedQtyUnitIndex : Int = 0
    var selectedContentUnitIndex : Int = 0
    var selectedPriceUnitIndex : Int = 0
    var selectedProdVarietyIndex : Int = 0
    var selectedArrIndex : Int = 0
    
    var existingQtyUnit : String = ""
    var existingContentUnit : String = ""
    var existingPriceUnit : String = ""
    var existingProdVarity : String = ""
    
    var selectedTransPostId : Int = 0
    var selectedDocLinesId : Int = 0
    var selectedSupplier : Entities!
    var selectedDocType : DocType!
    var selectedProdType : ProductType!
    var selectedProdVerity : ProductVarieties!
    
    var pickerView: UIPickerView!
    
    var mytoolbar: UIToolbar!
    
    var fabButtonMode : Int = 1
    
    var appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    var dateFormatter = NSDateFormatter()
    
    var activeTextField : UITextField!
    
    var detailPopover: UIPopoverPresentationController!
    var contentViewController : SearchPopViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.title = "ORDER DETAIL"
        
        let backButton: UIButton = UIButton(type: UIButtonType.Custom)
        backButton.setImage(UIImage(named: "icBackArrow"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: #selector(self.BackButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        backButton.frame = CGRectMake(0, 0, 50, 40)
        backButton.contentHorizontalAlignment = .Left
        let barbackButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = barbackButton
        
        self.startObservingKeyboardEvents()
        
        getCountiners()
        
        txtProdType.enabled = false
//        txtProdVarity.enabled = false
        
        let paddRigView4:UIView = UIView(frame: CGRect(x:0, y:0, width:15, height:20));
        let lblText2 : UILabel = UILabel(frame: CGRect(x:5, y:0, width:15, height:20))
        lblText2.text = "$"
        lblText2.font = UIFont(name: "Ubuntu", size: 14.0)!
        lblText2.textColor = UIColor.darkGrayColor()
        paddRigView4.addSubview(lblText2)
        txtPrice.leftView = paddRigView4
        txtPrice.leftViewMode = UITextFieldViewMode.Always;
        txtPrice.autocorrectionType = .No
//        txtPriceUnit.autocorrectionType = .No
        txtQty.autocorrectionType = .No
        txtQtyUnit.autocorrectionType = .No
        txtContents.autocorrectionType = .No
        txtContentUnit.autocorrectionType = .No
        txtProdVarity.autocorrectionType = .No
        
        generateTransPostID()
        
        if pageMode
        {
            hideHeader()
            fillData ()
            assignInitialContainer()
            txtQty.becomeFirstResponder()
        }
        else
        {
            showHeader()
            loadDocLineDetails()
        }
        
        txtPriceUnit.backgroundColor = UIColor(patternImage: UIImage(named: "icDropdown")!)
        txtPriceUnit.userInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.txtPriceUnitTabed)) //Selector("txtPriceUnitTabed"))
        tapGesture.delegate = self
        txtPriceUnit.addGestureRecognizer(tapGesture)
        
//        self.view.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.evo_drawerController?.openDrawerGestureModeMask = .PanningNavigationBar
        
        loadFloatingActionButtons()
        
        if !pageMode
        {
            showHeader()
            loadDocLineDetails()
        }
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        self.evo_drawerController?.openDrawerGestureModeMask = .All
        
        if fab != nil
        {
            fab.removeFromSuperview()
            fab = nil
        }
    }
    
    // MARK: - Functions
    
    func generateTransPostID()
    {
        if appDelegate.isNewOrder
        {
            appDelegate.selectedDocId = dbConn.getOrderDocId()
        }
        
        if appDelegate.isNewLine
        {
            selectedTransPostId = dbConn.getOrderTransPostId()
            
            selectedDocLinesId = dbConn.getOrderDocLineId()
        }
    }
    
    func hideHeader()
    {
        vwHeadHeightCons.constant = 1
        vwHeader.layoutIfNeeded()
        vwHeader.alpha = 0
        
        btnNext.hidden = true
        btnPrev.hidden = true
        lblLineNumber.hidden = true
        
        vwMVHeightCons.constant = 678
    }
    
    func showHeader()
    {
        vwHeadHeightCons.constant = 72
        vwHeader.layoutIfNeeded()
        
        vwHeader.alpha = 1
        
        btnNext.hidden = false
        btnPrev.hidden = false
        lblLineNumber.hidden = false
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        vwMainView.addGestureRecognizer(swipeRight)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.Left
        vwMainView.addGestureRecognizer(swipeDown)
        
         vwMVHeightCons.constant = 750
    }
    
    func fillData ()
    {
        txtProdType.text = selectedProdType.ProdTypeDesc
        txtProdVarity.text = selectedProdVerity.VarietyDesc
        
        txtProdType.tag = selectedProdType.ProdTypeId
        txtProdVarity.tag = selectedProdVerity.VarietyId
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                
                if CompareExistingClassValues()
                {
                    moveToPrevDocLine()
                }
                else
                {
                    let sysMsg : SystemMessage = dbConn.getMessageUsingId(201)
                    showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 7) // 201
                }
                
            case UISwipeGestureRecognizerDirection.Down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.Left:
                
                if CompareExistingClassValues()
                {
                    moveToNextDocLine()
                }
                else
                {
                    let sysMsg : SystemMessage = dbConn.getMessageUsingId(201)
                    showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 8) // 201
                }
                
            case UISwipeGestureRecognizerDirection.Up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func CompareExistingClassValues() -> Bool
    {
        if !pageMode
        {
            if existingsDocLine.LineId != selectedDocLine.LineId || existingsDocLine.SupplierRef != txtSupplierRef.text || existingsDocLine.DeliveryRef != txtDeliveryRef.text || existingsDocLine.BuyCtnQty != (txtQty.text != "" ? Int(txtQty.text!)! : 0) || existingsDocLine.FreeformBuyCtn != txtQtyUnit.text! || existingsDocLine.BuyUnitQty != (txtContents.text != "" ? Int(txtContents.text!)! : 0) || existingsDocLine.FreeformBuyUnit != txtContentUnit.text! || existingsDocLine.BuyPrice != (txtPrice.text != "" ? Double(txtPrice.text!)! : 0) || existingsDocLine.FreeformPriceUnit != txtPriceUnit.text! || existingsDocLine.Grade != txtGrade.text! || existingsDocLine.Colour != txtColour.text! || existingsDocLine.CountSize != (txtSizeCount.text != "" ? Int(txtSizeCount.text!)! : 0) || existingsDocLine.FreeformDesc != txtProdVarity.text!
            {
                return false
            }
        }
        else
        {
            return false
        }
        
        return true
    }
    
    func loadDocLineDetails()
    {
        if(selectedDocLine != nil)
        {
            appDelegate.selectedDocId = selectedDocLine.DocId
            selectedDocLinesId = selectedDocLine.LineId             
            
            txtProdType.text = selectedDocLine.ProdTypeName
            txtProdType.tag = selectedDocLine.ProdTypeId
            
            txtProdVarity.text = selectedDocLine.FreeformDesc
            txtProdVarity.tag = selectedDocLine.VarietyId
            
            txtQty.text = String(selectedDocLine.BuyCtnQty)
            txtQtyUnit.text = selectedDocLine.FreeformBuyCtn
            txtQtyUnit.tag = selectedDocLine.BuyCtnCid
            
            txtContents.text = String(selectedDocLine.BuyUnitQty)
            txtContentUnit.text = selectedDocLine.FreeformBuyUnit
            txtContentUnit.tag = selectedDocLine.BuyUnitCid
            
            refreshPriceUnitsArray()
            
            txtPrice.text = String(format: "%.2f", Double(selectedDocLine.BuyPrice))
            txtPriceUnit.text = selectedDocLine.FreeformPriceUnit
            txtPriceUnit.tag = selectedDocLine.BuyPriceCid
            
            txtDeliveryRef.text = selectedDocLine.DeliveryRef
            txtSupplierRef.text = selectedDocLine.SupplierRef
            
            txtSizeCount.text = selectedDocLine.CountSize != 0 ? String(selectedDocLine.CountSize) : ""
            txtGrade.text = selectedDocLine.Grade
            txtColour.text = selectedDocLine.Colour
            
            for index in 0 ..< arrContainers.count
            {
                let item = arrContainers[index]
                
                if item.Cid == selectedDocLine.BuyCtnCid
                {
                    selectedQtyUnitIndex = index
                }
                
                if item.Cid == selectedDocLine.BuyUnitCid
                {
                    selectedContentUnitIndex = index
                }
            }
            
            for index in 0 ..< arrPriceContainers.count
            {
                let item = arrPriceContainers[index]
                
                if item.Cid == selectedDocLine.BuyPriceCid
                {
                    selectedPriceUnitIndex = index
                }
            }
            
            for index in 0 ..< arrProdVarity.count
            {
                let item = arrProdVarity[index]
                
                if item.VarietyId == selectedDocLine.ProdTypeId
                {
                    selectedProdVarietyIndex = index
                }
            }
            
            lblLineNumber.text = "Line " + String(selectedArrIndex + 1)
            
            updateDocLineStatus(AppDelegate.DLStatus.YellowWithP.rawValue, bStatus : 0)
            
            hidePrevNextButton()
            
            hideConfirmAndPurchaseButton()
        }
    }
    
    func updateDocLineStatus(StatusId : Int, bStatus : Int)
    {
        if StatusId > selectedDocLine.locStatusId
        {
            selectedDocLine.locStatusId = StatusId
            dbConn.updateDocLineStatus(StatusId, LineId: selectedDocLine.OrderId, bStatus: bStatus)
        }
    }
    
    func getCountiners()
    {
        arrContainers = dbConn.getAllContainers()
    }
    
    func refreshPriceUnitsArray()
    {
        arrPriceContainers =  [Containers]()
        
        let contextCantainers : Containers = Containers()
        let contextCantainers1 : Containers = Containers()
        
//        if txtQtyUnit.tag == 0
//        {
//            contextCantainers = Containers()
//            contextCantainers.Cid = txtQtyUnit.tag
//            contextCantainers.CidDesc = txtQtyUnit.text!
//        }
//        else
//        {
//            contextCantainers = dbConn.getContainerbyId(String(txtQtyUnit.tag))
//        }
//
//        if txtContentUnit.tag == 0
//        {
//            contextCantainers1 = Containers()
//            contextCantainers1.Cid = txtContentUnit.tag
//            contextCantainers1.CidDesc = txtContentUnit.text!
//        }
//        else
//        {
//            contextCantainers1 = dbConn.getContainerbyId(String(txtContentUnit.tag))
//        }
//        
//        if(contextCantainers != nil)
//        {
//            arrPriceContainers.append(contextCantainers)
//            
//            if contextCantainers1 != nil && contextCantainers.Cid != contextCantainers1.Cid
//            {
//                arrPriceContainers.append(contextCantainers1)
//            }
//        }
//        else
//        {
//            if contextCantainers1 != nil
//            {
//                arrPriceContainers.append(contextCantainers1)
//            }
//        }
        
        contextCantainers.Cid = txtQtyUnit.tag
        contextCantainers.CidDesc = txtQtyUnit.text!
        
        contextCantainers1.Cid = txtContentUnit.tag
        contextCantainers1.CidDesc = txtContentUnit.text!
        
        arrPriceContainers.append(contextCantainers)
            
        if contextCantainers.CidDesc != contextCantainers1.CidDesc
        {
            arrPriceContainers.append(contextCantainers1)
        }
      
        if txtPriceUnit.tag > 0
        {
            let arrFilter: NSMutableArray  =  NSMutableArray(array: arrPriceContainers)
            
            let preRef1 : NSPredicate = NSPredicate(format: "Cid = %d", txtPriceUnit.tag);
            let groupnames : NSArray = arrFilter.filteredArrayUsingPredicate(preRef1)
            
            if(groupnames.count > 0)
            {
                
            }
            else
            {
                txtPriceUnit.text = ""
                txtPriceUnit.tag = 0
                selectedPriceUnitIndex = 0
            }
        }
        else
        {
            txtPriceUnit.tag = txtQtyUnit.tag
            txtPriceUnit.text = txtQtyUnit.text
            
            for index in 0 ..< arrPriceContainers.count
            {
                let item = arrPriceContainers[index]
                
                if item.Cid == txtQtyUnit.tag
                {
                    selectedPriceUnitIndex = index
                }
            }
        }
    }
    
    func assignInitialContainer()
    {
        arrProdVarity = self.dbConn.getProductVarieties(selectedProdType != nil ? selectedProdType.ProdTypeId : selectedDocLine.ProdTypeId)
        
        if(selectedProdVerity.CtnCid != 0)
        {
            let qtyCantainers = dbConn.getContainerbyId(String(selectedProdVerity.CtnCid))
            txtQtyUnit.tag = selectedProdVerity.CtnCid
            txtQtyUnit.text = qtyCantainers.CidDesc
        }
        
        if(selectedProdVerity.UnitCid != 0)
        {
            let contextCantainers = dbConn.getContainerbyId(String(selectedProdVerity.UnitCid))
            txtContentUnit.tag = selectedProdVerity.UnitCid
            txtContentUnit.text = contextCantainers.CidDesc
        }
        
        txtContents.text = String(format: "%.0f", selectedProdVerity.UnitQty)
        
        for index in 0 ..< arrContainers.count
        {
            let item = arrContainers[index]
            
            if item.Cid == txtQtyUnit.tag
            {
                selectedQtyUnitIndex = index
            }
            
            if item.Cid == txtContentUnit.tag
            {
                selectedContentUnitIndex = index
            }
        }
        
        for index in 0 ..< arrProdVarity.count
        {
            let item = arrProdVarity[index]
            
            print("Tag: " + String(item.VarietyId) + " - " + String(txtProdVarity.tag))
            
            if item.VarietyId == txtProdVarity.tag
            {
                print("ProdVarietyIndex: " + String(selectedProdVarietyIndex))
                selectedProdVarietyIndex = index
            }
        }
        
        refreshPriceUnitsArray()
    }
    
    func fillSizeCount(sizecount : String)
    {
        if !pageMode
        {
            selectedDocLine.CountSize = Int(sizecount)!
        }
        else
        {
            txtSizeCount.text = sizecount
        }
    }
    
    func hidePrevNextButton()
    {
        if selectedArrIndex == 0
        {
            btnPrev.enabled = false
        }
        else
        {
            btnPrev.enabled = true
        }
        
        if selectedArrIndex == arrDocLinestList.count-1
        {
            btnNext.enabled = false
        }
        else
        {
            btnNext.enabled = true
        }
    }
    
    func hideConfirmAndPurchaseButton()
    {
        if selectedDocLine.locStatusId >= AppDelegate.DLStatus.GreenWithC.rawValue
        {
            btnPurchase.enabled = false
            btnConfirmLine.enabled = false
            btnConfirmLine.alpha = 0.6
            btnPurchase.alpha = 0.6
        }
        else
        {
            btnPurchase.enabled = true
            btnConfirmLine.enabled = true
            btnPurchase.alpha = 1
            btnConfirmLine.alpha = 1
        }
        
        if selectedDocLine.locStatusId >= AppDelegate.DLStatus.PingWithP.rawValue
        {
            btnPurchase.enabled = false
            btnPurchase.alpha = 0.6
        }
        else
        {
            btnPurchase.enabled = true
            btnPurchase.alpha = 1
        }
    }
    
    func disableTextFields()
    {
        txtQty.enabled = false
        txtQtyUnit.enabled = false
        txtContents.enabled = false
        txtContentUnit.enabled = false
        txtPrice.enabled = false
        txtPriceUnit.enabled = false
        txtDeliveryRef.enabled = false
        txtSupplierRef.enabled = false
    }
    
    func enableTextFields()
    {
        txtQty.enabled = true
        txtQtyUnit.enabled = true
        txtContents.enabled = true
        txtContentUnit.enabled = true
        txtPrice.enabled = true
        txtPriceUnit.enabled = true
        txtDeliveryRef.enabled = true
        txtSupplierRef.enabled = true
    }
    
    func loadFloatingActionButtons()
    {
        if(fab == nil)
        {
            fab = KCFloatingActionButton()
            
            fab.buttonColor = GlobalConstants.progressTitleColor
            fab.plusColor = UIColor.whiteColor()
            fab.overlayColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255, alpha: 0.8)
            
            fab.size = 40
            fab.itemSize = 40
            
            fab.itemShadowColor = UIColor.whiteColor()
            
            let itemCancelReview = KCFloatingActionButtonItem()
            itemCancelReview.buttonColor = general.hexStringToUIColor("#16A826")
            itemCancelReview.circleShadowColor = UIColor.whiteColor()
            itemCancelReview.titleShadowColor = UIColor.whiteColor()
            itemCancelReview.icon = UIImage(named: "icCancelReivew")
            itemCancelReview.iconImageView.frame = CGRectMake(13, 13, 14.5, 14.5)
            itemCancelReview.title = "Cancel & Review"
            itemCancelReview.titleLabel.textColor = general.hexStringToUIColor("#343434")
            itemCancelReview.titleLabel.font =  UIFont(name: "Ubuntu", size: 12)
            itemCancelReview.titleLabel.textAlignment = .Right
            
            itemCancelReview.handler = { item in
                self.fab.close()
                self.fabButtonMode = 1
                
                self.OrderDetailsDetailSyncCompleted(nil)
            }
            
            
            let itemSaveReview = KCFloatingActionButtonItem()
            itemSaveReview.buttonColor = general.hexStringToUIColor("#16A826")
            itemSaveReview.circleShadowColor = UIColor.whiteColor()
            itemSaveReview.titleShadowColor = UIColor.whiteColor()
            itemSaveReview.icon = UIImage(named: "icSaveReview")
            itemSaveReview.iconImageView.frame = CGRectMake(11, 13, 16.5, 14.5)
            itemSaveReview.title = "Save & Review"
            itemSaveReview.titleLabel.textColor = general.hexStringToUIColor("#343434")
            itemSaveReview.titleLabel.font =  UIFont(name: "Ubuntu", size: 12)
            itemSaveReview.titleLabel.textAlignment = .Right
            
            itemSaveReview.handler = { item in
                
                self.fab.close()
                
                if !self.CompareExistingClassValues()
                {
                    self.fabButtonMode = 2
                    self.saveDocLineDetails()
                    self.sendDataToOnline()
                }
                else
                {
                    self.fabButtonMode = 6
                    self.OrderDetailsDetailSyncCompleted(nil)
                }
            }
            
            let itemSaveNewOrder = KCFloatingActionButtonItem()
            itemSaveNewOrder.buttonColor = general.hexStringToUIColor("#16A826")
            itemSaveNewOrder.circleShadowColor = UIColor.whiteColor()
            itemSaveNewOrder.titleShadowColor = UIColor.whiteColor()
            itemSaveNewOrder.icon = UIImage(named: "icNewOrder")
            itemSaveNewOrder.iconImageView.frame = CGRectMake(11, 13, 16.5, 14.5)
            itemSaveNewOrder.title = "Save & New Order"
            itemSaveNewOrder.titleLabel.textColor = general.hexStringToUIColor("#343434")
            itemSaveNewOrder.titleLabel.font =  UIFont(name: "Ubuntu", size: 12)
            itemSaveNewOrder.titleLabel.textAlignment = .Right
            
            itemSaveNewOrder.handler = { item in
                self.fab.close()
                
                if !self.CompareExistingClassValues()
                {
                    self.fabButtonMode = 3
                    self.saveDocLineDetails()
                    self.sendDataToOnline()
                }
                else
                {
                    self.fabButtonMode = 9
                    self.OrderDetailsDetailSyncCompleted(nil)
                }
            }
            
            let itemSaveNewLine = KCFloatingActionButtonItem()
            itemSaveNewLine.buttonColor = general.hexStringToUIColor("#16A826")
            itemSaveNewLine.circleShadowColor = UIColor.whiteColor()
            itemSaveNewLine.titleShadowColor = UIColor.whiteColor()
            itemSaveNewLine.icon = UIImage(named: "icNewLine")
            itemSaveNewLine.iconImageView.frame = CGRectMake(11, 13, 16.5, 14.5)
            itemSaveNewLine.title = "Save & New Line"
            itemSaveNewLine.titleLabel.textColor = general.hexStringToUIColor("#343434")
            itemSaveNewLine.titleLabel.font =  UIFont(name: "Ubuntu", size: 12)
            itemSaveNewLine.titleLabel.textAlignment = .Right
            
            itemSaveNewLine.handler = { item in
                self.fab.close()
                
                if !self.CompareExistingClassValues()
                {
                    self.fabButtonMode = 4
                    self.saveDocLineDetails()
                    self.sendDataToOnline()
                }
                else
                {
                    self.fabButtonMode = 5
                    self.OrderDetailsDetailSyncCompleted(nil)
                }
            }
            
            let itemDeleteLine = KCFloatingActionButtonItem()
            itemDeleteLine.buttonColor = general.hexStringToUIColor("#16A826")
            itemDeleteLine.circleShadowColor = UIColor.whiteColor()
            itemDeleteLine.titleShadowColor = UIColor.whiteColor()
            itemDeleteLine.icon = UIImage(named: "icDeleteLine")
            itemDeleteLine.iconImageView.frame = CGRectMake(11, 13, 16.5, 14.5)
            itemDeleteLine.title = "Delete Line"
            itemDeleteLine.titleLabel.textColor = general.hexStringToUIColor("#343434")
            itemDeleteLine.titleLabel.font =  UIFont(name: "Ubuntu", size: 12)
            itemDeleteLine.titleLabel.textAlignment = .Right
            
            itemDeleteLine.handler = { item in
                self.fab.close()
                let sysMsg : SystemMessage = self.dbConn.getMessageUsingId(204)
                self.showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 5) // 204
                self.fabButtonMode = 5
            }
            
            if !pageMode {
                fab.addItem(item: itemDeleteLine)
            }
            
            fab.addItem(item: itemSaveNewLine)
            fab.addItem(item: itemSaveNewOrder)
            fab.addItem(item: itemSaveReview)
            fab.addItem(item: itemCancelReview)
            
            fab.fabDelegate = self
            self.navigationController?.view.addSubview(fab)
        }
    }
    
    func saveDocLineDetails() -> Bool
    {
        if validateControls()
        {
            if appDelegate.isNewLine
            {
                selectedDocLine = DocLines()
                
                let objTransPost : TransPost = TransPost()
                
                dateFormatter.dateFormat = GlobalConstants.localDBDateFormat
                
                objTransPost.TransPostId = selectedTransPostId
                objTransPost.LineId  = selectedDocLinesId
                objTransPost.CreateTime = dateFormatter.stringFromDate(NSDate())
                objTransPost.DocId = appDelegate.selectedDocId
                objTransPost.DocTypeId = selectedDocType.DocTypeId
                objTransPost.SupplierId = selectedSupplier.EntityId
                objTransPost.BuyerId = Int(appDelegate.selectedBuyerId)!
                objTransPost.Status = false
                objTransPost.isPublished = -1
                objTransPost.isSendToSupplier = -1
                objTransPost.isUpdatedToOnline = 0
                
                selectedDocLine.LineId = selectedDocLinesId
                selectedDocLine.OrderId = selectedDocLinesId
                selectedDocLine.DocId = appDelegate.selectedDocId
                selectedDocLine.DocTypeId = selectedDocType.DocTypeId
                selectedDocLine.SupplierID = selectedSupplier.EntityId
                selectedDocLine.ProdTypeId = txtProdType.tag
                selectedDocLine.VarietyId = txtProdVarity.tag
                
                selectedDocLine.FreeformDesc = txtProdVarity.text!
                
                selectedDocLine.BuyCtnQty = txtQty.text != "" ? Int(txtQty.text!)! : 0
                selectedDocLine.BuyCtnCid = txtQtyUnit.tag
                selectedDocLine.FreeformBuyCtn = txtQtyUnit.text!
                
                selectedDocLine.BuyUnitQty = txtContents.text != "" ? Int(txtContents.text!)! : 0
                selectedDocLine.BuyUnitCid = txtContentUnit.tag
                selectedDocLine.FreeformBuyUnit = txtContentUnit.text!
                
                selectedDocLine.BuyPrice = txtPrice.text != "" ? Double(txtPrice.text!)! : 0
                selectedDocLine.BuyPriceCid = txtPriceUnit.tag
                selectedDocLine.FreeformPriceUnit = txtPriceUnit.text!
                
                selectedDocLine.DeliveryRef = txtDeliveryRef.text!
                selectedDocLine.SupplierRef = txtSupplierRef.text!
                
                selectedDocLine.Grade = txtGrade.text!
                selectedDocLine.Colour = txtColour.text!
                selectedDocLine.CountSize = txtSizeCount.text != "" ? Int(txtSizeCount.text!)! : 0
                
                if isSave
                {
                    dbConn.insertTransPostInLocal(objTransPost)
                    dbConn.insertDocLinesInLocal(selectedDocLine)
                }
            }
            else
            {
                selectedDocLine.ProdTypeId = txtProdType.tag
                selectedDocLine.VarietyId = txtProdVarity.tag
                selectedDocLine.FreeformDesc = txtProdVarity.text!
                
                selectedDocLine.BuyCtnQty = txtQty.text != "" ? Int(txtQty.text!)! : 0
                selectedDocLine.BuyCtnCid = txtQtyUnit.tag
                selectedDocLine.FreeformBuyCtn = txtQtyUnit.text!
                
                selectedDocLine.BuyUnitQty = txtContents.text != "" ? Int(txtContents.text!)! : 0
                selectedDocLine.BuyUnitCid = txtContentUnit.tag
                selectedDocLine.FreeformBuyUnit = txtContentUnit.text!
                
                selectedDocLine.BuyPrice = txtPrice.text != "" ? Double(txtPrice.text!)! : 0
                selectedDocLine.BuyPriceCid = txtPriceUnit.tag
                selectedDocLine.FreeformPriceUnit = txtPriceUnit.text!
                
                selectedDocLine.DeliveryRef = txtDeliveryRef.text!
                selectedDocLine.SupplierRef = txtSupplierRef.text!
                
                selectedDocLine.Grade = txtGrade.text!
                selectedDocLine.Colour = txtColour.text!
                selectedDocLine.CountSize = txtSizeCount.text != "" ? Int(txtSizeCount.text!)! : 0
                
                if isSave
                {
                    selectedDocLine.isUpdatedToOnline = 1
                    dbConn.updateDocLineDetails(selectedDocLine)
                }
            }
            
            isSave = true
            return true
        }
        else
        {
            return false
        }
    }
    
    
    
    func checkAllLinesConfirmed()
    {
        let docStatus : SaveDocument = dbConn.getTransPostStatus(appDelegate.selectedDocId, SupplierId: selectedSupplier.EntityId)
        
        if docStatus.locStatusId == 4
        {
            if docStatus.isPublished == -1
            {
                let sysMsg : SystemMessage = dbConn.getMessageUsingId(202)
                showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 3) //202
            }
            else if docStatus.isSendToSupplier == -1
            {
                let sysMsg : SystemMessage = dbConn.getMessageUsingId(203)
                showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 3) // 203
            }
        }
    }
    
    func createinputpicker()
    {
        pickerView = UIPickerView()
        mytoolbar = UIToolbar(frame: CGRectMake(0,0,320,44))
        
        pickerView.delegate = self
        pickerView.showsSelectionIndicator = true
        pickerView.dataSource = self
        
        let labPickerTitle  = UILabel(frame: CGRectMake(10,12,200,20))
        
        labPickerTitle.backgroundColor = UIColor.clearColor()
        labPickerTitle.font = UIFont(name: "Ubuntu", size: 14)
        labPickerTitle.textColor = UIColor.blackColor()
        
        labPickerTitle.text = "Units"
        
        mytoolbar.addSubview(labPickerTitle)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.Cancelled))
        cancelButton.setTitleTextAttributes(GlobalConstants.barButtonAttribute, forState: UIControlState.Normal)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.Selected))
        doneButton.setTitleTextAttributes(GlobalConstants.barButtonAttribute, forState: UIControlState.Normal)
        
        mytoolbar.setItems([spaceButton, cancelButton, doneButton], animated: true)
        
//        if(txtPriceUnit.isFirstResponder())
//        {
//            txtPriceUnit.inputView = pickerView;
//            txtPriceUnit.inputAccessoryView = mytoolbar;
//        }
//        else
        if(txtQtyUnit.isFirstResponder())
        {
            txtQtyUnit.inputView = pickerView;
            txtQtyUnit.inputAccessoryView = mytoolbar;
        }
        else if(txtContentUnit.isFirstResponder())
        {
            txtContentUnit.inputView = pickerView;
            txtContentUnit.inputAccessoryView = mytoolbar;
        }
        
        let pickerGR = UITapGestureRecognizer(target: self, action: #selector(self.selectByTouch))
        pickerGR.delegate = self;
        pickerView.addGestureRecognizer(pickerGR);
        
    }
    
    func showAlertWindow(title: String, message: String, tag: Int)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        if tag == 1
        {
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler:  { (action: UIAlertAction!) in
                self.txtQtyUnit.becomeFirstResponder()
            }))
        }
        else if tag == 6
        {
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler:  { (action: UIAlertAction!) in
                self.txtQty.becomeFirstResponder()
            }))
        }
        else if(tag == 2)
        {
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                
                if self.appDelegate.isNewLine && !self.appDelegate.isNewOrder
                {
                    self.moveToOrderListPage()
                }
                else
                {
                    self.navigationController?.popViewControllerAnimated(true)
                }
            }))
            
            alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Destructive,handler: nil))
        }
        else if(tag == 3) // Show alert for Publish
        {
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                self.UpdatePublish()
            }))
            
            alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in

            }))
        }
        else if(tag == 4) // Show alert for send to supplier
        {
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                self.UpdateSendToSupplier()
            }))
            
            alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.OrderDetailsDetailSyncCompleted), name:"SendToSupplierDetailSyncCompleted", object: nil)
                self.fabButtonMode = 1
                self.sendDataToOnline()
            }))
        }
        else if(tag == 5) // Delete confirmation
        {
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                self.DeleteLineFromDocList()
            }))
            
            alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                
            }))
        }
        else if(tag == 7) // move to prev docline
        {
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                if self.saveDocLineDetails()
                {
                    self.fabButtonMode = 7
                    self.sendDataToOnline()
                }
            }))
            
            alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                self.moveToPrevDocLine()
            }))
        }
        else if(tag == 8) // move to next docline
        {
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                if self.saveDocLineDetails()
                {
                    self.fabButtonMode = 8
                    self.sendDataToOnline()
                }
            }))
            
            alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                self.moveToNextDocLine()
            }))
        }
        
        self.presentViewController(alertController, animated: true, completion:nil)
    }
    
    func UpdatePublish()
    {
        let objData : SaveDocument = SaveDocument()
        
        objData.DocID = appDelegate.selectedDocId
        objData.SupplierID = selectedSupplier.EntityId
        objData.isPublished = 0
        objData.isSendToSupplier = -1
        objData.isUpdatedToOnline = 1
        
        dbConn.updateTransPostsToSaveDocument(objData)
        let sysMsg : SystemMessage = dbConn.getMessageUsingId(203)
        showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 4)
    }
    
    func UpdateSendToSupplier()
    {
        let objData : SaveDocument = SaveDocument()
        
        objData.DocID = appDelegate.selectedDocId
        objData.SupplierID = selectedSupplier.EntityId
        objData.isPublished = 0
        objData.isSendToSupplier = 0
        objData.isUpdatedToOnline = 1
        
        dbConn.updateTransPostsToSaveDocument(objData)
        
        fabButtonMode = 1
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.OrderDetailsDetailSyncCompleted), name:"SendToSupplierDetailSyncCompleted", object: nil)
        sendDataToOnline()
    }
    
    func ShowProgress()
    {
        let progressView = MRProgressOverlayView.showOverlayAddedTo(self.view, animated: true)
        progressView.mode = .IndeterminateSmall
        progressView.titleLabelText = "Uploading ...";
        progressView.titleLabel.textColor = GlobalConstants.progressTitleColor
        progressView.tintColor = GlobalConstants.progressTitleColor
    }
    
    func hideProgress()
    {
        MRProgressOverlayView.dismissAllOverlaysForView(self.view, animated: true)
    }
    
    func sendDataToOnline()
    {
        if(!Reachability.isConnectedToNetwork())
        {
//            self.navigationController?.popViewControllerAnimated(true)
            self.OrderDetailsDetailSyncCompleted(nil)
        }
        else
        {
            ShowProgress()
            NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(sendData), userInfo: nil, repeats: false)
        }
    }
    
    
    func sendData()
    {
        existingsDocLine = selectedDocLine
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.OrderDetailsDetailSyncCompleted), name:"OrderDetailsDetailSyncCompleted", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.UpdateDocLineIdFromServer), name:"UpdateDocLineIdFromServer", object: nil)
        let uploadData : UploadDatas = UploadDatas()
        uploadData.updateOfflineDataToServer(true)
    }
    
    func OrderDetailsDetailSyncCompleted(notification: NSNotification!)
    {
        
        print("fabButtonMode: " + String(fabButtonMode))
        
        switch fabButtonMode
        {
        case -1: //Refresh page
            pageMode = false
            appDelegate.isNewLine = false
            existingsDocLine = selectedDocLine
            hideProgress()
            SetUpdatedSalesHeaderID(notification)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "UpdateDocLineIdFromServer", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "OrderDetailsDetailSyncCompleted", object: nil)
            break;
        case 1: // Cancel & Review
            
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "UpdateDocLineIdFromServer", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "OrderDetailsDetailSyncCompleted", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "SendToSupplierDetailSyncCompleted", object: nil)
            
            if appDelegate.selectedDocId != -1
            {
                moveToOrderListPage()
            }
            else
            {
                self.navigationController?.popToRootViewControllerAnimated(true)
            }
            
            break;
            
        case 2: // Save & Review
            self.appDelegate.isNewOrder = true
            self.appDelegate.isNewLine = true
            hideProgress()
            
            SetUpdatedSalesHeaderID(notification)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "UpdateDocLineIdFromServer", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "OrderDetailsDetailSyncCompleted", object: nil)
            moveToOrderListPage()
            
            break;
        case 3: // New Order
            self.appDelegate.isNewOrder = true
            self.appDelegate.isNewLine = true
            hideProgress()
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "UpdateDocLineIdFromServer", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "OrderDetailsDetailSyncCompleted", object: nil)
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcSupplierID") as! SupplierViewController
            self.navigationController?.pushViewController(setViewController, animated: true)
            
            break;
        case 4: // New Line & with change
            hideProgress()
            self.appDelegate.isNewOrder = false
            self.appDelegate.isNewLine = true
            
            SetUpdatedSalesHeaderID(notification)
            
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "UpdateDocLineIdFromServer", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "OrderDetailsDetailSyncCompleted", object: nil)
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcProductTypeID") as! ProductTypeViewController
            setViewController.selectedSupplier = selectedSupplier
            setViewController.selectedDocType = selectedDocType
            setViewController.moveToRoot = true
            self.navigationController?.pushViewController(setViewController, animated: true)
            
            break;
        case 5: // New Line & with out change
            self.appDelegate.isNewOrder = false
            self.appDelegate.isNewLine = true
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcProductTypeID") as! ProductTypeViewController
            setViewController.selectedSupplier = selectedSupplier
            setViewController.selectedDocType = selectedDocType
            setViewController.moveToRoot = true
            self.navigationController?.pushViewController(setViewController, animated: true)
            
            break;
        case 6: // Save & Review & without change
            self.appDelegate.isNewOrder = true
            self.appDelegate.isNewLine = true
            moveToOrderListPage()
            break;
        case 7: // Move Prevoius doc line
            hideProgress()
            moveToPrevDocLine()
            break;
        case 8: // Move Next doc line
            hideProgress()
            moveToNextDocLine()
            break;
        case 9: // New Order & without change
            self.appDelegate.isNewOrder = true
            self.appDelegate.isNewLine = true
            hideProgress()
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcSupplierID") as! SupplierViewController
            self.navigationController?.pushViewController(setViewController, animated: true)
            
            break;
        case 10:
            
            pageMode = false
            appDelegate.isNewLine = false
            existingsDocLine = selectedDocLine
            hideProgress()
            SetUpdatedSalesHeaderID(notification)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "UpdateDocLineIdFromServer", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "OrderDetailsDetailSyncCompleted", object: nil)
            
            
//            if(Reachability.isConnectedToNetwork())
//            {
//                let uploadData : UploadDatas = UploadDatas()
//                uploadData.getSLRDetails(true, isDirectCall: true)
//            }
            
            break;
        default:
            
            break;
        }
    }
    
    func UpdateDocLineIdFromServer(notification: NSNotification)
    {
        let dict = notification.object as! NSDictionary
        
        let lineId : Int = dict.objectForKey("lineId") as! Int
        
        let lineSId : Int = dict.objectForKey("lineSId") as! Int
        
        if lineId == selectedDocLine.LineId
        {
            selectedDocLine.LineId = lineSId
        }
    }
    
    func SetUpdatedSalesHeaderID(notification: NSNotification!)
    {
        if notification != nil
        {
            let dict = notification.object as! NSDictionary
            
            let DocId : Int = dict.objectForKey("DocId") as! Int
            
            if DocId != 0
            {
                let DocSId : Int = dict.objectForKey("DocSId") as! Int
                
                if DocId != DocSId
                {
                    appDelegate.selectedDocId = DocSId
                }
            }
        }
    }
    
    func DeleteLineFromDocList()
    {
        if arrDocLinestList.count > 1
        {
            dbConn.deleteDocLine(arrDocLinestList[selectedArrIndex].LineId)
            arrDocLinestList.removeAtIndex(selectedArrIndex)
            
            if selectedArrIndex > arrDocLinestList.count-1
            {
               selectedArrIndex -= 1
            }
            
            selectedDocLine = arrDocLinestList[selectedArrIndex]
            
            existingsDocLine = ConvertClassToClass.convertDocLineToDocLine(selectedDocLine)
            
            loadDocLineDetails()
        }
        else
        {
            dbConn.deleteDocLine(arrDocLinestList[selectedArrIndex].LineId)
            arrDocLinestList.removeAtIndex(selectedArrIndex)
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
    
    func validateControls() -> Bool
    {
        if(txtQty.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) == "")
        {
            let sysMsg : SystemMessage = dbConn.getMessageUsingId(197)
            showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 6) // 197
            return false
        }
        else if(Int(txtQty.text!)! == 0)
        {
            let sysMsg : SystemMessage = dbConn.getMessageUsingId(198)
            showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 6) //198
            return false
        }
        else if(txtQtyUnit.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) == "")
        {
            let sysMsg : SystemMessage = dbConn.getMessageUsingId(199)
            showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 1) //199
            return false
        }
        
        return true
    }
    
    func moveToPrevDocLine()
    {
        if selectedArrIndex > 0
        {
            selectedArrIndex -= 1
            
            selectedDocLine = arrDocLinestList[selectedArrIndex]
            
            existingsDocLine = ConvertClassToClass.convertDocLineToDocLine(selectedDocLine)
            
            loadDocLineDetails()
        }
    }
    
    func moveToNextDocLine()
    {
        if selectedArrIndex < arrDocLinestList.count-1
        {
            selectedArrIndex += 1
            
            selectedDocLine = arrDocLinestList[selectedArrIndex]
            
            existingsDocLine = ConvertClassToClass.convertDocLineToDocLine(selectedDocLine)
            
            loadDocLineDetails()
        }
    }
    
    func showPopoverivew(sender: AnyObject?, pagemode: Int, selectedIndex: Int)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        contentViewController = storyboard.instantiateViewControllerWithIdentifier("vcSearchPopID") as! SearchPopViewController
        contentViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
        
        let width : CGFloat = (self.view.frame.width / 2) - 10.0
        contentViewController.preferredContentSize = CGSizeMake(width,90)
        
        detailPopover = contentViewController.popoverPresentationController!
        detailPopover.delegate = self
        if pagemode == 0 || pagemode == 1
        {
            contentViewController.lstContainers = arrContainers
            detailPopover.sourceView = sender as! UITextField
            detailPopover.sourceRect = (sender as! UITextField).bounds
        }
        else if pagemode == 2
        {
            contentViewController.lstContainers = arrPriceContainers
            detailPopover.sourceView = sender as! UILabel
            detailPopover.sourceRect = (sender as! UILabel).bounds
        }
        else if pagemode == 3
        {
            contentViewController.lstVarieties =  arrProdVarity
            detailPopover.sourceView = sender as! UITextField
            detailPopover.sourceRect = (sender as! UITextField).bounds
        }
        
        contentViewController.selectedIndex = selectedIndex
        
        contentViewController.pageMode = pagemode
        contentViewController.myParentVC = self
        

        detailPopover.permittedArrowDirections = UIPopoverArrowDirection.Up
        presentViewController(contentViewController, animated: true, completion:nil)
    }
    
    func fillPopoverivew(pagemode: Int, container: Containers!, prodVarity : ProductVarieties!, selectedIndex: Int)
    {
        if pagemode == 0
        {
            txtQtyUnit.text = container.CidDesc
            txtQtyUnit.tag = container.Cid
            txtQtyUnit.resignFirstResponder()
            selectedQtyUnitIndex = selectedIndex
        }
        else if pagemode == 1
        {
            txtContentUnit.text = container.CidDesc
            txtContentUnit.tag = container.Cid
            txtContentUnit.resignFirstResponder()
            selectedContentUnitIndex = selectedIndex
        }
        else if pagemode == 2
        {
            txtPriceUnit.text = container.CidDesc
            txtPriceUnit.tag = container.Cid
            txtPriceUnit.resignFirstResponder()
            selectedPriceUnitIndex = selectedIndex
        }
        else if pagemode == 3
        {
            selectedProdVerity = prodVarity
            txtProdVarity.text = prodVarity.VarietyDesc
            txtProdVarity.tag = prodVarity.VarietyId
            txtProdVarity.resignFirstResponder()
            selectedProdVarietyIndex = selectedIndex
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func txtPriceUnitTabed()
    {
        existingPriceUnit = txtPriceUnit.text!
        txtPriceUnit.text = ""
        
//        txtPriceUnit.inputView = nil;
//        txtPriceUnit.inputAccessoryView = nil;
        
        refreshPriceUnitsArray()
        self.showPopoverivew(txtPriceUnit, pagemode: 2, selectedIndex: selectedPriceUnitIndex)

    }
    
    // MARK: - Pickerview Delegate
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if txtPriceUnit.isFirstResponder()
        {
            return arrPriceContainers.count
        }
        else
        {
            return arrContainers.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if(txtPriceUnit.isFirstResponder())
        {
            return arrPriceContainers[row].CidDesc
        }
        else
        {
            return arrContainers[row].CidDesc
        }
    }
    
    // MARK: - Events
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func selectByTouch() {
        Selected();
    }
    
    func Cancelled()
    {
        if(txtPriceUnit.isFirstResponder())
        {
            txtPriceUnit.resignFirstResponder()
        }
        else if(txtContentUnit.isFirstResponder())
        {
            txtContentUnit.resignFirstResponder()
        }
        else if(txtQtyUnit.isFirstResponder())
        {
            txtQtyUnit.resignFirstResponder()
        }
        else if(txtQty.isFirstResponder())
        {
            txtQty.resignFirstResponder()
        }
        else if(txtPrice.isFirstResponder())
        {
            txtPrice.resignFirstResponder()
        }
        else if(txtContents.isFirstResponder())
        {
            txtContents.resignFirstResponder()
        }
        else if(txtDeliveryRef.isFirstResponder())
        {
            txtDeliveryRef.resignFirstResponder()
        }
        else if(txtSupplierRef.isFirstResponder())
        {
            txtSupplierRef.resignFirstResponder()
        }
    }
    
    func Selected() {
        
        if(txtPriceUnit.isFirstResponder())
        {
            selectedPriceUnitIndex = pickerView.selectedRowInComponent(0)
            let option = arrPriceContainers[pickerView.selectedRowInComponent(0)]
            txtPriceUnit.text = option.CidDesc
            txtPriceUnit.tag = arrPriceContainers[pickerView.selectedRowInComponent(0)].Cid
            txtPriceUnit.resignFirstResponder()
        }
        else if(txtContentUnit.isFirstResponder())
        {
            selectedContentUnitIndex = pickerView.selectedRowInComponent(0)
            let option = arrContainers[pickerView.selectedRowInComponent(0)]
            txtContentUnit.text = option.CidDesc
            txtContentUnit.tag = arrContainers[pickerView.selectedRowInComponent(0)].Cid
            txtContentUnit.resignFirstResponder()
            refreshPriceUnitsArray()
        }
        else if(txtQtyUnit.isFirstResponder())
        {
            selectedQtyUnitIndex = pickerView.selectedRowInComponent(0)
            let option = arrContainers[pickerView.selectedRowInComponent(0)]
            txtQtyUnit.text = option.CidDesc
            txtQtyUnit.tag = arrContainers[pickerView.selectedRowInComponent(0)].Cid
            txtQtyUnit.resignFirstResponder()
            refreshPriceUnitsArray()
        }
        else if(txtQty.isFirstResponder())
        {
            txtQty.resignFirstResponder()
        }
        else if(txtPrice.isFirstResponder())
        {
            txtPrice.resignFirstResponder()
        }
        else if(txtContents.isFirstResponder())
        {
            txtContents.resignFirstResponder()
        }
        else if(txtDeliveryRef.isFirstResponder())
        {
            txtDeliveryRef.resignFirstResponder()
        }
        else if(txtSupplierRef.isFirstResponder())
        {
            txtSupplierRef.resignFirstResponder()
        }
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        print("FAB Opened")
    }
    
    func KCFABClosed(fab: KCFloatingActionButton) {
        print("FAB Closed")
    }
    
    func BackButtonPressed()
    {
        if activeTextField != nil
        {
            activeTextField.resignFirstResponder()
        }
        
        if !CompareExistingClassValues()
        {
            let sysMsg : SystemMessage = dbConn.getMessageUsingId(200)
            showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 2) //200
        }
        else
        {
            if fabButtonMode == -1 || fabButtonMode == 10
            {
                moveToOrderListPage()
            }
            else
            {
                self.navigationController?.popViewControllerAnimated(true)
            }
        }
    }
    
    func moveToOrderListPage()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcOrderListID") as! OrderListViewController
        setViewController.DocId = appDelegate.selectedDocId
        setViewController.SupplierId = selectedSupplier.EntityId
        setViewController.moveToRoot = true
        self.navigationController?.pushViewController(setViewController, animated: true)
    }
    
    @IBAction func txtProdVarity_EditDidBegin(sender: AnyObject)
    {
        existingProdVarity = txtProdVarity.text!
        txtProdVarity.text = ""
        self.showPopoverivew(sender, pagemode: 3, selectedIndex: selectedProdVarietyIndex)
    }
    
    @IBAction func txtProdVarity_EditingChanged(sender: AnyObject)
    {
//        if txtProdVarity.text != ""
//        {
//            let arr: NSMutableArray  =  NSMutableArray(array: arrProdVarity)
//            
//            let preRef1 : NSPredicate = NSPredicate(format: "VarietyDesc CONTAINS[c] %@", txtProdVarity.text!);
//            let groupnames : [ProductVarieties] = arr.filteredArrayUsingPredicate(preRef1) as! [ProductVarieties]
//            
//            contentViewController.lstVarieties = groupnames
//        }
//        else
//        {
//            contentViewController.lstVarieties = arrProdVarity
//        }
    }
    
    @IBAction func txtQtyUnit_EditDidBegin(sender: AnyObject)
    {
        existingQtyUnit = txtQtyUnit.text!
        txtQtyUnit.text = ""
        self.showPopoverivew(sender, pagemode: 0, selectedIndex: selectedQtyUnitIndex)
        
//        self.createinputpicker()
//        pickerView.selectRow(selectedQtyUnitIndex, inComponent: 0, animated: true)
    }
    
    @IBAction func txtQtyUnit_EditingChanged(sender: AnyObject)
    {
        if txtQtyUnit.text != ""
        {
            let arr: NSMutableArray  =  NSMutableArray(array: arrContainers)
            
            let preRef1 : NSPredicate = NSPredicate(format: "CidDesc CONTAINS[c] %@", txtQtyUnit.text!);
            let groupnames : [Containers] = arr.filteredArrayUsingPredicate(preRef1) as! [Containers]
            
            contentViewController.lstContainers = groupnames 
        }
        else
        {
            contentViewController.lstContainers = arrContainers
        }
        
        contentViewController.tvSearchView.reloadData()
    }
    
    
    @IBAction func txtContentUnit_EditDidBegin(sender: AnyObject)
    {
        existingContentUnit = txtContentUnit.text!
        txtContentUnit.text = ""
        self.showPopoverivew(sender, pagemode: 1, selectedIndex: selectedContentUnitIndex)
//        self.createinputpicker()
//        pickerView.selectRow(selectedContentUnitIndex, inComponent: 0, animated: true)
    }
    
    @IBAction func txtContentUnit_EditingChanged(sender: AnyObject)
    {
        if txtContentUnit.text != ""
        {
            let arr: NSMutableArray  =  NSMutableArray(array: arrContainers)
            
            let preRef1 : NSPredicate = NSPredicate(format: "CidDesc CONTAINS[c] %@", txtContentUnit.text!);
            let groupnames : [Containers] = arr.filteredArrayUsingPredicate(preRef1) as! [Containers]
            
            contentViewController.lstContainers = groupnames
        }
        else
        {
            contentViewController.lstContainers = arrContainers
        }
        
        contentViewController.tvSearchView.reloadData()
    }
    
    @IBAction func txtPriceUnit_EditDidBegin(sender: AnyObject)
    {
//        existingPriceUnit = txtPriceUnit.text!
//        txtPriceUnit.text = ""
//        
//        txtPriceUnit.inputView = nil;
//        txtPriceUnit.inputAccessoryView = nil;
//        
//        refreshPriceUnitsArray()
//        self.showPopoverivew(sender, pagemode: 2, selectedIndex: selectedPriceUnitIndex)
        
//        self.createinputpicker()
//        pickerView.selectRow(selectedPriceUnitIndex, inComponent: 0, animated: true)
    }
    
    @IBAction func txtPriceUnit_EditingChanged(sender: AnyObject)
    {
        
//        txtPriceUnit.inputView = nil;
//        txtPriceUnit.inputAccessoryView = nil;
//        
//        if txtPriceUnit.text != ""
//        {
//            let arr: NSMutableArray  =  NSMutableArray(array: arrPriceContainers)
//            
//            let preRef1 : NSPredicate = NSPredicate(format: "CidDesc CONTAINS[c] %@", txtPriceUnit.text!);
//            let groupnames : [Containers] = arr.filteredArrayUsingPredicate(preRef1) as! [Containers]
//            
//            contentViewController.lstContainers = groupnames
//        }
//        else
//        {
//            contentViewController.lstContainers = arrPriceContainers
//        }
//        
//        contentViewController.tvSearchView.reloadData()
    }
    
    @IBAction func txtField_EditDidBegin(sender: AnyObject)
    {
        mytoolbar = UIToolbar(frame: CGRectMake(0,0,320,44))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.Cancelled))
        cancelButton.setTitleTextAttributes(GlobalConstants.barButtonAttribute, forState: UIControlState.Normal)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.Selected))
        doneButton.setTitleTextAttributes(GlobalConstants.barButtonAttribute, forState: UIControlState.Normal)
        
        mytoolbar.setItems([spaceButton, cancelButton, doneButton], animated: true)
        
        let selectedTextField = sender as! UITextField
        
        selectedTextField.inputAccessoryView = mytoolbar;
    }
    
    @IBAction func btnPurchase_Click(sender: AnyObject)
    {
        fabButtonMode = 10
        
        if saveDocLineDetails()
        {
            btnPurchase.enabled = false
            btnPurchase.alpha = 0.6
            
            // DLineID -> consider as OrderId (DocLines)
            dbConn.insertSignalRStatus(selectedDocLine.OrderId)
            
            sendDataToOnline()
            
            updateDocLineStatus(AppDelegate.DLStatus.PingWithP.rawValue, bStatus : 0)
            
            hideConfirmAndPurchaseButton()
        }
    }
    
    @IBAction func btnConfirmLine_Click(sender: AnyObject)
    {
        fabButtonMode = -1
        
        if saveDocLineDetails()
        {
            btnConfirmLine.enabled = false
            btnConfirmLine.alpha = 0.6
            
            sendDataToOnline()
            
            updateDocLineStatus(AppDelegate.DLStatus.GreenWithC.rawValue, bStatus : 1)
            
            hideConfirmAndPurchaseButton()
            
            self.checkAllLinesConfirmed()
        }
    }
    
    
    @IBAction func btnPrevious_Click(sender: AnyObject)
    {
        if CompareExistingClassValues()
        {
            moveToPrevDocLine()
        }
        else
        {
            let sysMsg : SystemMessage = dbConn.getMessageUsingId(201)
            showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 7) //201
        }
    }
    
    @IBAction func btnNext_Click(sender: AnyObject)
    {
        if CompareExistingClassValues()
        {
            moveToNextDocLine()
        }
        else
        {
            let sysMsg : SystemMessage = dbConn.getMessageUsingId(201)
            showAlertWindow(sysMsg.SystemMessageType.uppercaseString, message: sysMsg.SystemMessageDetails, tag: 8) //201
        }
    }
    
    @IBAction func txtSizeCount_EditDidBegin(sender: AnyObject)
    {
        txtSizeCount.resignFirstResponder()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcSizeCountID") as! SizeCountViewController
        setViewController.prodTypeId = selectedProdType != nil ? selectedProdType.ProdTypeId : selectedDocLine.ProdTypeId
        self.navigationController?.pushViewController(setViewController, animated: true)
    }
    
    // MARK: - UIPopoverPresentationController Delegate
    
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    
    func presentationController(controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        let navigationController = UINavigationController(rootViewController: controller.presentedViewController)
        //        let btnDone = UIBarButtonItem(title: "Done", style: .Done, target: self, action: "dismiss")
        //        navigationController.topViewController!.navigationItem.rightBarButtonItem = btnDone
        return navigationController
    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController)
    {
        // popover = nil
        
        if txtQtyUnit.isFirstResponder()
        {
           txtQtyUnit.resignFirstResponder()
        }
        else if txtContentUnit.isFirstResponder()
        {
           txtContentUnit.resignFirstResponder()
        }
        else if txtProdVarity.isFirstResponder()
        {
            txtProdVarity.resignFirstResponder()
        }
        else
        {
            txtPriceUnit.text = existingPriceUnit
        }
    }

    
    // MARK: - UITextField Delegate
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        activeTextField = nil
        
//        if textField == txtPriceUnit
//        {
//            self.dismissViewControllerAnimated(true, completion: nil)
//            
//            if txtPriceUnit.text == ""
//            {
//               txtPriceUnit.text = existingPriceUnit
//            }
//        }
//        else
        
        if textField == txtContentUnit
        {
            refreshPriceUnitsArray()
            self.dismissViewControllerAnimated(true, completion: nil)
            
            if txtContentUnit.text == ""
            {
               txtContentUnit.text = existingContentUnit
            }
        }
        else if textField == txtQtyUnit
        {
            refreshPriceUnitsArray()
            self.dismissViewControllerAnimated(true, completion: nil)
            
            if txtQtyUnit.text == ""
            {
               txtQtyUnit.text = existingQtyUnit
            }
        }
        else if(textField == txtProdVarity)
        {
            self.dismissViewControllerAnimated(true, completion: nil)
            
            if txtProdVarity.text == "" {
               txtProdVarity.text = existingProdVarity
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return false
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if(textField == txtQty)
        {
            let inverseSet = NSCharacterSet(charactersInString:"-0123456789").invertedSet
            let components = string.componentsSeparatedByCharactersInSet(inverseSet)
            let filtered = components.joinWithSeparator("")
            
            guard let text = textField.text else { return true }
            
            if(text.characters.count >= 10 && string != "")
            {
                return false
            }
            
            return string == filtered
        }
        if(textField == txtContents || textField == txtDeliveryRef)
        {
            let inverseSet = NSCharacterSet(charactersInString:"0123456789").invertedSet
            let components = string.componentsSeparatedByCharactersInSet(inverseSet)
            let filtered = components.joinWithSeparator("")
            
            guard let text = textField.text else { return true }
            
            if(text.characters.count >= 10 && string != "")
            {
                return false
            }
            
            return string == filtered
        }
//        else if(textField == txtContentUnit || textField == txtPriceUnit || textField == txtQtyUnit)
//        {
//            return false
//        }
        else if(textField == txtPrice)
        {
            let inverseSet = NSCharacterSet(charactersInString:"0123456789.").invertedSet
            let components = string.componentsSeparatedByCharactersInSet(inverseSet)
            let filtered = components.joinWithSeparator("")
            
            guard let text = textField.text else { return true }
            
            if(text.characters.count >= 10 && string != "")
            {
                return false
            }
            
            if string != ""
            {
                if let input = textField.text
                {
                    let numberFormatter = NSNumberFormatter()
                    let range = input.rangeOfString(numberFormatter.decimalSeparator)
                    if let r = range {
                        let endIndex = input.startIndex.advancedBy(input.startIndex.distanceTo(r.endIndex))
                        let decimals = input.substringFromIndex(endIndex)
                        return decimals.characters.count < 2
                    }
                }
            }
            
            return string == filtered
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        activeTextField = textField
        if(textField == txtQty)
        {
            textField.selectedTextRange = textField.textRangeFromPosition(textField.beginningOfDocument, toPosition: textField.endOfDocument)
        }
        else if (textField == txtPrice)
        {
            txtPrice?.text = "";
        }
        else if (textField ==  txtContents)
        {
            txtContents?.text = ""
        }
    }
    
    private func startObservingKeyboardEvents()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWasShown(_:)), name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWillBeHidden(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
    }
    
    private func stopObservingKeyboardEvents()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWasShown (notification: NSNotification)
    {
        let userInfo : NSDictionary = notification.userInfo!
        if let keyboardSize: CGSize = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size
        {
            let insets: UIEdgeInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height+10, 0)
            scrlView.contentInset = insets
            scrlView.scrollIndicatorInsets = insets
            
            scrlView.contentOffset = CGPointMake(scrlView.contentOffset.x, scrlView.contentOffset.y ) //+ keyboardSize.height
        }
    }
    
    func keyboardWillBeHidden (notification: NSNotification)
    {
        let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
        self.scrlView.contentInset = contentInsets
        self.scrlView.scrollIndicatorInsets = contentInsets
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
