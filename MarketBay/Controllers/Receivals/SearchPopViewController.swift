//
//  SearchPopViewController.swift
//  MarketBay
//
//  Created by Apple on 05/10/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class SearchPopViewController: UIViewController {

    
    @IBOutlet weak var tvSearchView: UITableView!
    
    var lstContainers : [Containers]  = [Containers]()
    
    var lstVarieties : [ProductVarieties]  = [ProductVarieties]()
    
    var pageMode : Int = 0
    
    var selectedIndex : Int = 0
    
    var myParentVC : CreateOrderViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tvSearchView.reloadData()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        let rowToSelect : NSIndexPath = NSIndexPath(forRow: selectedIndex, inSection: 0);
//        self.tvSearchView.scrollToRowAtIndexPath(rowToSelect, atScrollPosition: UITableViewScrollPosition.Middle, animated: true)
        self.tvSearchView.selectRowAtIndexPath(rowToSelect, animated: true, scrollPosition: UITableViewScrollPosition.Middle)
        tvSearchView.setContentOffset(CGPointMake(0, CGFloat((selectedIndex-1) * 30)), animated: true)
    }

    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if pageMode != 3
        {
            return lstContainers.count
        }
        else
        {
            return lstVarieties.count
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 30.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ContentCell", forIndexPath: indexPath)
        
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if pageMode != 3
        {
            let objContainers = lstContainers[indexPath.row]
            cell.textLabel?.text = objContainers.CidDesc
        }
        else
        {
            let objVar = lstVarieties[indexPath.row]
            cell.textLabel?.text = objVar.VarietyDesc
        }
        
        cell.textLabel?.font = UIFont(name: "Ubuntu", size: 12.0)
        cell.textLabel?.textColor = UIColor.lightGrayColor()
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
         
        if pageMode != 3
        {
            let objContainers = lstContainers[indexPath.row]
            myParentVC.fillPopoverivew(pageMode, container: objContainers, prodVarity: nil, selectedIndex: indexPath.row)
        }
        else
        {
            let objVar = lstVarieties[indexPath.row]
            myParentVC.fillPopoverivew(pageMode, container: nil, prodVarity: objVar, selectedIndex: indexPath.row)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
