//
//  OrderMasterViewController.swift
//  MarketBay
//
//  Created by Apple on 20/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import DrawerController
import KCFloatingActionButton
import MRProgress

class OrderMasterViewController : UIViewController , KCFloatingActionButtonDelegate
{
 
    @IBOutlet weak var tvOrderList: UITableView!
    
    var fab: KCFloatingActionButton!
    
    var arrTrasnPostList : [TransPost]  = [TransPost]()
    
    var appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    let defaults = NSUserDefaults.standardUserDefaults()
    
    
    override func viewWillAppear(animated: Bool)
    {
        getTransPostDetails()
        
        loadFloatingActionButtons()
        
        if !appDelegate.isSubscribed
        {
            self.appDelegate.mySLRConnection.start()
            
            if self.appDelegate.tempTimer == nil
            {
                self.appDelegate.tempTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self.appDelegate, selector: #selector(self.appDelegate.reSubscribeUser), userInfo: nil, repeats: true)
            }
        }
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        if fab != nil
        {
            fab.removeFromSuperview()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupLeftMenuButton()
        
        self.title = "ORDER LIST"
    }

    func setupLeftMenuButton() {
        
        let leftDrawerButton = DrawerBarButtonItem(target: self, action: #selector(self.leftDrawerButtonPress(_:)))
        leftDrawerButton.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem = leftDrawerButton
    }
    
    func leftDrawerButtonPress(sender: AnyObject?) {
        self.evo_drawerController?.toggleDrawerSide(.Left, animated: true, completion: nil)
    }
    
    func getTransPostDetails()
    {
        if appDelegate.selectedBuyerId != ""
        {
            let dbConn : DBConnection = DBConnection.instance
            arrTrasnPostList = dbConn.getTransPostDetails(Int(appDelegate.selectedBuyerId)!)
            tvOrderList.reloadData()
        }
    }
    
    func loadFloatingActionButtons()
    {
        fab = KCFloatingActionButton()
        
        fab.buttonColor = GlobalConstants.progressTitleColor
        fab.plusColor = UIColor.whiteColor()
        fab.overlayColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255, alpha: 0.8)
        
        fab.size = 40
        fab.itemSize = 40
        
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    func emptyKCFABSelected(fab: KCFloatingActionButton)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcSupplierID") as! SupplierViewController
        
        self.appDelegate.isNewOrder = true
        self.appDelegate.isNewLine = true
        
        self.navigationController?.pushViewController(setViewController, animated: true)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTrasnPostList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ContentCell", forIndexPath: indexPath) as! OrderListTVCell
        
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        let objTranpost : TransPost = arrTrasnPostList[indexPath.row]
        
        cell.lblSupplier.text = objTranpost.SupplierMarketCode
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = GlobalConstants.localDBDateFormat
        
        let docCreatedDate : NSDate = dateFormatter.dateFromString(objTranpost.CreateTime)!
        
        dateFormatter.dateFormat = GlobalConstants.localAppDateFormat
        
        cell.lblDate.text = dateFormatter.stringFromDate(docCreatedDate)
        
        dateFormatter.dateFormat = GlobalConstants.localAppTimeFormat
        
        cell.lblTime.text = dateFormatter.stringFromDate(docCreatedDate)
        
        cell.lblLines.text = String(objTranpost.LineCount)
        
        if objTranpost.locStatusId == 4
        {
            cell.ivStatus.image = UIImage(named: "icStatusGreen")
            cell.lblStatus.text = "C"
        }
        else
        {
            cell.ivStatus.image = UIImage(named: "icStatusYellow")
        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        let objTranpost : TransPost = arrTrasnPostList[indexPath.row]
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcOrderListID") as! OrderListViewController
        
        setViewController.DocId = objTranpost.DocId
        
        setViewController.SupplierId = objTranpost.SupplierId
        
        self.navigationController?.pushViewController(setViewController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
