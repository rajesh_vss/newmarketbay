//
//  MenuTableViewController.swift
//  MarketBay
//
//  Created by Apple on 20/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBuyerName: UILabel!
    @IBOutlet weak var btnSwithcBuyer: UIButton!
    
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    
    var appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet weak var heightConstriant: NSLayoutConstraint!
    
    @IBOutlet weak var vwFooterView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = "Welcome " + appDelegate.UserRealName
        
        let vwVersion : UIView = UIView(frame: CGRectMake(0.0, appDelegate.screenHeight - 30, 280, 30.0))
        self.tableView.addSubview(vwVersion)
        
        let nsObject: AnyObject? = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"]
        
        let textView = UILabel(frame: CGRectMake(0.0, 5, 280, 20.0))
        textView.text = "Version " + (nsObject as! String)
        textView.textColor = UIColor.lightGrayColor()
        textView.backgroundColor = UIColor.clearColor()
        textView.font = UIFont(name: "Ubuntu", size: 10)
        textView.textAlignment = .Center
        vwVersion.addSubview(textView)
        
        btnSwithcBuyer.layer.borderWidth = 0.5
        btnSwithcBuyer.layer.cornerRadius = 2
        btnSwithcBuyer.layer.borderColor = UIColor.whiteColor().CGColor
        
         
        let buyerName : String = defaults.objectForKey("selectedMarketCode") as! String != "" ? defaults.objectForKey("selectedMarketCode") as! String : " - "
        lblBuyerName.text =  "Buyer: " +  buyerName
        
        let dbConnection : DBConnection = DBConnection.instance
        
        let arrBuyerHeader = dbConnection.getBuyerList()
        
        if arrBuyerHeader.count > 1
        {
//            btnSwithcBuyer.hidden = false
            topConstraint.constant = 20
            btnSwithcBuyer.layoutIfNeeded()
        }
        else
        {
            btnSwithcBuyer.hidden = true
            topConstraint.constant = 40
            btnSwithcBuyer.layoutIfNeeded()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func LogoutUser(title :String, message:String)
    {
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {action in
                self.appDelegate.logoutUserFromApp()
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    @IBAction func btnSwithcBuyer_Click(sender: AnyObject)
    {
        if defaults.objectForKey("selectedMarketCode") as! String != ""
        {
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC = storyboard.instantiateViewControllerWithIdentifier("vcBuyerSelectionID") as! BuyerSelectionViewController
            loginVC.pageMode = "Change"
            let navigationController : UINavigationController = UINavigationController(rootViewController: loginVC)
            
            appDelegate.drawerController?.setCenterViewController(navigationController, withCloseAnimation: true, completion: nil)
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if appDelegate.selectedBuyerId == ""
        {
            return 1
        }
        else
        {
            return 3
        }
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("menuCell", forIndexPath: indexPath) as! MenuTVCell

        if appDelegate.selectedBuyerId == ""
        {
            cell.ivImage.image = UIImage(named: "icLogout")
            cell.lblTitle.text = "Logout"
        }
        else
        {
            switch indexPath.row {
            case 0:
                cell.ivImage.image = UIImage(named: "icPurchase")
                cell.lblTitle.text = "Purchases"
                break;
            case 1:
                cell.ivImage.image = UIImage(named: "icSettings")
                cell.lblTitle.text = "Settings"
                break;
            case 2:
                cell.ivImage.image = UIImage(named: "icLogout")
                cell.lblTitle.text = "Logout"
                break;
            default:
                break;
            }
        }

        return cell
    }
 
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if appDelegate.selectedBuyerId == ""
        {
            LogoutUser("",message: "Are you sure want to logout?")
        }
        else
        {
            switch indexPath.row {
            case 0:
                let mainViewController = storyboard.instantiateViewControllerWithIdentifier("vcOrderMasterID")
                let nav = UINavigationController(rootViewController: mainViewController)
                self.evo_drawerController?.setCenterViewController(nav, withCloseAnimation: true, completion: nil)
                break;
            case 1:
                let mainViewController = storyboard.instantiateViewControllerWithIdentifier("vcSettingsID")
                let nav = UINavigationController(rootViewController: mainViewController)
                self.evo_drawerController?.setCenterViewController(nav, withCloseAnimation: true, completion: nil)
                break;
            case 2:
                LogoutUser("",message: "Are you sure want to logout?")
                break;
            default:
                break;
            }
        }
    }

    

}
