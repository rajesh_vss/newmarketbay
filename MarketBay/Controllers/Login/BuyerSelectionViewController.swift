//
//  BuyerSelectionViewController.swift
//  MarketBay
//
//  Created by Apple on 19/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import DrawerController

class BuyerSelectionViewController: UIViewController {

    @IBOutlet weak var tvBuyerView: UITableView!
    
    @IBOutlet weak var tvBHHeightCons : NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblSubTitle: UILabel!
    
    var appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    var arrBuyerHeader:[UserRole]  = [UserRole]()
    
    var pageMode : String = "New"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
 
        self.title = "MARKETBAY"
        
        lblTitle.text = "Welcome " + appDelegate.UserRealName
        
        tvBuyerView.hidden = true
        lblSubTitle.hidden = true
        
        let downData : DownloadData = DownloadData()
        downData.loginDelegate = self
        downData.ShowProgress()
        downData.GetUserInRolesFromServer()
            {
                (result : String) in
                
                if result != ""
                {
                    self.showAlert(result, title: "")
                }
        }
        
        if pageMode != "New"
        {
            self.setupLeftMenuButton()
        }
    }
    
    func setupLeftMenuButton() {
        
        let leftDrawerButton = DrawerBarButtonItem(target: self, action: #selector(self.leftDrawerButtonPress(_:)))
        leftDrawerButton.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem = leftDrawerButton
    }
    
    func leftDrawerButtonPress(sender: AnyObject?) {
        self.evo_drawerController?.toggleDrawerSide(.Left, animated: true, completion: nil)
    }
    
    func getBuyerDetails()
    {
        tvBuyerView.hidden = false
        lblSubTitle.hidden = false
        
        let dbConnection : DBConnection = DBConnection()
        
        arrBuyerHeader = dbConnection.getBuyerList()
        
        let currentViewHeight : Float = Float(self.view.frame.height)
        
        var constantHeight : Float = 95.0
        
        constantHeight += (Float(arrBuyerHeader.count) * 44.0) + 2.0
        
        if constantHeight > currentViewHeight
        {
            tvBuyerView.removeConstraint(tvBHHeightCons)
        }
        else
        {
            constantHeight = constantHeight - 95
            tvBuyerView.scrollEnabled = false
            tvBHHeightCons.constant = CGFloat(constantHeight)
            tvBuyerView.layoutIfNeeded()
        }
        
        tvBuyerView.reloadData()
        
        if pageMode == "New"
        {
            if arrBuyerHeader.count == 1
            {
                let userrole = arrBuyerHeader[0]
                
                appDelegate.selectedBuyerId = String(userrole.EntityId)
                appDelegate.selectedMarketCode = userrole.MarketCode
                
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(String(userrole.EntityId), forKey: "selectedBuyerId")
                defaults.setObject(userrole.MarketCode, forKey: "selectedMarketCode")
                defaults.setObject("no", forKey: "NoBuyers")
                defaults.synchronize()
                
                loadDrawerController()
            }
            else if arrBuyerHeader.count == 0
            {
                self.showAlert("You require BuyerAdmin or BuyerSalesman role to access", title: "")
            }
        }
    }
    
    func loadDrawerController()
    {
        appDelegate.loadDrawerController()
    }
    
    func showAlert(message : NSString, title: NSString)
    {
        let alertController = UIAlertController(title: title as String, message:
            message as String, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
            self.appDelegate.selectedBuyerId = ""
            self.appDelegate.selectedMarketCode = ""
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject("", forKey: "selectedBuyerId")
            defaults.setObject("", forKey: "selectedMarketCode")
            defaults.setObject("yes", forKey: "NoBuyers")
            defaults.synchronize()
            
            self.loadDrawerController()
        }))
        
        self.presentViewController(alertController, animated: true, completion:nil)
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrBuyerHeader.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ContentCell", forIndexPath: indexPath) as! BuyerSelectionTVCell
        
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        let userrole = arrBuyerHeader[indexPath.row]
        
        cell.lblBuyerCode.text = userrole.MarketCode + " (" + userrole.RoleName + ")"
        
//        cell.lblRole.text = userrole.RoleName
        
        cell.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0)
        cell.ivSelection.image = UIImage(named: "icUnselect")
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let cell : BuyerSelectionTVCell = tableView.cellForRowAtIndexPath(indexPath) as! BuyerSelectionTVCell
        
        cell.ivSelection.image = UIImage(named: "icSelect")
        
        let userrole = arrBuyerHeader[indexPath.row]
        
        appDelegate.selectedBuyerId = String(userrole.EntityId)
        appDelegate.selectedMarketCode = userrole.MarketCode
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(String(userrole.EntityId), forKey: "selectedBuyerId")
         defaults.setObject(userrole.MarketCode, forKey: "selectedMarketCode")
        defaults.setObject("no", forKey: "NoBuyers")
        defaults.synchronize()
        
        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(self.loadDrawerController), userInfo: nil, repeats: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
