//
//  PINViewController.swift
//  MarketBay
//
//  Created by Apple on 19/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class PINViewController: UIViewController {

    
    
    @IBOutlet weak var vwMainView: UIView!
    
    @IBOutlet weak var vwPin: UIView!
    
    @IBOutlet weak var mvHeightCons: NSLayoutConstraint!
    
    @IBOutlet weak var circleHeightCons: NSLayoutConstraint!
    
    @IBOutlet weak var circleWidthCons: NSLayoutConstraint!
    
    @IBOutlet weak var btnOne: UIButton!
    @IBOutlet weak var btnTwo: UIButton!
    @IBOutlet weak var btnThree: UIButton!
    @IBOutlet weak var btnFour: UIButton!
    @IBOutlet weak var btnFive: UIButton!
    @IBOutlet weak var btnSix: UIButton!
    @IBOutlet weak var btnSeven: UIButton!
    @IBOutlet weak var btnEight: UIButton!
    @IBOutlet weak var btnNine: UIButton!
    @IBOutlet weak var btnZero: UIButton!
    
    @IBOutlet weak var ivPinOne: UIImageView!
    @IBOutlet weak var ivPinTwo: UIImageView!
    @IBOutlet weak var ivPinThree: UIImageView!
    @IBOutlet weak var ivPinFour: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var middleConstraint: NSLayoutConstraint!
    
    var index : Int = 0
    var strNewPassword : NSString = NSString()
    var strConfirmPassword : NSString = NSString()
    var strCheckPassword : NSString = NSString()
    var strExistingPassword : NSString = NSString()
    var pageMode : String = "New"
    
    var pageFrom : String = ""
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    var passwordType : Int = 2
    // *** passwordType = 0 - New Pin *** //
    // *** passwordType = 1 - Confirm Pin *** //
    // *** passwordType = 2 - Check Pin *** //
    // *** passwordType = 3 - Change Pin *** //
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
 
        if (appDelegate.isPadDevice)
        {
            circleHeightCons.constant = 80
            circleWidthCons.constant = 80
        }
        else if(!appDelegate.isPadDevice && appDelegate.screenHeight > 568.0)
        {
            circleHeightCons.constant = 60
            circleWidthCons.constant = 60
        }
        else
        {
            circleHeightCons.constant = 50
            circleWidthCons.constant = 50
        }
        
        if (appDelegate.isAppAlive == nil)
        {
            appDelegate.isAppAlive = false
        }
        
        lblTitle.text = "Welcome " + appDelegate.UserRealName
        
        if passwordType == 0
        {
            lblSubTitle.text = "Set passcode for the day"
        }
        else if passwordType == 1
        {
            lblSubTitle.text = "Confirm passcode for the day"
        }
        else if passwordType == 2
        {
            lblSubTitle.text = "Enter the passcode"
        }
        else if passwordType == 3
        {
            lblSubTitle.text = "Enter passcode for the day"
        }
        
        mvHeightCons.constant = appDelegate.screenHeight
        vwMainView.layoutIfNeeded()
        
        if pageMode == "Change"
        {
            self.title = "CHANGE PASSCODE"
            let backButton: UIButton = UIButton(type: UIButtonType.Custom)
            backButton.setImage(UIImage(named: "icBackArrow"), forState: UIControlState.Normal)
            backButton.addTarget(self, action: #selector(self.BackButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
            backButton.frame = CGRectMake(0, 0, 50, 40)
            backButton.contentHorizontalAlignment = .Left
            let barbackButton = UIBarButtonItem(customView: backButton)
            self.navigationItem.leftBarButtonItem = barbackButton
            
            lblTitle.hidden = true
            
            middleConstraint.constant = -50
            vwMainView.layoutIfNeeded()
        }
    }
    
    func BackButtonPressed()
    {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    
    func setSelectedPinImage()
    {
        let redimage : UIImage = UIImage(named:"icSmallCircleSelected")!
        let image : UIImage = UIImage(named:"icSmallCircle")!
        
        switch index
        {
        case 0:
            ivPinOne.image = image
            ivPinTwo.image = image
            ivPinThree.image = image
            ivPinFour.image = image
            break;
            
        case 1:
            ivPinOne.image = redimage
            ivPinTwo.image = image
            ivPinThree.image = image
            ivPinFour.image = image
            break;
            
        case 2:
            ivPinOne.image = redimage
            ivPinTwo.image = redimage
            ivPinThree.image = image
            ivPinFour.image = image
            break;
            
        case 3:
            ivPinOne.image = redimage
            ivPinTwo.image = redimage
            ivPinThree.image = redimage
            ivPinFour.image = image
            break;
        case 4:
            ivPinOne.image = redimage
            ivPinTwo.image = redimage
            ivPinThree.image = redimage
            ivPinFour.image = redimage 
            break;
            
        default:
            
            break;
        }
    }
    
    func invokeAlertMethod(strTitle: String, strBody: String, delegate: AnyObject?, tag : Int)
    {
        let alertController = UIAlertController(title: strTitle, message: strBody, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion:nil)
    }
    
    func setHomePage()
    {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        if appDelegate.selectedBuyerId != "" || appDelegate.isNoBuyers
        {
            appDelegate.loadDrawerController()
        }
        else
        {
            let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcBuyerSelectionID") as! BuyerSelectionViewController
            
            let navController : UINavigationController = UINavigationController(rootViewController: setViewController)
            
            UIView.animateWithDuration(0.1, animations: {
                self.view.alpha = 0.0
                }, completion: {
                    (value: Bool) in
                    
                    setViewController.view.alpha = 1.0
                    self.view .removeFromSuperview()
                    self.appDelegate.window?.rootViewController = navController
            })
        }
    }
    
    func shakeView()
    {
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.04
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(vwPin.center.x - 20.0, vwPin.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(vwPin.center.x + 20.0, vwPin.center.y))
        vwPin.layer.addAnimation(animation, forKey: "position")
    }
    
    func moveToRootView()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    
    @IBAction func btnNumber_click(sender: AnyObject)
    {
        let button = sender as! UIButton
        
        button.highlighted = true
        button.selected = true
        
        let number : String = String(button.tag)
        
        if(passwordType == 0)
        {
            if(strNewPassword.length < 4)
            {
                if(strNewPassword.length > 0)
                {
                    strNewPassword = (strNewPassword as String) + number
                }
                else
                {
                    strNewPassword = number
                }
                
                index = strNewPassword.length
                setSelectedPinImage()
                
                if (strNewPassword.length == 4)
                {
                    passwordType = 1;
                    lblSubTitle.text =  "Confirm passcode for the day"
                    index = 0
                    NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(PINViewController.setSelectedPinImage), userInfo: nil, repeats: false)
                }
            }
        }
        else if(passwordType == 1)
        {
            if(strConfirmPassword.length < 4)
            {
                if(strConfirmPassword.length > 0)
                {
                    strConfirmPassword = (strConfirmPassword as String) + number
                }
                else
                {
                    strConfirmPassword = number
                }
                
                index = strConfirmPassword.length
                setSelectedPinImage()
                
                if(strConfirmPassword.length == 4)
                {
                    passwordType = 0;
                    
                    if(Int((strNewPassword as String)) == Int((strConfirmPassword as String)))
                    {
                        defaults.setObject(strConfirmPassword, forKey: "UserPin")
                        defaults.synchronize()
                        
                        if(pageMode == "Change")
                        {
                            NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(PINViewController.moveToRootView), userInfo: nil, repeats: false)
                        }
                        else
                        {
                            appDelegate.isAppAlive = true
                            NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(PINViewController.setHomePage), userInfo: nil, repeats: false)
                        }
                    }
                    else
                    {
                        strConfirmPassword = "";
                        strNewPassword = "";
                        lblSubTitle.text = "Set passcode for the day";
                        invokeAlertMethod("", strBody: "Passcode & Confirmation passcode not matching, please try again", delegate: nil, tag: 0)
                    }
                    
                    index = 0
                    NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(PINViewController.setSelectedPinImage), userInfo: nil, repeats: false)
                }
            }
        }
        else if(passwordType == 2)
        {
            if(strCheckPassword.length < 4)
            {
                if(strCheckPassword.length > 0)
                {
                    strCheckPassword = (strCheckPassword as String) + number
                }
                else
                {
                    strCheckPassword = number
                }
                
                index = strCheckPassword.length
                setSelectedPinImage()
                
                if (strCheckPassword.length == 4)
                {
                    let UserPin: String =  (defaults.objectForKey("UserPin") == nil) ? "" : defaults.objectForKey("UserPin") as! String
                    
                    if(Int(UserPin) == Int((strCheckPassword as String)))
                    {
                        if pageFrom == "vcLoginID"
                        {
                            self.dismissViewControllerAnimated(true, completion: nil)
                            self.setHomePage()
                        }
                        else
                        {
                            if (appDelegate.isAppAlive == true)
                            {
                                self.dismissViewControllerAnimated(true, completion: nil)
                            }
                            else
                            {
                                appDelegate.isAppAlive = true
                                self.setHomePage()
                            }
                        }
                        
//                        if appDelegate.isAppTerminateLastTime
//                        { 
//                            appDelegate.isAppTerminateLastTime = false
//                            defaults.setObject("0", forKey: "isAppTerminateLastTime")
//                            defaults.synchronize()
//                            NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(PINViewController.setHomePage), userInfo: nil, repeats: false)
//                        }
//                        else
//                        {
//                            self.dismissViewControllerAnimated(true, completion: nil)
//                        }
                    }
                    else
                    {
                        strCheckPassword = ""
                        index = 0
                        setSelectedPinImage()
                        passwordType = 2
                        shakeView()
                    }
                }
            }
        }
        else if(passwordType == 3)
        {
            if(strExistingPassword.length < 4)
            {
                if(strExistingPassword.length > 0)
                {
                    strExistingPassword = (strExistingPassword as String) + number
                }
                else
                {
                    strExistingPassword = number
                }
                
                index = strExistingPassword.length
                setSelectedPinImage()
                
                if (strExistingPassword.length == 4)
                {
                    let UserPin: String =  (NSUserDefaults.standardUserDefaults().objectForKey("UserPin") == nil) ? "" : NSUserDefaults.standardUserDefaults().objectForKey("UserPin") as! String
                    
                    if(Int(UserPin) == Int((strExistingPassword as String)))
                    {
                        lblSubTitle.text = "Enter the new passcode"
                        passwordType = 0
                        index = 0
                        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(PINViewController.setSelectedPinImage), userInfo: nil, repeats: false)
                    }
                    else
                    {
                        strExistingPassword = ""
                        index = 0
                        setSelectedPinImage()
                        passwordType = 3
                        shakeView()
                    }
                }
            }
        }
        
        button.highlighted = false
        button.selected = false
    }
    
    func showAlert(message : NSString, title: NSString)
    {
        let alertController = UIAlertController(title: title as String, message:
            message as String, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        appDelegate.window?.rootViewController!.presentViewController(alertController, animated: true, completion:nil)
    }

    @IBAction func btnBack_Click(sender: AnyObject)
    {
        if(passwordType == 0)
        {
            if(strNewPassword.length > 0)
            {
                strNewPassword = strNewPassword.substringToIndex(strNewPassword.length-1)
                index = strNewPassword.length
                setSelectedPinImage()
            }
        }
        else if(passwordType == 1)
        {
            if(strConfirmPassword.length > 0)
            {
                strConfirmPassword = strConfirmPassword.substringToIndex(strConfirmPassword.length-1)
                index = strConfirmPassword.length
                setSelectedPinImage()
            }
        }
        else if(passwordType == 2)
        {
            if(strCheckPassword.length > 0)
            {
                strCheckPassword = strCheckPassword.substringToIndex(strCheckPassword.length-1)
                index = strCheckPassword.length
                setSelectedPinImage()
            }
        }
        else if(passwordType == 3)
        {
            if(strExistingPassword.length > 0)
            {
                strExistingPassword = strExistingPassword.substringToIndex(strExistingPassword.length-1)
                index = strExistingPassword.length
                setSelectedPinImage()
            }
        }
    }
    
    
    @IBAction func btnChangeLogin_Click(sender: AnyObject)
    {
        appDelegate.UnSubscribeUserSLRUser()
        appDelegate.isUserLogin = false
        appDelegate.isSubscribed = false
        appDelegate.mySLRConnection.stop()
        appDelegate.setLoginPage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
