//
//  AppDelegate.swift
//  MarketBay
//
//  Created by Apple on 18/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import DrawerController
import SwiftR

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    
    var isUserLogin : Bool = false
    
    var showingPasscode = false
    
    var selectedBuyerCode : String = ""
    
    var passCodeForDay : Int = 0
    
    var logUsername : String = ""
    
    var logPassword : String = ""
    
    var UserRealName : String = ""
    
    var selectedBuyerId : String = ""
    
    var selectedMarketCode : String = ""
    
    var lastSeenTime : NSDate!
    
    var verificationInterval: Double = 3 //Seconds
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var drawerController: DrawerController!
    
    var isAppTerminateLastTime : Bool = false
    
    var isNoBuyers : Bool = false
    
    var screenWidth : CGFloat = 320.0
    
    var screenHeight : CGFloat = 568.0
    
    var isPadDevice : Bool = false
    
    var isAppAlive : Bool!
    
    var isNewOrder : Bool = false
    
    var isNewLine : Bool = false
    
    var selectedDocId : Int = 0
    
    var isSubscribed : Bool = false
     
    enum DLStatus : Int
    {
        case BlankYellow = 0
        case YellowWithP = 1
        case PingWithP = 2
        case PingWithC = 3
        case GreenWithC = 4
    }
    
    internal enum State {
        case Connecting
        case Connected
        case Disconnected
    }
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
        UINavigationBar.appearance().barTintColor = GlobalConstants.NavigationBar
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor(),  NSFontAttributeName: UIFont(name: "Impact", size: 22.0)!]
        
        
        if defaults.objectForKey("loggedInUsername") != nil
        {
            logUsername = defaults.objectForKey("loggedInUsername") as! String
            
            if mySLRConnection == nil
            {
                self.createSLRConnectionAndHub({ (result) in
                    
                })
            }
            else
            {
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    self.mySLRConnection.start()
                    self.SubscribeUserSLRUser(false, isDirectCall: false, isReCallSLRDetails: false)
                }
            }
        }
        
        if defaults.objectForKey("loggedInPassword") != nil
        {
            logPassword = defaults.objectForKey("loggedInPassword") as! String
        }
        
        if defaults.objectForKey("UserRealName") != nil
        {
            UserRealName = defaults.objectForKey("UserRealName") as! String
        }
        
        if defaults.objectForKey("selectedBuyerId") != nil
        {
            selectedBuyerId = defaults.objectForKey("selectedBuyerId") as! String
        }
        
        if defaults.objectForKey("selectedMarketCode") != nil
        {
            selectedMarketCode = defaults.objectForKey("selectedMarketCode") as! String
        }
        
        if defaults.objectForKey("InitialDownload") != nil && defaults.objectForKey("InitialDownload") as! String != ""
        {
            isUserLogin = true
        }
        
        lastSeenTime = NSDate()
        
        if(defaults.objectForKey("isAppTerminateLastTime") != nil && defaults.objectForKey("isAppTerminateLastTime") as! String == "1")
        {
            isAppTerminateLastTime = true
        }
        else
        {
            isAppTerminateLastTime = false
        }
        
        if(defaults.objectForKey("NoBuyers") != nil && defaults.objectForKey("NoBuyers") as! String == "yes")
        {
            isNoBuyers = true
        }
        else
        {
            isNoBuyers = false
        }
        
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
        lastSeenTime = NSDate()
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        lastSeenTime = NSDate()
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        findScreenSize()
        
        if defaults.objectForKey("InitialDownload") != nil && defaults.objectForKey("InitialDownload") as! String != ""
        {
            isUserLogin = true
        }
        
        if(defaults.objectForKey("isAppTerminateLastTime") != nil && defaults.objectForKey("isAppTerminateLastTime") as! String == "1")
        {
            isAppTerminateLastTime = true
        }
        else
        {
            isAppTerminateLastTime = false
        }
        
        if !isUserLogin || isServerDayChanged()
        {
            setLoginPage()
        }
        else
        {
            let elapsedTime = NSDate().timeIntervalSinceDate(lastSeenTime)
            
//            if(isUserLogin)
//            {
//                self.getPassCode()
//            }
            
            if verificationInterval <= elapsedTime
            {
                self.getPassCode()
            }
        }
    }
    
    func applicationWillTerminate(application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        NSUserDefaults.standardUserDefaults().setObject("1", forKey: "isAppTerminateLastTime")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    
    func showAlert(message : NSString, title: NSString)
    {
        let alertController = UIAlertController(title: title as String, message:
            message as String, preferredStyle: UIAlertControllerStyle.Alert)
        
        //        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
            self.logoutUserFromApp()
        }))
        
        self.window?.rootViewController!.presentViewController(alertController, animated: true, completion:nil)
    }
    
    func getPassCode()
    {
        if(!self.showingPasscode)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC = storyboard.instantiateViewControllerWithIdentifier("vcPINID") as! PINViewController
            loginVC.passwordType = 2
            self.window?.rootViewController?.presentViewController(loginVC, animated: true, completion: {
                
            })
        }
    }
    
    func setLoginPage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyboard.instantiateViewControllerWithIdentifier("vcLoginID") as! ViewController
        let navController : UINavigationController = UINavigationController(rootViewController: loginVC)
        navController.navigationBarHidden = true
        self.window?.rootViewController = navController
    }
    
    func clearAllValuesFromDB()
    {
        if defaults.objectForKey("isRememberSelected") == nil || (defaults.objectForKey("isRememberSelected") != nil && defaults.objectForKey("isRememberSelected") as! String != "1")
        {
//            let appDomain = NSBundle.mainBundle().bundleIdentifier!
//            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.removeObjectForKey("loggedInUsername")
            defaults.removeObjectForKey("loggedInPassword")
            defaults.removeObjectForKey("UserRealName")
            defaults.removeObjectForKey("InitialDownload")
            defaults.removeObjectForKey("selectedMarketCode")
            defaults.removeObjectForKey("selectedBuyerId")
            defaults.removeObjectForKey("isAppTerminateLastTime")
            if isServerDayChanged() {
                defaults.removeObjectForKey("UserPin")
            }
            defaults.synchronize()
        }
        else
        {
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.removeObjectForKey("UserRealName")
            defaults.removeObjectForKey("InitialDownload")
            defaults.removeObjectForKey("selectedMarketCode")
            defaults.removeObjectForKey("selectedBuyerId")
            defaults.removeObjectForKey("isAppTerminateLastTime")
            
            if isServerDayChanged() {
                defaults.removeObjectForKey("UserPin")
            }
            
            defaults.synchronize()
        }
        
        logUsername = ""
        logPassword = ""
        selectedBuyerId = ""
        UserRealName = ""
        isUserLogin = false
        
        if syncTimer != nil
        {
            syncTimer.invalidate()
            syncTimer = nil
        }
        
        let dbConn : DBConnection = DBConnection.instance
        dbConn.DeleteRecordsFromAllTables()
    }
    
    func logoutUserFromApp()
    {
        // Upload local data to server here
        
        
        // when do logout move to Passcode page - Requested by Client (Trello ID :- MBAY01)
        //        if defaults.objectForKey("isRememberSelected") == nil || (defaults.objectForKey("isRememberSelected") != nil && defaults.objectForKey("isRememberSelected") as! String != "1")
        //        {
        //            let appDomain = NSBundle.mainBundle().bundleIdentifier!
        //            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
        //        }
        //        else
        //        {
        //            let defaults = NSUserDefaults.standardUserDefaults()
        //            defaults.removeObjectForKey("UserRealName")
        //            defaults.removeObjectForKey("InitialDownload")
        //            defaults.removeObjectForKey("selectedBuyerId")
        //            defaults.removeObjectForKey("isAppTerminateLastTime")
        //            defaults.removeObjectForKey("UserPin")
        //            defaults.synchronize()
        //        }
        //
        //        logUsername = ""
        //        logPassword = ""
        //        selectedBuyerId = ""
        //        UserRealName = ""
        //        isUserLogin = false
        //
        //        if syncTimer != nil
        //        {
        //            syncTimer.invalidate()
        //            syncTimer = nil
        //        }
        //
        //        let dbConn : DBConnection = DBConnection.instance
        //        dbConn.DeleteRecordsFromAllTables()
        //        setLoginPage()
        
        getPassCode()
    }
    
    func isServerDayChanged() -> Bool
    {
        // Retrun "TURE"   --> Yes, Server day changed
        // Retrun "FALSE"  --> No, Server day not changed
        
        if NSUserDefaults.standardUserDefaults().objectForKey("serverDateTime") != nil
        {
            let serverDateTime : NSDate = NSUserDefaults.standardUserDefaults().objectForKey("serverDateTime") as! NSDate
            
            let objGeneral : General = General()
            
            let currentServerTime = objGeneral.generateCurrentServerTime()
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            let strServerTime : String = dateFormatter.stringFromDate(serverDateTime)
            
            let strCurrentTime : String = dateFormatter.stringFromDate(currentServerTime)
            
            if dateFormatter.dateFromString(strServerTime) == dateFormatter.dateFromString(strCurrentTime)
            {
                return false
            }
            
            return true
        }
        else
        {
            return true
        }
    }
    
    func loadDrawerController()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let leftSideDrawerViewController = storyboard.instantiateViewControllerWithIdentifier("vcMenuTableID")
        let centerViewController = storyboard.instantiateViewControllerWithIdentifier("vcOrderMasterID")
        
        let navigationController = UINavigationController(rootViewController: centerViewController)
        navigationController.restorationIdentifier = "CenterNavigationControllerRestorationKey"
        
        let leftSideNavController = UINavigationController(rootViewController: leftSideDrawerViewController)
        leftSideNavController.restorationIdentifier = "LeftNavigationControllerRestorationKey"
        
        
        self.drawerController = DrawerController(centerViewController: navigationController, leftDrawerViewController: leftSideDrawerViewController, rightDrawerViewController: nil)
        self.drawerController.showsShadows = false
        
        self.drawerController.restorationIdentifier = "Drawer"
        self.drawerController.maximumLeftDrawerWidth = 280.0
        self.drawerController.openDrawerGestureModeMask = .All
        self.drawerController.closeDrawerGestureModeMask = .All
        
        let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        
        appDelegate.window?.addSubview(drawerController.view)
        
        appDelegate.window?.rootViewController = drawerController
    }
    
    func findScreenSize()
    {
        screenWidth = UIScreen.mainScreen().bounds.size.width
        
        screenHeight = UIScreen.mainScreen().bounds.size.height
        
        print(screenWidth)
        
        print(screenHeight)
        
        if(screenWidth > screenHeight)
        {
            screenHeight = UIScreen.mainScreen().bounds.size.width
        }
        
        if (UI_USER_INTERFACE_IDIOM() == .Pad)
        {
            isPadDevice = true
        }
        else
        {
            isPadDevice = false
        }
    }
    
    // MARK: - Background Sync process
    
    var syncTimer : NSTimer!
    var isSynchronization : Bool = true //isSynchronization = true is ready to sync
    var isimmediateSync : Bool = false
    
    func startBackgroundSync()
    {
        syncTimer = nil
        syncTimer = NSTimer.scheduledTimerWithTimeInterval(120, target: self, selector: #selector(AppDelegate.initiateProcessInBackground), userInfo: nil, repeats: true)
    }
    
    func initiateProcessInBackground()
    {
        if Reachability.isConnectedToNetwork()
        {
            self.performSelectorInBackground(#selector(AppDelegate.performSync), withObject: nil)
        }
    }
    
    func performSync()
    {
        if(self.isSynchronization)
        {
            self.isSynchronization = false
            let currentTime : NSDate = NSDate()
            print("BackgroundSync: \(currentTime)")
            
            let apiservice : UploadDatas = UploadDatas()
            apiservice.updateOfflineDataToServer(false)
            
            if(syncTimer == nil)
            {
                dispatch_async(dispatch_get_main_queue(),{
                    self.startBackgroundSync()
                });
            }
        }
        else
        {
            if(syncTimer != nil)
            {
                syncTimer.invalidate()
                syncTimer = nil
            }
            
            NSThread.sleepForTimeInterval(20)
            
            initiateProcessInBackground()
        }
    }
    
    //*** SignalR Process ***//
    
    var mySLRConnection: SignalR!
    var SLRHub: Hub! = nil
    var commands : HttpURL = HttpURL()
    var tempTimer : NSTimer!
    
    func createSLRConnectionAndHub(completionHandler: (result : Bool) -> Void)
    {
         
        mySLRConnection = SwiftR.connect(commands.strSignalR) { connection in
            self.SLRHub = connection.createHubProxy(self.commands.strMessageHub)
            
            
            // Event handler
            self.SLRHub.on(self.commands.strReceiveMessage) { args in
                
                let localData : NSArray = args! as NSArray
                let dicData : NSDictionary = localData[0] as! NSDictionary
                
//                print(dicData)
                
                let logId : String = String(dicData["LogId"] as! Int)
                
                print(logId)
                
                self.SetDeliveredLogId(logId)
                
                if dicData["LineId"] != nil
                {
//                    let LineId : String = String(dicData["LineId"] as! Int)
                    let orderId : String = String(dicData["OrderId"] as! Int)
                    
                    let dbConn : DBConnection = DBConnection.instance
                    dbConn.updateDocLineStatus(AppDelegate.DLStatus.GreenWithC.rawValue, LineId: Int(orderId)!, bStatus: 0)
                    dbConn.updateSignalRStatus(Int(orderId)!, LogId: Int(logId)!, Status: 2)
                    
                    let LineId : Int = dbConn.getLineIdFromOrder(Int(orderId)!)
                    
                    dbConn.updateTransPostsSendToSupplier(LineId, isPublished: 1, isSendToSupplier: 1)
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("RefreshOrderlist", object: nil)
                }
            }
            
            completionHandler(result: true)
        }
        
        mySLRConnection.error = { error in
//            print("Error: \(error)")
            
            if let source = error?["source"] as? String where source == "TimeoutException" {
                print("Connection timed out. Restarting...")
                self.mySLRConnection.start()
                
                self.UnSubscribeUserSLRUser()
                self.SubscribeUserSLRUser(false, isDirectCall: false, isReCallSLRDetails : false)
            }
            else
            {
                completionHandler(result: false)
            }
        }
        
        mySLRConnection.reconnecting = { print("reconnecting") }
        mySLRConnection.reconnected = { print("reconnected") }
        
        mySLRConnection.disconnected = {
            print("Disconnected...")
            self.isSubscribed = false
            // Try again after 5 seconds
            
            if self.isUserLogin
            {
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    self.mySLRConnection.start()
                    
                    if self.tempTimer == nil
                    {
                        self.tempTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: #selector(self.reSubscribeUser), userInfo: nil, repeats: true)
                    }
                }
            }
            
        }
    }
    
    func reSubscribeUser()
    {
        if self.mySLRConnection.state == .Connected
        {
            self.SubscribeUserSLRUser(false, isDirectCall: false, isReCallSLRDetails : true)
            self.tempTimer.invalidate()
            self.tempTimer = nil
        }
    }
    
    func SubscribeUserSLRUser(isUploadOnly : Bool, isDirectCall : Bool, isReCallSLRDetails : Bool)
    {
        SLRHub.invoke(commands.strSubscribeUser, arguments: [self.logUsername]) { (result, error) in
            if let e = error {
                print("Error: \(e)")
            } else {
                print("SubscribeUser - Success!")
                self.isSubscribed = true
                
                if isReCallSLRDetails
                {
                    let upload : UploadDatas = UploadDatas()
                    upload.updateOfflineDataToServer(false)
//                    upload.getSLRDetails(isUploadOnly, isDirectCall: isDirectCall)
                }
                
                if let r = result {
                    print("Result: \(r)")
                }
            }
        }
    }
    
    func UnSubscribeUserSLRUser()
    {
        SLRHub.invoke(commands.strUnSubscribeUser, arguments: [self.logUsername]) { (result, error) in
            if let e = error {
                print("Error: \(e)")
            } else {
                print("Success!", result, error)
                if let r = result {
                    print("Result: \(r)")
                }
            }
        }
    }
    
    func SetDeliveredLogId(LogId: String)
    {
        SLRHub.invoke(commands.strSetDelivered, arguments: [LogId]) { (result, error) in
            if let e = error {
                print("Error: \(e)")
            } else {
//                print("Success!", result, error)
                if let r = result {
                    print("Result: \(r)")
                }
            }
        }
    }
}

