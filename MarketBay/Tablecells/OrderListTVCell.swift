//
//  OrderListTVCell.swift
//  MarketBay
//
//  Created by Apple on 23/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class OrderListTVCell: UITableViewCell {

    
    
    @IBOutlet weak var lblSupplier: UILabel!
    @IBOutlet weak var lblDeliveryRef: UILabel!
    @IBOutlet weak var lblSupplierRef: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblLines: UILabel!
    @IBOutlet weak var ivStatus: UIImageView!
    
    @IBOutlet weak var lblStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
