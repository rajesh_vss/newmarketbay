//
//  BuyerSelectionTVCell.swift
//  MarketBay
//
//  Created by Apple on 20/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class BuyerSelectionTVCell: UITableViewCell {

    @IBOutlet weak var lblBuyerCode: UILabel!
    
    @IBOutlet weak var lblRole: UILabel!
    
    @IBOutlet weak var ivSelection: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
