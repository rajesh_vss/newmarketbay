//
//  OrderTVCell.swift
//  MarketBay
//
//  Created by Apple on 31/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class OrderTVCell: UITableViewCell {

    
    @IBOutlet weak var lblLineNumber: UILabel!
    @IBOutlet weak var lblSupplier: UILabel!
    @IBOutlet weak var lblProdType: UILabel!
    @IBOutlet weak var lblProdVarity: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblQtyUnit: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    
    @IBOutlet weak var lblContentUnit: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPriceUnit: UILabel!
    @IBOutlet weak var ivStatus: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
