//
//  ViewController.swift
//  MarketBay
//
//  Created by Apple on 18/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import MRProgress

class ViewController: UIViewController {

    @IBOutlet weak var scrlView: UIScrollView!
    
    @IBOutlet weak var txtUsername: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnRemember: UIButton!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var vwMVHeightCons: NSLayoutConstraint!
    
    @IBOutlet weak var vwMainView: UIView!
    
    @IBOutlet weak var ivBackgroundImage: UIImageView!
    
    @IBOutlet weak var txtUNWidthConst: NSLayoutConstraint!
    
    var activeTextField : UITextField!
    
    var general : General = General()
    
    var downData : DownloadData = DownloadData()
    
    var progressView = MRProgressOverlayView()
    
    var appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var isRememberSelected : Bool = false
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(animated: Bool)
    {
        changeBackgroundImage()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.startObservingKeyboardEvents()
        
        self.alignTextField()
        
        if defaults.objectForKey("isRememberSelected") != nil && defaults.objectForKey("isRememberSelected") as! String == "1"
        {
            txtUsername.text =  defaults.objectForKey("loggedInUsername") != nil ? defaults.objectForKey("loggedInUsername") as! String : ""
            txtPassword.text =  defaults.objectForKey("loggedInPassword") != nil ? defaults.objectForKey("loggedInPassword") as! String : ""
            
            isRememberSelected = true
            btnRemember.setImage(UIImage(named: "icRemChecked"), forState: UIControlState.Normal)
        }
        
        txtUsername.autocorrectionType = .No
        txtPassword.autocorrectionType = .No
        
        self.vwMVHeightCons.constant = appDelegate.screenHeight
        vwMainView.layoutIfNeeded()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.DeviceRotated), name: UIApplicationDidChangeStatusBarOrientationNotification, object: nil)
    }
    
    // MARK: - Functions
    
    func changeBackgroundImage()
    {
        var isPortrait : Bool = true
        
        switch UIDevice.currentDevice().orientation{
        case .Portrait:
            isPortrait = true
        case .PortraitUpsideDown:
            isPortrait = true
        case .LandscapeLeft:
            isPortrait = false
        case .LandscapeRight:
            isPortrait = false
        default:
            isPortrait = true
        }
        
        if appDelegate.isPadDevice
        {
            if isPortrait
            {
                ivBackgroundImage.image = UIImage(named: "768x1024.png")
            }
            else
            {
                ivBackgroundImage.image = UIImage(named: "1024x768.png")
            }
            
            txtUNWidthConst.constant = 444
        }
        else
        {
            if isPortrait
            {
                ivBackgroundImage.image = UIImage(named: "iPhoneBGPortrait")
                txtUNWidthConst.constant = 244
            }
            else
            {
                ivBackgroundImage.image = UIImage(named: "667x375.png")
                
                if(!appDelegate.isPadDevice && appDelegate.screenHeight > 568.0)
                {
                    txtUNWidthConst.constant = 344
                }
            }
        }
    }
    
    func DeviceRotated()
    {
        changeBackgroundImage()
    }
    
    func alignTextField()
    {
        txtUsername.attributedPlaceholder = NSAttributedString(string:"Username",
                                                               attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        txtPassword.attributedPlaceholder = NSAttributedString(string:"Password",
                                                               attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        let userLeftView:UIView = UIView(frame: CGRect(x:0, y:0, width:25, height:20));
        let imgUserView = UIImageView(frame: CGRectMake(2, 0, 16, 17))
        imgUserView.image = UIImage(named: "icUsername")
        userLeftView.addSubview(imgUserView)
        txtUsername.leftViewMode = UITextFieldViewMode.Always;
        txtUsername.leftView = userLeftView
        
        let passLeftView:UIView = UIView(frame: CGRect(x:0, y:0, width:25, height:20));
        let imgPassView = UIImageView(frame: CGRectMake(2, 2, 12, 17))
        imgPassView.image = UIImage(named: "icPassword")
        passLeftView.addSubview(imgPassView)
        txtPassword.leftViewMode = UITextFieldViewMode.Always;
        txtPassword.leftView = passLeftView
    }
    
    func validateControls() -> Bool
    {
        if(txtUsername.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) == "")
        {
            invokeAlertMethod("Required", strBody: "Please enter username", delegate: self, tag: 0,focusField: txtUsername, focusOnField: true)
            return false
        }
        else if(txtPassword.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) == "")
        {
            invokeAlertMethod("Required", strBody: "Please enter password", delegate: self, tag: 0,focusField: txtPassword, focusOnField: true)
            return false
        }
        else if(!Reachability.isConnectedToNetwork())
        {
            invokeAlertMethod("Required", strBody: "Internet connection required to login, please check your internet connection", delegate: self, tag: 0,focusField: txtPassword, focusOnField: true)
            return false
        }
        return true
    }
    
    func invokeAlertMethod(strTitle: String, strBody: String, delegate: AnyObject?, tag : Int, focusField:UITextField?,focusOnField:Bool = false )
    {
        let alertController = UIAlertController(title: strTitle, message: strBody, preferredStyle: UIAlertControllerStyle.Alert)
        
        if(focusField == false)
        {
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        }
        else
        {
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: { (action: UIAlertAction!) in
                if(focusField != nil) {focusField!.becomeFirstResponder() };
            }))
        }
        
        self.presentViewController(alertController, animated: true, completion:nil)
    }
    
    func ValidateCredentials()
    {
        if validateControls()
        {
            
            if activeTextField != nil
            {
                activeTextField.resignFirstResponder()
            }
            
            ShowProgress()
            
            appDelegate.isNoBuyers = false
            
            downData.ValidateCredentialsInServer(txtUsername.text!, Password: txtPassword.text!)
            {
                (result : Bool) in
            
                self.hideProgress()
                
                if result
                {
                    self.defaults.setObject(self.txtUsername.text!, forKey: "loggedInUsername")
                    self.defaults.setObject(self.txtPassword.text!, forKey: "loggedInPassword")
                    self.defaults.synchronize()
                    
                    self.appDelegate.logUsername = self.txtUsername.text!
                    self.appDelegate.logPassword = self.txtPassword.text!
                    
                    self.appDelegate.isSubscribed = false
                    self.appDelegate.createSLRConnectionAndHub(){
                        result in ()
                        
//                        print(result)
//                        self.appDelegate.SubscribeUserSLRUser()
//                        print(NSDate())
//                        NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(self.SubscribeUserSLRUser), userInfo: nil, repeats: false)
                    }
                    
                    self.moveToPinView()
                }
                else
                {
                    self.invokeAlertMethod("Error", strBody: "Invalid username/password", delegate: self, tag: 0, focusField: nil, focusOnField: false)
                }
            }
        }
    } 
    
    func moveToPinView()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let setViewController = mainStoryboard.instantiateViewControllerWithIdentifier("vcPINID") as! PINViewController
        setViewController.pageFrom = "vcLoginID"
        
        if defaults.objectForKey("UserPin") == nil
        {
            setViewController.passwordType = 0
        }
        else if(appDelegate.isServerDayChanged())
        {
            setViewController.passwordType = 0
        }
        else
        {
            setViewController.passwordType = 2
        }
        
        UIView.animateWithDuration(0.1, animations: {
            self.view.alpha = 0.0
            }, completion: {
                (value: Bool) in
                
                setViewController.view.alpha = 1.0
                self.view .removeFromSuperview()
                self.appDelegate.window?.rootViewController = setViewController
        })
    }
    
    func ShowProgress()
    {
        progressView = MRProgressOverlayView.showOverlayAddedTo(self.navigationController!.view, animated: true)
        progressView.mode = .IndeterminateSmall
        progressView.titleLabelText = "Validating ...";
        progressView.titleLabel.textColor = GlobalConstants.progressTitleColor
        progressView.tintColor = GlobalConstants.progressTitleColor
        progressView.show(true)
    }
    
    func hideProgress()
    {
        progressView.dismiss(true)
    }
    
    // MARK: - Events
    
    @IBAction func btnLogin_click(sender: AnyObject)
    {
        appDelegate.clearAllValuesFromDB()
        ValidateCredentials()
    }
    
    @IBAction func btnRemember_click(sender: AnyObject)
    {
        if isRememberSelected
        {
            defaults.setObject("0", forKey: "isRememberSelected")
            isRememberSelected = false
            btnRemember.setImage(UIImage(named: "icRemember"), forState: UIControlState.Normal)
        }
        else
        {
            defaults.setObject("1", forKey: "isRememberSelected")
            isRememberSelected = true
            btnRemember.setImage(UIImage(named: "icRemChecked"), forState: UIControlState.Normal)
        }
        
        defaults.synchronize()
    }
    
    // MARK: -  UITextField Delegate
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        activeTextField = textField
        
        if textField == txtUsername
        {
            txtUsername.returnKeyType = .Next
        }
        else
        {
            txtPassword.returnKeyType = .Done
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        activeTextField = nil
        
        if textField == txtUsername
        {
            txtUsername.resignFirstResponder()
            txtPassword.becomeFirstResponder()
        }
        else
        {
            txtPassword.resignFirstResponder()
            ValidateCredentials()
        }
        
        return true
    }
    
    // MARK: - Keyboard Delegate
    
    private func startObservingKeyboardEvents()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWasShown(_:)), name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWillBeHidden(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    private func stopObservingKeyboardEvents()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWasShown (notification: NSNotification)
    {
        let userInfo : NSDictionary = notification.userInfo!
        if let keyboardSize: CGSize = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size
        {
            let insets: UIEdgeInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height+10, 0)
            scrlView.contentInset = insets
            scrlView.scrollIndicatorInsets = insets
            scrlView.contentOffset = CGPointMake(scrlView.contentOffset.x, scrlView.contentOffset.y ) //+ keyboardSize.height
        }
    }
    
    func keyboardWillBeHidden (notification: NSNotification)
    {
        let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
        self.scrlView.contentInset = contentInsets
        self.scrlView.scrollIndicatorInsets = contentInsets
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

