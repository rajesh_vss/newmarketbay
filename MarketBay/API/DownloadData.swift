//
//  DownloadData.swift
//  MarketBay
//
//  Created by Apple on 19/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Alamofire
import MRProgress

class DownloadData: NSObject
{
    var commands : HttpURL = HttpURL()
    var general : General = General()
    var parser: Parser = Parser()
    var dbConnection : DBConnection = DBConnection()
    let defaults = NSUserDefaults.standardUserDefaults()
    var appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    var isInitialDownloadCompleted = false
    
    var loginDelegate : BuyerSelectionViewController!
    
    // MARK: - Login Process
    
    func ValidateCredentialsInServer(Username : String, Password : String, completionHandler: (result : Bool) -> Void)
    {
        Alamofire.request(.POST, commands.strDomain + commands.strValidate, parameters: ["Username": Username, "Password" : Password ])
            .responseJSON { response in
         
                if response.result.value != nil
                {
                    let dic = response.result.value as! NSDictionary
                    
                    print(dic)
                    
                    var authenticated : Bool = false
                    
                    if dic.objectForKey("status") as! String == "success"
                    {
                        let serverDateTime : NSDate = self.general.dateFromJSON(dic.objectForKey("DateandTime") as! String)
                        
                        let currentDateTime : NSDate = NSDate()
                        let timediff = serverDateTime.timeIntervalSinceDate(currentDateTime)
                        let str = String(stringInterpolationSegment: timediff)
                        
                        let realName = dic.objectForKey("RealName") as! String
                        
                        self.appDelegate.UserRealName = realName
                        
                        NSUserDefaults.standardUserDefaults().setObject(realName, forKey: "UserRealName")
                        NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey: "loginDateTime")
                        NSUserDefaults.standardUserDefaults().setObject(serverDateTime, forKey: "serverDateTime")
                        NSUserDefaults.standardUserDefaults().setObject(str, forKey: "TimeDiff")
                        NSUserDefaults.standardUserDefaults().synchronize()
                        
                        authenticated = dic.objectForKey("Authenticated") as! Bool
                    }
                    
                    completionHandler(result: authenticated)
                }
                else
                {
                    completionHandler(result: false)
                }
        }
    }
    
    func GetUserInRolesFromServer(completionHandler: (result : String) -> Void)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.downloadIntrupted()
        }
        else
        {
            DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastEntityTypeSyncDateTime)
            
            Alamofire.request(.POST, commands.strDomain + commands.strGetUserInRoles, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword])
                .responseJSON { response in
                    
                    var statusMessage : String = ""
                    
                    if response.result.value != nil
                    {
                        let dic = response.result.value as! NSDictionary
                        
                        if dic.objectForKey("status") as! String == "success"
                        {
                            let arrRole = dic.objectForKey("UserInRoles") as! NSArray
                            
                            let resUserRole = self.parser.parseDatatoUserRole(arrRole)
                            
                            if resUserRole.count > 0
                            {
                                var isValidToDownloadData : Bool = false
                                
                                let dateFormatter = NSDateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
                                
                                var currentDate : NSDate = dateFormatter.dateFromString(self.dbConnection.CurrentDateTime())!
                                
                                dateFormatter.dateFormat = "yyyy-MM-dd"
                                
                                let strCurrentDate : String = dateFormatter.stringFromDate(currentDate)
                                
                                currentDate = dateFormatter.dateFromString(strCurrentDate)!
                                
                                for index in 0 ..< resUserRole.count
                                {
                                    let currentUser = resUserRole[index]
                                    
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
                                    
                                    if currentUser.LicenceExpiredDate != ""
                                    {
                                        var licDate : NSDate = dateFormatter.dateFromString(currentUser.LicenceExpiredDate)!
                                        
                                        dateFormatter.dateFormat = "yyyy-MM-dd"
                                        
                                        let strlicDate : String = dateFormatter.stringFromDate(licDate)
                                        
                                        licDate = dateFormatter.dateFromString(strlicDate)!
                                        
                                        if licDate > currentDate
                                        {
                                            isValidToDownloadData = true
                                            break
                                        }
                                    }
                                }
                                
                                if isValidToDownloadData
                                {
                                    sharedInstance.database!.open()
                                    sharedInstance.database!.beginTransaction()
                                    
                                    for role in resUserRole
                                    {
                                        self.dbConnection.insertUserInRoles(role)
                                    }
                                    
                                    sharedInstance.database!.commit()
                                    sharedInstance.database!.close()
                                    
                                    self.StartDownload()
                                }
                                else
                                {
                                    // Licence Expired
                                    self.defaults.setObject("Completed", forKey: "InitialDownload")
                                    self.defaults.synchronize()
                                    self.hideProgress()
                                    statusMessage = "Licence Expired for the Buyers"
                                }
                                
                            }
                            else
                            {
                                // No active entity
                                self.defaults.setObject("Completed", forKey: "InitialDownload")
                                self.defaults.synchronize()
                                self.hideProgress()
                                statusMessage = "You require BuyerAdmin or BuyerSalesman role to access"
                            }
                        }
                    }
                    
                    completionHandler(result: statusMessage)
            }
        }
    }
    
    // MARK: - Initinal Sync Process
    
    func downloadIntrupted()
    {
        self.hideProgress()
        self.appDelegate.showAlert("This app required internet connection to run", title: "Error")
    }
    
    func downloadCorrupted()
    {
        self.hideProgress()
        self.appDelegate.showAlert("Found problem with initial download process, Please check with system admin or after sometime", title: "Error")
    }
    
    func ShowProgress()
    {
        let progressView = MRProgressOverlayView.showOverlayAddedTo(appDelegate.window, animated: true)
        progressView.mode = .IndeterminateSmall
        progressView.titleLabelText = "Downloading ...";
        progressView.titleLabel.textColor = GlobalConstants.progressTitleColor
        progressView.tintColor = GlobalConstants.progressTitleColor
    }
    
    func hideProgress()
    {
        MRProgressOverlayView.dismissAllOverlaysForView(appDelegate.window, animated: true)
    }
    
    func StartDownload()
    {
        if(defaults.objectForKey("InitialDownload") == nil)
        {
            isInitialDownloadCompleted = false
            DownloadEntityType(1)
        }
        else
        {
            isInitialDownloadCompleted = true
            postDownloadCompleted()
        }
    }
    
    func postDownloadCompleted()
    {
        if(defaults.objectForKey("InitialDownload") == nil)
        {
            isInitialDownloadCompleted = true
            appDelegate.isUserLogin = true
            defaults.setObject("Completed", forKey: "InitialDownload")
            defaults.synchronize()
        }
        
        //For Background sync process
        
        appDelegate.startBackgroundSync()
        
        hideProgress()
        
        loginDelegate.getBuyerDetails()
    }
    
    // *** Download Data from server *** //
    
    func DownloadEntityType(currentPagneNo : Int)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.downloadIntrupted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastEntityTypeSyncDateTime)
            
            if lastSyncTime == ""
            {
                ShowProgress()
                
                Alamofire.request(.POST, commands.strDomain + commands.strGetEntityType, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword])
                    .responseJSON { response in
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrEntityType = dic.objectForKey("EntityTypes") as! NSArray
                                
                                let resEntityType = self.parser.parseDatatoEntityType(arrEntityType)
                                
                                sharedInstance.database!.open()
                                sharedInstance.database!.beginTransaction()
                                
                                for product in resEntityType
                                {
                                    self.dbConnection.insertEntityType(product)
                                }
                                
                                sharedInstance.database!.commit()
                                sharedInstance.database!.close()
                                
                                if(arrEntityType.count < self.commands.pageCount)
                                {
                                    self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastEntityTypeSyncDateTime, PrimayrKey: 1)
                                    self.DownloadEntities(1)
                                }
                                else
                                {
                                    self.DownloadEntityType(currentPagneNo+1)
                                }
                            }
                            else
                            {
                                self.dbConnection.truncateEntityType()
                                self.downloadCorrupted()
                            }
                        }
                        else
                        {
                            self.dbConnection.truncateEntityType()
                            self.downloadIntrupted()
                        }
                }
            }
            else
            {
                self.DownloadEntityTypeInBackground()
            }
        }
    }
    
    func DownloadEntities(currentPagneNo : Int)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.downloadIntrupted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastEntitiesSyncDateTime)
            
            if lastSyncTime == ""
            {
                Alamofire.request(.POST, commands.strDomain + commands.strGetEntity, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "PageCount" : String(currentPagneNo)])
                    .responseJSON { response in
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrEntityType = dic.objectForKey("Entities") as! NSArray
                                
                                let resEntityType = self.parser.parseDatatoEntities(arrEntityType)
                                
                                sharedInstance.database!.open()
                                sharedInstance.database!.beginTransaction()
                                
                                for product in resEntityType
                                {
                                    self.dbConnection.insertEntities(product)
                                }
                                
                                sharedInstance.database!.commit()
                                sharedInstance.database!.close()
                                
                                if(arrEntityType.count < self.commands.pageCount)
                                {
                                    self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastEntitiesSyncDateTime, PrimayrKey: 2)
                                    self.DownloadProductType(1)
                                }
                                else
                                {
                                    self.DownloadEntities(currentPagneNo+1)
                                }
                            }
                            else
                            {
                                self.dbConnection.truncateEntities()
                                self.downloadCorrupted()
                            }
                        }
                        else
                        { 
                            self.dbConnection.truncateEntities()
                            self.downloadIntrupted()
                        }
                }
            }
            else
            {
                self.DownloadEntitiesInBackground()
            }
        }
    }
    
    func DownloadProductType(currentPagneNo : Int)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.downloadIntrupted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastProductTypeSyncDateTime)
            
            if lastSyncTime == ""
            {
                Alamofire.request(.POST, commands.strDomain + commands.strGetProductType, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "PageCount" : String(currentPagneNo)])
                    .responseJSON { response in
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrProductType = dic.objectForKey("ProdTypes") as! NSArray
                                
                                let resProductType = self.parser.parseDatatoProdutType(arrProductType)
                                
                                sharedInstance.database!.open()
                                sharedInstance.database!.beginTransaction()
                                
                                for product in resProductType
                                {
                                    self.dbConnection.insertPrdouctType(product)
                                }
                                
                                sharedInstance.database!.commit()
                                sharedInstance.database!.close()
                                
                                if(arrProductType.count < self.commands.pageCount)
                                {
                                    self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastProductTypeSyncDateTime, PrimayrKey: 3)
                                    self.DownloadProductVarieties(1);
                                }
                                else
                                {
                                    self.DownloadProductType(currentPagneNo+1)
                                }
                            }
                            else
                            {
                                self.dbConnection.truncateEntities()
                                self.downloadCorrupted()
                            }
                        }
                        else
                        {
                            self.dbConnection.truncateEntities()
                            self.downloadIntrupted()
                        }
                }
            }
            else
            {
                self.DownloadProductTypeInBackground()
            }
        }
    }
    
    func DownloadProductVarieties(currentPagneNo : Int)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.downloadIntrupted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastProductVarietiesSyncDateTime)
            
            if lastSyncTime == ""
            {
                Alamofire.request(.POST, commands.strDomain + commands.strGetProductVarieties, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "PageCount" : String(currentPagneNo)])
                    .responseJSON { response in
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrProductType = dic.objectForKey("ProdVarieties") as! NSArray
                                
                                let resProductType = self.parser.parseDatatoProductVarieties(arrProductType)
                                
                                sharedInstance.database!.open()
                                sharedInstance.database!.beginTransaction()
                                
                                for product in resProductType
                                {
                                    self.dbConnection.insertPropertyVarieties(product)
                                }
                                
                                sharedInstance.database!.commit()
                                sharedInstance.database!.close()
                                
                                if(arrProductType.count < self.commands.pageCount)
                                {
                                    self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastProductVarietiesSyncDateTime, PrimayrKey: 4)
                                    self.DownloadContainers(1);
                                }
                                else
                                {
                                    self.DownloadProductVarieties(currentPagneNo+1)
                                }
                            }
                            else
                            {
                                self.dbConnection.truncateEntities()
                                self.downloadCorrupted()
                            }
                        }
                        else
                        {
                            self.dbConnection.truncateEntities()
                            self.downloadIntrupted()
                        }
                }
            }
            else
            {
                self.DownloadProductVarietiesInBackground()
            }
        }
    }
    
    func DownloadContainers(currentPagneNo : Int)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.downloadIntrupted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastContainersSyncDateTime)
            
            if lastSyncTime == ""
            {
                Alamofire.request(.POST, commands.strDomain + commands.strGetContainers, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "PageCount" : String(currentPagneNo)])
                    .responseJSON { response in
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrProductType = dic.objectForKey("GetContainers") as! NSArray
                                
                                let resProductType = self.parser.parseDatatoContainers(arrProductType)
                                
                                sharedInstance.database!.open()
                                sharedInstance.database!.beginTransaction()
                                
                                for product in resProductType
                                {
                                    self.dbConnection.insertContainers(product)
                                }
                                
                                sharedInstance.database!.commit()
                                sharedInstance.database!.close()
                                
                                if(arrProductType.count < self.commands.pageCount)
                                {
                                    self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastContainersSyncDateTime, PrimayrKey: 5)
                                    self.DownloadDocType(1);
                                }
                                else
                                {
                                    self.DownloadContainers(currentPagneNo+1)
                                }
                            }
                            else
                            {
                                self.dbConnection.truncateEntities()
                                self.downloadCorrupted()
                            }
                        }
                        else
                        {
                            self.dbConnection.truncateEntities()
                            self.downloadIntrupted()
                        }
                }
            }
            else
            {
                self.DownloadContainersInBackground()
            }
        }
    }
    
    func DownloadDocType(currentPagneNo : Int)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.downloadIntrupted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastDocTypeSyncDateTime)
            
            if lastSyncTime == ""
            {
                Alamofire.request(.POST, commands.strDomain + commands.strGetDocTypes, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "PageCount" : String(currentPagneNo)])
                    .responseJSON { response in
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrProductType = dic.objectForKey("DocTypes") as! NSArray
                                
//                                print(arrProductType)
                                
                                let resProductType = self.parser.parseDatatoDocType(arrProductType)
                                
                                sharedInstance.database!.open()
                                sharedInstance.database!.beginTransaction()
                                
                                for product in resProductType
                                {
                                    self.dbConnection.insertDocType(product)
                                }
                                
                                sharedInstance.database!.commit()
                                sharedInstance.database!.close()
                                
                                if(arrProductType.count < self.commands.pageCount)
                                {
                                    self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastDocTypeSyncDateTime, PrimayrKey: 6)
                                    self.DownloadDocLines(1);
                                }
                                else
                                {
                                    self.DownloadDocType(currentPagneNo+1)
                                }
                            }
                            else
                            {
                                self.dbConnection.truncateEntities()
                                self.downloadCorrupted()
                            }
                        }
                        else
                        {
                            self.dbConnection.truncateEntities()
                            self.downloadIntrupted()
                        }
                }
            }
            else
            {
                self.DownloadDocTypeInBackground()
            }
        }
    }
    
    func DownloadDocLines(currentPagneNo : Int)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.downloadIntrupted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastDocLinesSyncDateTime)
            
            if lastSyncTime == ""
            {
                Alamofire.request(.POST, commands.strDomain + commands.strGetDocLines, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "PageCount" : String(currentPagneNo)])
                    .responseJSON { response in
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            print(dic)
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrProductType = dic.objectForKey("DocLines") as! NSArray
                                
                                let resProductType = self.parser.parseDatatoDocLines(arrProductType)
                                
//                                print(resProductType)
                                
                                sharedInstance.database!.open()
                                sharedInstance.database!.beginTransaction()
                                
                                for product in resProductType
                                {
                                    self.dbConnection.insertDocLines(product)
                                }
                                
                                sharedInstance.database!.commit()
                                sharedInstance.database!.close()
                                
                                if(arrProductType.count < self.commands.pageCount)
                                {
                                    self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastDocLinesSyncDateTime, PrimayrKey: 7)
                                    self.DownloadTransPost(1);
                                }
                                else
                                {
                                    self.DownloadDocLines(currentPagneNo+1)
                                }
                            }
                            else
                            {
                                self.dbConnection.truncateEntities()
                                self.downloadCorrupted()
                            }
                        }
                        else
                        {
                            self.dbConnection.truncateEntities()
                            self.downloadIntrupted()
                        }
                }
            }
            else
            {
                self.DownloadDocLinesInBackground()
            }
        }
    }
    
    func DownloadTransPost(currentPagneNo : Int)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.downloadIntrupted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastTransPostSyncDateTime)
            
            if lastSyncTime == ""
            {
                Alamofire.request(.POST, commands.strDomain + commands.strGetTransPost, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "PageCount" : String(currentPagneNo)])
                    .responseJSON { response in
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrTransPost = dic.objectForKey("TransPost") as! NSArray
                                
                                let resTransPost = self.parser.parseDatatoTransPost(arrTransPost)
                                
                                sharedInstance.database!.open()
                                sharedInstance.database!.beginTransaction()
                                
                                for transPost in resTransPost
                                {
                                    self.dbConnection.insertTransPost(transPost)
                                }
                                
                                sharedInstance.database!.commit()
                                sharedInstance.database!.close()
                                
                                if(arrTransPost.count < self.commands.pageCount)
                                {
                                    self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastTransPostSyncDateTime, PrimayrKey: 8)
                                    self.DownloadProdSizeCount(1);
                                }
                                else
                                {
                                    self.DownloadTransPost(currentPagneNo+1)
                                }
                            }
                            else
                            {
                                self.dbConnection.truncateEntities()
                                self.downloadCorrupted()
                            }
                        }
                        else
                        {
                            self.dbConnection.truncateEntities()
                            self.downloadIntrupted()
                        }
                }
            }
            else
            {
                self.DownloadTransPostInBackground()
            }
        }
    }
    
    func DownloadProdSizeCount(currentPagneNo : Int)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.downloadIntrupted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastProdSizeCountSyncDateTime)
            
            if lastSyncTime == ""
            {
                Alamofire.request(.POST, commands.strDomain + commands.strGetProdSizeCount, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "PageCount" : String(currentPagneNo)])
                    .responseJSON { response in
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrProdSizeCount = dic.objectForKey("GetProdSizeCount") as! NSArray
                                
                                let resProdSizeCount = self.parser.parseDatatoProdSizeCount(arrProdSizeCount)
                                
                                sharedInstance.database!.open()
                                sharedInstance.database!.beginTransaction()
                                
                                for SizeCount in resProdSizeCount
                                {
                                    self.dbConnection.insertProdSizeCount(SizeCount)
                                }
                                
                                sharedInstance.database!.commit()
                                sharedInstance.database!.close()
                                
                                if(arrProdSizeCount.count < self.commands.pageCount)
                                {
                                    self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastProdSizeCountSyncDateTime, PrimayrKey: 9)
                                    self.DownloadSystemMessage();
                                }
                                else
                                {
                                    self.DownloadProdSizeCount(currentPagneNo+1)
                                }
                            }
                            else
                            {
                                self.dbConnection.truncateEntities()
                                self.downloadCorrupted()
                            }
                        }
                        else
                        {
                            self.dbConnection.truncateEntities()
                            self.downloadIntrupted()
                        }
                }
            }
            else
            {
                self.DownloadPrdSizeCountInBackground()
            }
        }
    }
    
    func DownloadSystemMessage()
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.downloadIntrupted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastSystemMessageSyncDateTime)
            
            if lastSyncTime == ""
            {
                Alamofire.request(.POST, commands.strDomain + commands.strGetSystemMessage, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "MessageNo" : "197,198,199,200,201,202,203,204,205"])
                    .responseJSON { response in
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrProdSizeCount = dic.objectForKey("SystemMessage") as! NSArray
                                
                                let resProdSizeCount = self.parser.parseDatatoSystemMessage(arrProdSizeCount)
                                
                                sharedInstance.database!.open()
                                sharedInstance.database!.beginTransaction()
                                
                                for SizeCount in resProdSizeCount
                                {
                                    self.dbConnection.insertSystemMessage(SizeCount)
                                }
                                
                                sharedInstance.database!.commit()
                                sharedInstance.database!.close()
                                
                                if(arrProdSizeCount.count < self.commands.pageCount)
                                {
                                    self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastProdSizeCountSyncDateTime, PrimayrKey: 9)
                                    self.postDownloadCompleted();
                                }
                            }
                            else
                            {
                                self.dbConnection.truncateEntities()
                                self.downloadCorrupted()
                            }
                        }
                        else
                        {
                            self.dbConnection.truncateEntities()
                            self.downloadIntrupted()
                        }
                }
            }
            else
            {
                self.DownloadPrdSizeCountInBackground()
            }
        }
    }
    
    // MARK: - Background Sync Process
    
    func DownloadEntityTypeInBackground()
    {
        if(!Reachability.isConnectedToNetwork())
        {
            //self.backgroundSyncIncompleted()
        }
        else
        {
            
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastEntityTypeSyncDateTime)
            
            Alamofire.request(.POST, commands.strDomain + commands.strGetEntityType, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "LastModified" : lastSyncTime])
                .responseJSON { response in
                    
                    switch response.result
                    {
                    case .Success(let JSON):
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrEntityType = dic.objectForKey("EntityTypes") as! NSArray
                                
                                let resEntityType = self.parser.parseDatatoEntityType(arrEntityType)
                                
                                sharedInstance.database!.open()
                                sharedInstance.database!.beginTransaction()
                                
                                for product in resEntityType
                                {
                                    self.dbConnection.insertEntityType(product)
                                }
                                
                                sharedInstance.database!.commit()
                                sharedInstance.database!.close()
                                
                                self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastEntityTypeSyncDateTime, PrimayrKey: 1)
                                
                                // If Initinal sync not completed then continue the initinal sync process
                                // Other wishe conitune the background process
                                
                                if(!self.isInitialDownloadCompleted)
                                {
                                    self.DownloadEntities(1)
                                }
                                else
                                {
                                    self.DownloadEntitiesInBackground()
                                }
                            }
                            else
                            {
                                self.DownloadEntitiesInBackground()
                            }
                        }
                        else
                        {
                            self.DownloadEntitiesInBackground()
                        }
                        break
                    case .Failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            self.backgroundSyncIncompleted()
                            return
                        }
                        else
                        {
                            self.DownloadEntitiesInBackground()
                        }
                    }
            }
        }
    }
    
    
    func DownloadEntitiesInBackground()
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.backgroundSyncIncompleted()
        }
        else
        {
            
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastEntitiesSyncDateTime)
            
            let pageCount : String = "0"
            
            Alamofire.request(.POST, commands.strDomain + commands.strGetEntity, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "LastModified" : lastSyncTime, "PageCount" :pageCount])
                .responseJSON { response in
                    
                    switch response.result
                    {
                    case .Success(let JSON):
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrEntityType = dic.objectForKey("Entities") as! NSArray
                                
                                if(arrEntityType.count > 0)
                                {
                                    let resEntityType = self.parser.parseDatatoEntities(arrEntityType)
                                    
                                    sharedInstance.database!.open()
                                    sharedInstance.database!.beginTransaction()
                                    
                                    for product in resEntityType
                                    {
                                        self.dbConnection.insertEntities(product)
                                    }
                                    
                                    sharedInstance.database!.commit()
                                    sharedInstance.database!.close()
                                }
                                
                                self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastEntitiesSyncDateTime, PrimayrKey: 2)
                                
                                // If Initinal sync not completed then continue the initinal sync process
                                // Other wishe conitune the background process
                                
                                if(!self.isInitialDownloadCompleted)
                                {
                                    self.DownloadProductType(1)
                                }
                                else
                                {
                                    self.DownloadProductTypeInBackground()
                                }
                            }
                            else
                            {
                                self.DownloadProductTypeInBackground()
                            }
                        }
                        else
                        {
                            self.DownloadProductTypeInBackground()
                        }
                        break
                    case .Failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            print("Timeout")
                            self.backgroundSyncIncompleted()
                            return
                        }
                        else
                        {
                            self.DownloadProductTypeInBackground()
                        }
                    }
            }
        }
    }
    
    
    func DownloadProductTypeInBackground()
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.backgroundSyncIncompleted()
        }
        else
        {
            
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastProductTypeSyncDateTime)
            
            let pageCount : String = "0"
            
            Alamofire.request(.POST, commands.strDomain + commands.strGetProductType, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "LastModified" : lastSyncTime, "PageCount" :pageCount])
                .responseJSON { response in
                    
                    switch response.result
                    {
                    case .Success(let JSON):
                        if response.result.value != nil
                        {
                            
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrProdut = dic.objectForKey("ProdTypes") as! NSArray
                                
                                if(arrProdut.count > 0)
                                {
                                    let resProduct = self.parser.parseDatatoProdutType(arrProdut)
                                    
                                    sharedInstance.database!.open()
                                    sharedInstance.database!.beginTransaction()
                                    
                                    for product in resProduct
                                    {
                                        self.dbConnection.insertPrdouctType(product)
                                    }
                                    
                                    sharedInstance.database!.commit()
                                    sharedInstance.database!.close()
                                }
                                
                                self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastProductTypeSyncDateTime, PrimayrKey: 3)
                                
                                // If Initinal sync not completed then continue the initinal sync process
                                // Other wishe conitune the background process
                                
                                if(!self.isInitialDownloadCompleted)
                                {
                                    self.DownloadProductVarieties(1)
                                }
                                else
                                {
                                    self.DownloadProductVarietiesInBackground()
                                }
                            }
                            else
                            {
                                self.DownloadProductVarietiesInBackground()
                            }
                        }
                        else
                        {
                            self.DownloadProductVarietiesInBackground()
                        }
                        break
                    case .Failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            print("Timeout")
                            self.backgroundSyncIncompleted()
                            return
                        }
                        else
                        {
                            self.DownloadProductVarietiesInBackground()
                        }
                    }
            }
        }
    }
    
    func DownloadProductVarietiesInBackground()
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.backgroundSyncIncompleted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastProductVarietiesSyncDateTime)
            
            let pageCount : String = "0"
            
            Alamofire.request(.POST, commands.strDomain + commands.strGetProductVarieties, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "LastModified" : lastSyncTime, "PageCount" :pageCount])
                .responseJSON { response in
                    
                    switch response.result
                    {
                    case .Success(let JSON):
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrProductVari = dic.objectForKey("ProdVarieties") as! NSArray
                                
                                if(arrProductVari.count > 0)
                                {
                                    let resProductVari = self.parser.parseDatatoProductVarieties(arrProductVari)
                                    
                                    sharedInstance.database!.open()
                                    sharedInstance.database!.beginTransaction()
                                    
                                    for productVari in resProductVari
                                    {
                                        self.dbConnection.insertPropertyVarieties(productVari)
                                    }
                                    sharedInstance.database!.commit()
                                    sharedInstance.database!.close()
                                }
                                
                                self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastProductVarietiesSyncDateTime, PrimayrKey: 4)
                                
                                // If Initinal sync not completed then continue the initinal sync process
                                // Other wishe conitune the background process
                                
                                if(!self.isInitialDownloadCompleted)
                                {
                                    self.DownloadContainers(1)
                                }
                                else
                                {
                                    self.DownloadContainersInBackground()
                                }
                            }
                            else
                            {
                                self.DownloadContainersInBackground()
                            }
                        }
                        else
                        {
                            self.DownloadContainersInBackground()
                        }
                        break
                    case .Failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            print("Timeout")
                            self.backgroundSyncIncompleted()
                            return
                        }
                        else
                        {
                            self.DownloadContainersInBackground()
                        }
                    }
                    
                    
            }
        }
    }
    
    func DownloadContainersInBackground()
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.backgroundSyncIncompleted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastContainersSyncDateTime)
            
            let pageCount : String = "0"
            
            Alamofire.request(.POST, commands.strDomain + commands.strGetContainers, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "LastModified" : lastSyncTime, "PageCount" :pageCount])
                .responseJSON { response in
                    
                    switch response.result
                    {
                    case .Success(let JSON):
                        if response.result.value != nil
                        {
                            
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrContainer = dic.objectForKey("GetContainers") as! NSArray
                                
                                if(arrContainer.count > 0)
                                {
                                    let resContainer = self.parser.parseDatatoContainers(arrContainer)
                                    
                                    sharedInstance.database!.open()
                                    sharedInstance.database!.beginTransaction()
                                    
                                    for index in 0 ..< resContainer.count
                                    {
                                        let container = resContainer[index]
                                        self.dbConnection.insertContainers(container)
                                    }
                                    
                                    sharedInstance.database!.commit()
                                    sharedInstance.database!.close()
                                }
                                
                                
                                self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastContainersSyncDateTime, PrimayrKey: 5)
                                
                                // If Initinal sync not completed then continue the initinal sync process
                                // Other wishe conitune the background process
                                
                                if(!self.isInitialDownloadCompleted)
                                {
                                    self.DownloadDocLines(1)
                                }
                                else
                                {
                                    self.DownloadDocTypeInBackground()
                                }
                            }
                            else
                            {
                                self.DownloadDocTypeInBackground()
                            }
                        }
                        else
                        {
                            self.DownloadDocTypeInBackground()
                        }
                        break
                    case .Failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            print("Timeout")
                            self.backgroundSyncIncompleted()
                            return
                        }
                        else
                        {
                            self.DownloadDocTypeInBackground()
                        }
                    }
            }
        }
    }
    
    func DownloadDocTypeInBackground()
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.backgroundSyncIncompleted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastDocTypeSyncDateTime)
            
            let pageCount : String = "0"
            
            Alamofire.request(.POST, commands.strDomain + commands.strGetDocTypes, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "LastModified" : lastSyncTime, "PageCount" :pageCount])
                .responseJSON { response in
                    
                    switch response.result
                    {
                    case .Success(let JSON):
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrContainer = dic.objectForKey("DocTypes") as! NSArray
                                
                                if(arrContainer.count > 0)
                                {
                                    let resContainer = self.parser.parseDatatoDocType(arrContainer)
                                    
                                    sharedInstance.database!.open()
                                    sharedInstance.database!.beginTransaction()
                                    
                                    for index in 0 ..< resContainer.count
                                    {
                                        let objDT = resContainer[index]
                                        self.dbConnection.insertDocType(objDT)
                                    }
                                    
                                    sharedInstance.database!.commit()
                                    sharedInstance.database!.close()
                                }
                                
                                self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastDocTypeSyncDateTime, PrimayrKey: 6)
                                
                                // If Initinal sync not completed then continue the initinal sync process
                                // Other wishe conitune the background process
                                
                                if(!self.isInitialDownloadCompleted)
                                {
                                    self.DownloadDocLines(1)
                                }
                                else
                                {
                                    self.DownloadDocLinesInBackground()
                                }
                            }
                            else
                            {
                                self.DownloadDocLinesInBackground()
                            }
                        }
                        else
                        {
                            self.DownloadDocLinesInBackground()
                        }
                        break
                    case .Failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            print("Timeout")
                            self.backgroundSyncIncompleted()
                            return
                        }
                        else
                        {
                            self.DownloadDocLinesInBackground()
                        }
                    }
            }
        }
    }
    
    func DownloadDocLinesInBackground()
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.backgroundSyncIncompleted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastDocLinesSyncDateTime)
            
            let pageCount : String = "0"
            
            Alamofire.request(.POST, commands.strDomain + commands.strGetDocLines, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "LastModified" : lastSyncTime, "PageCount" :pageCount])
                .responseJSON { response in
                    
                    switch response.result
                    {
                    case .Success(let JSON):
                        if response.result.value != nil
                        {
                            
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrdocline = dic.objectForKey("DocLines") as! NSArray
                                
                                if(arrdocline.count > 0)
                                {
                                    let resDocline = self.parser.parseDatatoDocLines(arrdocline)
                                    
                                    sharedInstance.database!.open()
                                    sharedInstance.database!.beginTransaction()
                                    
                                    for index in 0 ..< resDocline.count
                                    {
                                        let docline = resDocline[index]
                                        self.dbConnection.insertDocLines(docline)
                                    }
                                    
                                    sharedInstance.database!.commit()
                                    sharedInstance.database!.close()
                                }
                                
                                
                                self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastDocLinesSyncDateTime, PrimayrKey: 7)
                                
                                // If Initinal sync not completed then continue the initinal sync process
                                // Other wishe conitune the background process
                                
                                if(!self.isInitialDownloadCompleted)
                                {
                                    self.DownloadTransPost(1)
                                }
                                else
                                {
                                    self.DownloadTransPostInBackground()
                                }
                            }
                            else
                            {
                                self.DownloadTransPostInBackground()
                            }
                        }
                        else
                        {
                            self.DownloadTransPostInBackground()
                        }
                        break
                    case .Failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            print("Timeout")
                            self.backgroundSyncIncompleted()
                            return
                        }
                        else
                        {
                            self.DownloadTransPostInBackground()
                        }
                    }
            }
        }
    }
    
    func DownloadTransPostInBackground()
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.backgroundSyncIncompleted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastTransPostSyncDateTime)
            
            let pageCount : String = "0"
            
            Alamofire.request(.POST, commands.strDomain + commands.strGetTransPost, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "LastModified" : lastSyncTime, "PageCount" :pageCount])
                .responseJSON { response in
                    
                    switch response.result
                    {
                    case .Success(let JSON):
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrTransPost = dic.objectForKey("TransPost") as! NSArray
                                
                                if(arrTransPost.count > 0)
                                {
                                    let resTransPost = self.parser.parseDatatoTransPost(arrTransPost)
                                    
                                    sharedInstance.database!.open()
                                    sharedInstance.database!.beginTransaction()
                                    
                                    for index in 0 ..< resTransPost.count
                                    {
                                        let transPost = resTransPost[index]
                                        self.dbConnection.insertTransPost(transPost)
                                    }
                                    
                                    sharedInstance.database!.commit()
                                    sharedInstance.database!.close()
                                    
                                }
                                
                                self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastDocLinesSyncDateTime, PrimayrKey: 7)
                                
                                // If Initinal sync not completed then continue the initinal sync process
                                // Other wishe conitune the background process
                                
                                if(!self.isInitialDownloadCompleted)
                                {
                                    self.DownloadProdSizeCount(1)
                                }
                                else
                                {
                                    self.DownloadTransPostInBackground()
                                }
                            }
                        }
                        
                        self.backgroundSyncCompleted()
                        
                        break
                    case .Failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            print("Timeout")
                            self.backgroundSyncIncompleted()
                            return
                        }
                        else
                        {
                            self.backgroundSyncIncompleted()
                        }
                    }
            }
        }
    }

    
    func DownloadPrdSizeCountInBackground()
    {
        if(!Reachability.isConnectedToNetwork())
        {
            self.backgroundSyncIncompleted()
        }
        else
        {
            let lastSyncTime = DBConnection.instance.GetLastSyncDateTime(GlobalConstants.LastProdSizeCountSyncDateTime)
            
            let pageCount : String = "0"
            
            Alamofire.request(.POST, commands.strDomain + commands.strGetTransPost, parameters: ["Username": appDelegate.logUsername, "Password" : appDelegate.logPassword, "LastModified" : lastSyncTime, "PageCount" :pageCount])
                .responseJSON { response in
                    
                    switch response.result
                    {
                    case .Success(let JSON):
                        
                        if response.result.value != nil
                        {
                            let dic = response.result.value as! NSDictionary
                            
                            if dic.objectForKey("status") as! String == "success"
                            {
                                let arrTransPost = dic.objectForKey("GetProdSizeCount") as! NSArray
                                
                                if(arrTransPost.count > 0)
                                {
                                    let resTransPost = self.parser.parseDatatoProdSizeCount(arrTransPost)
                                    
                                    sharedInstance.database!.open()
                                    sharedInstance.database!.beginTransaction()
                                    
                                    for index in 0 ..< resTransPost.count
                                    {
                                        let transPost = resTransPost[index]
                                        self.dbConnection.insertProdSizeCount(transPost)
                                    }
                                    
                                    sharedInstance.database!.commit()
                                    sharedInstance.database!.close()
                                    
                                }
                                
                                self.dbConnection.UpdateCurrentSyncTime(GlobalConstants.LastProdSizeCountSyncDateTime, PrimayrKey: 8)
                                
                                // If Initinal sync not completed then continue the initinal sync process
                                // Other wishe conitune the background process
                                
                                if(!self.isInitialDownloadCompleted)
                                {
                                    self.postDownloadCompleted()
                                }
                            }
                        }
                        
                        self.backgroundSyncCompleted()
                        
                        break
                    case .Failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            print("Timeout")
                            self.backgroundSyncIncompleted()
                            return
                        }
                        else
                        {
                            self.backgroundSyncIncompleted()
                        }
                    }
            }
        }
    }

    
    func backgroundSyncCompleted()
    {
        let myDictOfDict : NSDictionary = ["Status" : "Success"]
        NSNotificationCenter.defaultCenter().postNotificationName("BackgroundSyncDataDownloadCompletd", object: nil)
        NSNotificationCenter.defaultCenter().postNotificationName("BackgroundSyncDataDownloadCompletdFromSale", object: myDictOfDict)
    }
    
    func backgroundSyncIncompleted()
    {
        let myDictOfDict : NSDictionary = ["Status" : "Failure"]
        NSNotificationCenter.defaultCenter().postNotificationName("BackgroundSyncDataDownloadCompletd", object: nil)
        NSNotificationCenter.defaultCenter().postNotificationName("BackgroundSyncDataDownloadCompletdFromSale", object: myDictOfDict)
    }
}
