//
//  UploadDatas.swift
//  MarketBay
//
//  Created by Apple on 23/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Alamofire
import JSONModel


class UploadDatas: NSObject
{
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    var commands : HttpURL = HttpURL()
    let dbconn : DBConnection = DBConnection()
    
    // *** Upload date from local db to server *** //
    
    func updateOfflineDataToServer(isUploadOnly: Bool)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            
        }
        else
        {
            uploadDocLineToServer(isUploadOnly)
        }
    }
    
    func uploadDocLineToServer(isUploadOnly : Bool)
    {
        var arrDocLines : [DocLines] = [DocLines]()
                
        let arrTransPost : [TransPost] = dbconn.getTransPostToUploadServer()
        
        if arrTransPost.count > 0
        {
            for lines in arrTransPost
            {
                arrDocLines.append(dbconn.getDocLinesDetailsToUploadServer(lines.LineId))
                
                let details : SaveDocument = SaveDocument()
                details.isPublished = -1
                details.isSendToSupplier = -1
                
                let parseDaTDic : ParseDataToDictionary = ParseDataToDictionary()
                
                var jsonData = NSData()
                
                // *** Service accept only array of datas so creating temporay array here. *** //
                var tempArrTransPost : [TransPost] = [TransPost]()
                
                tempArrTransPost.append(lines)
                
                let orderDic = parseDaTDic.convertOrderDetailsToDictionay(details, arrofTrans: tempArrTransPost, arrofDoc: arrDocLines)
                
                do
                {
                    jsonData = try NSJSONSerialization.dataWithJSONObject(orderDic, options: NSJSONWritingOptions.PrettyPrinted)
                    
                    let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)
                    print("jsonString: " + String(jsonString))
                    
                    UploadDocLineToServer(jsonData)
                    
                } catch let error as NSError {
                    print(error)
                }
            }
        }
        
        getSLRDetails(isUploadOnly, isDirectCall : false)
    }
    
    
    func getSLRDetails(isUploadOnly : Bool, isDirectCall : Bool)
    {
        if(!Reachability.isConnectedToNetwork())
        {
            
        }
        else
        {
            var connectionState : Bool = false
            
            if appDelegate.mySLRConnection.state == .Connected
            {
                connectionState = true
                print("Connected")
            }
            else if appDelegate.mySLRConnection.state == .Disconnected
            {
                connectionState = false
                print("Disconnected")
            }
            
            
            if connectionState && appDelegate.isSubscribed
            {
                let arrdoclines : [DocLines] = dbconn.getSignalRStatusToUpdateServer()
                
                if arrdoclines.count > 0
                {
                    for index in 0...arrdoclines.count-1
                    {
                        let docline : DocLines = arrdoclines[index]
                        
                        print("LineId: " + String(docline.LineId))
                        
                        if index == arrdoclines.count-1
                        {
                            SendBuyerRequestLogToserver(docline, lastIndex: -1, isDirectCall: isDirectCall, isUploadOnly: isUploadOnly)
                        }
                        else
                        {
                            SendBuyerRequestLogToserver(docline, lastIndex: index, isDirectCall: isDirectCall, isUploadOnly: isUploadOnly)
                        }
                    }
                }
                else
                {
                    uploadDataToServer(isUploadOnly)
                }
            }
            else
            {
//                appDelegate.mySLRConnection.start()
//                appDelegate.SubscribeUserSLRUser(isUploadOnly, isDirectCall: isDirectCall, isReCallSLRDetails: true)
//                uploadDataToServer(isUploadOnly)
                
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    self.appDelegate.mySLRConnection.start()
                    self.appDelegate.SubscribeUserSLRUser(isUploadOnly, isDirectCall: isDirectCall, isReCallSLRDetails: true)
                }

            }
        }
    }
    
    func uploadDataToServer(isUploadOnly : Bool)
    {
        let orderDetails = dbconn.getSaveDocumentDetailsToUploadServer()
        
        if(orderDetails.count > 0)
        {
            for details in orderDetails
            {
                var arrTransPost : [TransPost]!
                
                var arrDocLines : [DocLines] = [DocLines]()
                
                arrTransPost = dbconn.getTransPostDetailsToUploadServer(details.DocID, SupplerId: details.SupplierID)
                
                if arrTransPost.count > 0
                {
                    for lines in arrTransPost
                    {
                        arrDocLines.append(dbconn.getDocLinesDetailsToUploadServer(lines.LineId))
                    }
                    
                    let parseDaTDic : ParseDataToDictionary = ParseDataToDictionary()
                    
                    var jsonData = NSData()
                    
                    let orderDic = parseDaTDic.convertOrderDetailsToDictionay(details, arrofTrans: arrTransPost, arrofDoc: arrDocLines)
                    
                    do
                    {
                        jsonData = try NSJSONSerialization.dataWithJSONObject(orderDic, options: NSJSONWritingOptions.PrettyPrinted)
                        
//                        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)
//                        print("jsonString: " + String(jsonString))
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                    
                    do
                    {
                        let decoded = try NSJSONSerialization.JSONObjectWithData(jsonData, options: []) as? Dictionary<String, AnyObject>;
                        
                        uploadOrderDetailsToOnline(decoded!, lastIndex: -1, isUploadOnly: isUploadOnly)
                    }
                    catch _ as NSError
                    {
                    }
                }
            }
        }
        else
        {
             let myDictOfDict : NSDictionary = ["DocId" : 0, "DocSId" : 0]
             NSNotificationCenter.defaultCenter().postNotificationName("OrderDetailsDetailSyncCompleted", object: myDictOfDict)
             NSNotificationCenter.defaultCenter().postNotificationName("SendToSupplierDetailSyncCompleted", object: nil)
        }
    }
    
    func uploadOrderDetailsToOnline(order: Dictionary<String,AnyObject>, lastIndex : Int, isUploadOnly : Bool)
    {
        if(!Reachability.isConnectedToNetwork())
        {
        }
        else
        {
            Alamofire.request(.POST, (commands.strDomain + commands.strSaveDocument), parameters: order, encoding: .JSON, headers:.None ).responseJSON { response in
                
                if response.result.value != nil
                {
                    let dic = response.result.value as! NSDictionary
                    
//                    print("JSON: \(response.result.value)")
                    
                    let TransPosts = dic.objectForKey("TransPosts") as! NSArray
                    
                    let isPublished : Int = dic.objectForKey("Published") as! Bool == true ? 1 : 0
                    
                    let isSendToSupplier : Int = dic.objectForKey("SendToSupplier") as! Bool == true ? 1 : -1
                    
                    let dbConn : DBConnection = DBConnection.instance
                    
                    for index in 0 ..< TransPosts.count
                    {
                        let LineId : Int = TransPosts[index].objectForKey("LineId")  as! Int
                        
                        dbConn.updateTransPostsSendToSupplier(LineId, isPublished: isPublished, isSendToSupplier: isSendToSupplier)
                    }
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("SendToSupplierDetailSyncCompleted", object: nil)
                }
                
                if !isUploadOnly
                {
                    self.downloadDataFromServer()
                }
            }
        }
    }
    
    
    
    // *** Comments ***//
    // I'm not using Alamofire here to upload data to server because Alamofire call function asynchronously but I need service to call one by one in for loop
    // so I use separate code to upload data Refer *uploadDocLineToServer*
    
    func UploadDocLineToServer(jsonData: NSData)
    {
        let  url = NSURL(string: commands.strDomain + commands.strSaveDocument)
        
        let theJSONText = NSString(data: jsonData, encoding: NSASCIIStringEncoding)
        let requestBodyData: NSData = theJSONText!.dataUsingEncoding(NSUTF8StringEncoding)!
        
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = requestBodyData
        
        var response: NSURLResponse? = nil
        var error: NSError? = nil
        var receivedData: NSData? = nil
        
        do {
            receivedData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
        } catch let error1 as NSError {
            error = error1
            print(error)
            receivedData = nil
        }
        
        if let JSONData = receivedData
        {
            do {
                let json: AnyObject = try NSJSONSerialization.JSONObjectWithData(JSONData, options: [])
                if let jsonDictionary = json as? NSDictionary
                {
                    print("responseString \(jsonDictionary)")
                    
                    let dbConn : DBConnection = DBConnection.instance
                    
                    let docLines = jsonDictionary.objectForKey("DocLines") as! NSArray
                    
                    let TransPosts = jsonDictionary.objectForKey("TransPosts") as! NSArray
                    
                    let DocId = TransPosts[0].objectForKey("DocId") as! Int
                    
                    let DocSId = TransPosts[0].objectForKey("SDocId") as! Int
                    
                    for index in 0 ..< docLines.count
                    {
                        if docLines[index].objectForKey("OrderId") != nil
                        {
                            let OrderId = docLines[index].objectForKey("OrderId") as! Int
                            
                            let sOrderId = docLines[index].objectForKey("SOrderId") as! Int
                            
                            let LineId : Int = docLines[index].objectForKey("LineId")  as! Int
                            
                            let SLineId : Int = docLines[index].objectForKey("SLineId")  as! Int
                            
                            let DocId : Int = docLines[index].objectForKey("DocId")  as! Int
                            
                            dbConn.updateDocLinesLineId(LineId, LineSId: SLineId, DocId: DocId, OrderId: OrderId, sOrderId: sOrderId)
                            
                            let myDictOfDict : NSDictionary = ["lineId" : LineId, "lineSId" : SLineId]
                            
                            NSNotificationCenter.defaultCenter().postNotificationName("UpdateDocLineIdFromServer", object: myDictOfDict)
                        }
                        
                    }
                    
                    for index in 0 ..< TransPosts.count
                    {
                        let TransPostId : Int = TransPosts[index].objectForKey("TransPostId")  as! Int
                        
                        let TransPostSId : Int = TransPosts[index].objectForKey("TransPostSId")  as! Int
                        
                        let DocId : Int = TransPosts[index].objectForKey("DocId")  as! Int
                        
                        let DocSId : Int = TransPosts[index].objectForKey("SDocId")  as! Int
                        
                        let LineId : Int = TransPosts[index].objectForKey("LineId")  as! Int
                        
                        if DocSId > 0
                        {
                            dbConn.updateTransPostId(TransPostId, TransPostSId: TransPostSId, DocId: DocId, DocSId: DocSId, LineId: LineId)
                        }
                    }
                    
                    let myDictOfDict : NSDictionary = ["DocId" : DocId, "DocSId" : DocSId]
                    NSNotificationCenter.defaultCenter().postNotificationName("OrderDetailsDetailSyncCompleted", object: myDictOfDict)
                }
            } catch let error1 as NSError {
                error = error1
                let myDictOfDict : NSDictionary = ["DocId" : 0, "DocSId" : 0]
                NSNotificationCenter.defaultCenter().postNotificationName("OrderDetailsDetailSyncCompleted", object: myDictOfDict)
            }
        }
    }
    
    func SendBuyerRequestLogToserver(docLine: DocLines, lastIndex : Int, isDirectCall : Bool, isUploadOnly : Bool)
    {
        var GrossValue : Double = 0.0
        
        if docLine.BuyPriceUnit == docLine.BuyCtnUnit
        {
            GrossValue = docLine.BuyPrice * Double(docLine.BuyCtnQty)
        }
        else
        {
            GrossValue = docLine.BuyPrice * Double(docLine.BuyCtnQty) * Double(docLine.BuyUnitQty)
        }
        
        let dayTimePeriodFormatter = NSDateFormatter()
        dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let RequestTime : NSDate = NSDate()
        
        let dictionary : NSDictionary =  ["LogId" : "",
        "TransactionId" : "",
        "OrderId" : docLine.OrderId,
        "RequestType" : "bullet",
        "RequestTime" : dayTimePeriodFormatter.stringFromDate(RequestTime),
        "MarketBaseEntity" : docLine.SupplierMarketCode,
        "MarketBaseRepId" : "",
        "MarketBayEntity" : appDelegate.selectedMarketCode,
        "MarketBayUser" : appDelegate.logUsername,
        "TransRef" : docLine.SupplierRef,
        "ProdType" : docLine.ProdTypeDesc,
        "ProdVariety" : docLine.FreeformDesc,
        "OuterQty" : docLine.BuyCtnQty,
        "OuterUnit" : docLine.FreeformBuyCtn,
        "InnerQty" : docLine.BuyUnitQty,
        "InnerUnit" : docLine.FreeformBuyUnit,
        "Price" : docLine.BuyPrice,
        "PriceUnit" : docLine.FreeformPriceUnit,
        "GrossValue" : GrossValue,
        "Grade" : docLine.Grade,
        "SizeCount" : docLine.CountSize,
        "Colour" : docLine.Colour,
        "LineId" : docLine.LineId,
        "DocId" : docLine.DocId,
        "ItemStatus" : "",
        "IsComplete" : ""]
 
        
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(dictionary, options: NSJSONWritingOptions(rawValue: 0))
        
        let theJSONText = NSString(data: theJSONData!, encoding: NSASCIIStringEncoding)
        
        print(theJSONText, terminator: "")
        
        let requestBodyData: NSData = theJSONText!.dataUsingEncoding(NSUTF8StringEncoding)!
        
        let url = NSURL(string: commands.strDomain + commands.strSendBuyerRequestLog)
        
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = requestBodyData
        
        var response: NSURLResponse? = nil
        var error: NSError? = nil
        var receivedData: NSData? = nil
        
        do {
            receivedData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
        } catch let error1 as NSError {
            error = error1
            print(error)
            receivedData = nil
        }
        
        let responseString: NSString = NSString(data: receivedData!, encoding: NSUTF8StringEncoding)!;
        
        print("responseString \(responseString)")
        
        if let JSONData = receivedData
        {
            do {
                let json: AnyObject = try NSJSONSerialization.JSONObjectWithData(JSONData, options: [])
                if let jsonDictionary = json as? NSDictionary
                {
                    let status : String = jsonDictionary.objectForKey("status") as! String
                    
                    if status == "success"
                    {
                        let dbConn : DBConnection = DBConnection.instance
                        dbConn.updateSignalRStatus(docLine.OrderId, LogId: 0, Status: 1)
                    }
                }
            }
            catch let error1 as NSError
            {
                error = error1
            }
        }
        
        if !isDirectCall && lastIndex == -1
        {
            uploadDataToServer(isUploadOnly)
        }
    }
    
    // *** Download date from server to local db *** //
    
    func downloadDataFromServer()
    {
        let downData : DownloadData = DownloadData()
        
        downData.isInitialDownloadCompleted = true
        downData.DownloadEntitiesInBackground()
        
        appDelegate.isSynchronization = true
        
        if(appDelegate.isimmediateSync)
        {
            appDelegate.isSynchronization = false
            appDelegate.isimmediateSync = false
            updateOfflineDataToServer(false)
        }
    }
}
