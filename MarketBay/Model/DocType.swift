//
//  DocType.swift
//  MarketBay
//
//  Created by Apple on 06/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class DocType: NSObject
{
    var DocTypeId : Int = 0
    var DocTypeDesc : String = ""
    var XML_DocType : String = ""
    var XSL_Stylesheet : String = ""
    var isActive : Bool = false
    var IsCheckbox : Bool = false
    var LastModified : String = ""
}
