//
//  SaveDocument.swift
//  MarketBay
//
//  Created by Apple on 01/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class SaveDocument: NSObject
{
    var SupplierID : Int = 0
    var DocID : Int = 0
    var LineID : Int = 0
    var isPublished : Int = 0
    var isSendToSupplier : Int = 0
    var isUpdatedToOnline : Int = 0
    var locStatusId : Int = 0
}
