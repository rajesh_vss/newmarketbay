//
//  ProdSizeCount.swift
//  MarketBay
//
//  Created by Apple on 22/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class ProdSizeCount: NSObject
{
    var ProdSizeCountId : String = ""
    var ProdTypeId : Int = 0
    var SizeCount : Int = 0
    var IsActive : Bool = false
    var Cid : Int = 0
    var ContainerCode : String = ""
    var CreatedBy : String = ""
    var LastModified : String = ""
}
