//
//  Containers.swift
//  MarketBay
//
//  Created by Apple on 22/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class Containers: NSObject
{
    var Cid : Int = 0
    var CidDesc : String = ""
    var Deposit : Double = 0
    var CreatedBy : String = ""
    var LastModified : String = ""
}
