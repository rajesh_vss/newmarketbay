//
//  ProductType.swift
//  MarketBay
//
//  Created by Apple on 22/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class ProductType: NSObject
{
    var ProdTypeId : Int = 0
    var ProdTypeDesc : String = ""
    var ProdTypeShortName : String = ""
    var ClassId : Int = 0
    var CreatedBy : String = ""
    var LastModified : String = ""  
}
