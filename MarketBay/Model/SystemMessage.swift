//
//  SystemMessage.swift
//  MarketBay
//
//  Created by Apple on 23/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class SystemMessage: NSObject
{
    var MessageNo : Int = 0
    var ModuleName : String = ""
    var SystemMessageType : String = ""
    var SystemMessageDetails : String = ""
    var Selection1 : String = ""
    var Selection2 : String = ""
    var Selection3 : String = ""
}
