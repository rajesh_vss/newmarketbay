//
//  Entities.swift
//  MarketBay
//
//  Created by Apple on 19/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class Entities: NSObject
{
    var ClientId: Int = 0
    
    var EntityId: Int = 0
    
    var EntityTypeID: Int = 0
    
    var EntityType: String = ""
    
    var Name1: String = ""
    
    var Name2: String = ""
    
    var MarketCode: String = ""
    
    var IsActive: String = ""
    
    var RepId : Int = 0
    
    var LastModified : String = ""
    
    var RebatePercentage:Double = 0.0
    
    var HandlingFeePer:Double = 0.0
    
    var Representative : Int = 0
    
    var ClientEntity : Int = 0
}
