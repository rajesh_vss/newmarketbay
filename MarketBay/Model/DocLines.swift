//
//  DocLines.swift
//  MarketBay
//
//  Created by Apple on 22/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class DocLines: NSObject
{ 
    var LineId : Int = 0
    var DocId : Int = 0
    var DocTypeId : Int = 0
    
    var ProdTypeId : Int = 0
    var ProdTypeDesc : String = ""
    
    var VarietyId : Int = 0
    var VarietyDesc : String = ""
    
    var SupplierRef : String = ""
    var DeliveryRef : String = ""
    var FreeformDesc : String = ""
    var CountSize : Int = 0
    var Grade : String = ""
    var Colour : String = ""
    
    var BuyCtnQty : Int = 0
    var BuyCtnCid : Int = 0
    var BuyCtnUnits : String = ""
    
    var BuyUnitQty : Int = 0
    var BuyUnitCid : Int = 0
    var BuyUnitUnits : String = ""
    
    
    var BuyPrice : Double = 0
    var BuyPriceCid : Int = 0
    var BuyPriceUnits : String = ""
    
    var SupplierID : Int = 0
    var SupplierMarketCode : String = ""
    var SupplierRepId : String = ""
    
    var EntityReference : String = ""
    
    var SellPrice : Int = 0
    var SellPriceCid : Int = 0
    var SellCtnQty : Int = 0
    var SellCtnCid : Int = 0
    var SellUnitQty : Int = 0
    var SellUnitCid : Int = 0
    var PackDate : String = ""
    var UseByDate : String = ""
    var GrowerId : Int = 0
    var IndustryRegionId : Int = 0
    var Status : Bool = false
    var GTIN : String = ""
    var TI : Int = 0
    var HI : Int = 0
    var PalletQty : Int = 0
    var ParentDocLineId : Int = 0
    var CreatedBy : String = ""
    var LastModified : String = ""
    var OwnerId : Int = 0
    
    var ProdTypeName : String = ""
    var ProdVarityName : String = ""
    var SupplierName : String = ""
    var MarketCode : String = ""
    var BuyCtnQtyUnit : String = ""
    var BuyCtnUnit : String = ""
    var BuyPriceUnit : String = ""
    var locStatusId : Int = 0
    var IsActive : Bool = false
    
    var isUpdatedToOnline : Int = 0
    
    var OrderId : Int = 0
    var FreeformBuyCtn  : String = ""
    var FreeformBuyUnit  : String = ""
    var FreeformPriceUnit  : String = ""

}
