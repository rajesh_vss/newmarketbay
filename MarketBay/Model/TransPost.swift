//
//  TransPost.swift
//  MarketBay
//
//  Created by Apple on 22/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class TransPost: NSObject
{
    var TransPostId : Int = 0
    var SupplierId : Int = 0
    var SupplierMarketCode : String = ""
    var LineId : Int = 0
    var DocTypeId : Int = 0
    var DocId : Int = 0
    var CreateTime : String = ""
    var SendTime : String = ""
    var RespondTime : String = ""
    var Flag : String = ""
    var Status : Bool = false
    var DocSendId : Int = 0
    var CreatedBy : String = ""
    var LastModified : String = ""
    var BuyerId : Int = 0
    var DocSendLastStatus : String = ""
    var LineCount : Int = 0
    var locStatusId : Int = 0
    var isPublished : Int = 0
    var isSendToSupplier : Int = 0
    var isUpdatedToOnline : Int = 0
}
