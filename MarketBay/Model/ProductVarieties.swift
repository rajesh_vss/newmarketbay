//
//  ProductVarieties.swift
//  MarketBay
//
//  Created by Apple on 22/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class ProductVarieties: NSObject
{
    var VarietyId : Int = 0
    var VarietyDesc : String = ""
    var ProdTypeId : Int = 0
    var EntityId : Int = 0
    var EntityReference : String = ""
    var EntityGroup : String = ""
    var CtnCid : Int = 0
    var UnitQty : Double = 0
    var UnitCid : Int = 0
    var TI : Int = 0
    var HI : Int = 0
    var PalletQty : Int = 0
    var GTIN : String = ""
    var PackDate_Days : String = ""
    var UseByDate_Days : String = ""
    var ProdStandardId : Int = 0
    var CreatedBy : String = ""
    var LastModified : String = ""
    var ProdStandardCode : String = ""
}
