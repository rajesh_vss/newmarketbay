//
//  UserRole.swift
//  MarketBay
//
//  Created by Apple on 20/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class UserRole: NSObject
{
    var EntityId: Int = 0
    
    var RoleId: String = ""
    
    var RoleName: String = ""
    
    var LicenceExpiredDate : String = ""
    
    var MarketCode : String = ""
}
