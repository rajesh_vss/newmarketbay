//
//  customTextField.swift
//  MarketBay
//
//  Created by Apple on 31/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class customTextField: UITextField {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    override func drawRect(rect: CGRect) {
        // Drawing code
        
        
        let leftPaddingView : UIView = UIView(frame: CGRectMake(0, 0, 6, 20))
        leftPaddingView.backgroundColor = UIColor.clearColor()
        self.leftView = leftPaddingView
        self.leftViewMode = UITextFieldViewMode.Always
        
    }
    
}
