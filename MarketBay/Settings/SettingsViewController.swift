//
//  SettingsViewController.swift
//  MarketBay
//
//  Created by Apple on 24/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import DrawerController
import MRProgress

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLeftMenuButton()
        
        self.title = "SETTINGS"
    }
    
    func setupLeftMenuButton() {
        
        let leftDrawerButton = DrawerBarButtonItem(target: self, action: #selector(self.leftDrawerButtonPress(_:)))
        leftDrawerButton.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem = leftDrawerButton
    }
    
    func leftDrawerButtonPress(sender: AnyObject?) {
        self.evo_drawerController?.toggleDrawerSide(.Left, animated: true, completion: nil)
    }

    @IBAction func btnChangePasscode_Click(sender: AnyObject)
    {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyboard.instantiateViewControllerWithIdentifier("vcPINID") as! PINViewController
        loginVC.passwordType = 3
        loginVC.pageMode = "Change"
        self.navigationController?.pushViewController(loginVC, animated: true)
        
    }
    
    @IBAction func btnFilter_Click(sender: AnyObject)
    {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
