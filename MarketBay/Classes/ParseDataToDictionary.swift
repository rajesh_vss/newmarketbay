//
//  ParseDataToDictionary.swift
//  MarketBay
//
//  Created by Apple on 02/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class ParseDataToDictionary: NSObject
{
    
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    func convertOrderDetailsToDictionay(docDetails: SaveDocument, arrofTrans : [TransPost], arrofDoc: [DocLines]) -> NSDictionary
    {
        let dicOrder: NSMutableDictionary = NSMutableDictionary()
        
        dicOrder.setValue(appDelegate.logUsername, forKey: "Username")
        
        dicOrder.setValue(appDelegate.logPassword, forKey: "Password")
        
        dicOrder.setValue(docDetails.isPublished == 0 ? "True" : "False", forKey: "Publish")
        
        dicOrder.setValue(docDetails.isSendToSupplier == 0 ? "True" : "False", forKey: "SendToSupplier")
        
        
        let arrTransPost: NSMutableArray = NSMutableArray()
        
        for trans in arrofTrans
        {
            let objTrans = convertTransPostDetailsToDictionay(trans)
            
            arrTransPost.addObject(objTrans)
        }
        
        dicOrder.setValue(arrTransPost,forKey:"TransPosts")
        
        
        let arrofDocLines : NSMutableArray = NSMutableArray()
        
        for line in arrofDoc
        {
            let objline = convertDocLinesDetailsToDictionay(line)
            
            arrofDocLines.addObject(objline)
        }
        
        dicOrder.setValue(arrofDocLines,forKey:"DocLines")
        
        return dicOrder
    }
    
    func convertTransPostDetailsToDictionay(objTrans : TransPost) -> NSDictionary
    {
        let dicTrans: NSMutableDictionary = NSMutableDictionary()
        
        dicTrans.setValue(objTrans.TransPostId, forKey: "TransPostId")
        dicTrans.setValue(objTrans.SupplierId, forKey: "SupplierId")
        dicTrans.setValue(objTrans.LineId , forKey: "LineId")
        dicTrans.setValue(objTrans.DocTypeId , forKey: "DocTypeId")
        dicTrans.setValue(objTrans.DocId , forKey: "DocId")
        dicTrans.setValue(objTrans.CreateTime , forKey: "CreateTime")
        dicTrans.setValue(objTrans.SendTime , forKey: "SendTime")
        dicTrans.setValue(objTrans.RespondTime , forKey: "RespondTime")
        dicTrans.setValue(objTrans.Flag , forKey: "Flag")
        dicTrans.setValue(objTrans.Status , forKey: "Status")
        dicTrans.setValue(objTrans.DocSendId , forKey: "DocSendId")
        dicTrans.setValue(objTrans.CreatedBy , forKey: "CreatedBy")
        dicTrans.setValue(objTrans.LastModified , forKey: "LastModified")
        dicTrans.setValue(objTrans.BuyerId , forKey: "BuyerId")
        
        return dicTrans
    }
    
    func convertDocLinesDetailsToDictionay(objLine : DocLines) -> NSDictionary
    {
        let dicDoc: NSMutableDictionary = NSMutableDictionary()
        
        dicDoc.setValue(objLine.LineId, forKey: "LineId")
        dicDoc.setValue(objLine.OrderId, forKey: "OrderId")
        dicDoc.setValue(objLine.DocId, forKey: "DocId")
        dicDoc.setValue(objLine.DocTypeId , forKey: "DocTypeId")
        dicDoc.setValue(objLine.ProdTypeId , forKey: "ProdTypeId")
        dicDoc.setValue(objLine.VarietyId , forKey: "VarietyId")
        dicDoc.setValue(objLine.SupplierRef , forKey: "SupplierRef")
        dicDoc.setValue(objLine.DeliveryRef , forKey: "DeliveryRef")
        dicDoc.setValue(objLine.FreeformDesc , forKey: "FreeformDesc")
        dicDoc.setValue(objLine.CountSize , forKey: "CountSize")
        dicDoc.setValue(objLine.Grade , forKey: "Grade")
        dicDoc.setValue(objLine.Colour , forKey: "Colour")
        dicDoc.setValue(objLine.SellCtnQty , forKey: "SellCtnQty")
        dicDoc.setValue(objLine.SellCtnCid , forKey: "SellCtnCid")
        dicDoc.setValue(objLine.SellUnitQty , forKey: "SellUnitQty")
        dicDoc.setValue(objLine.SellUnitCid , forKey: "SellUnitCid")
        dicDoc.setValue(objLine.SellPrice , forKey: "SellPrice")
        dicDoc.setValue(objLine.SellPriceCid , forKey: "SellPriceCid")
        dicDoc.setValue(objLine.BuyCtnQty , forKey: "BuyCtnQty")
        dicDoc.setValue(objLine.BuyCtnCid , forKey: "BuyCtnCid")
        dicDoc.setValue(objLine.BuyUnitQty , forKey: "BuyUnitQty")
        dicDoc.setValue(objLine.BuyUnitCid , forKey: "BuyUnitCid")
        dicDoc.setValue(objLine.BuyPrice , forKey: "BuyPrice")
        dicDoc.setValue(objLine.BuyPriceCid , forKey: "BuyPriceCid")
        dicDoc.setValue(objLine.SupplierID , forKey: "SupplierID")
        dicDoc.setValue(objLine.EntityReference , forKey: "EntityReference")
        dicDoc.setValue(objLine.PackDate , forKey: "PackDate")
        dicDoc.setValue(objLine.UseByDate , forKey: "UseByDate")
        dicDoc.setValue(objLine.GrowerId , forKey: "GrowerId")
        dicDoc.setValue(objLine.IndustryRegionId , forKey: "IndustryRegionId")
        dicDoc.setValue(objLine.Status , forKey: "Status")
        dicDoc.setValue(objLine.GTIN , forKey: "GTIN")
        dicDoc.setValue(objLine.TI , forKey: "TI")
        dicDoc.setValue(objLine.HI , forKey: "HI")
        dicDoc.setValue(objLine.PalletQty , forKey: "PalletQty")
        dicDoc.setValue(objLine.ParentDocLineId , forKey: "ParentDocLineId")
        dicDoc.setValue(objLine.CreatedBy , forKey: "CreatedBy")
        dicDoc.setValue(objLine.LastModified , forKey: "LastModified")
        dicDoc.setValue(objLine.OwnerId , forKey: "OwnerId")
        dicDoc.setValue(objLine.IsActive, forKey: "IsActive")
        
        dicDoc.setValue(objLine.FreeformBuyCtn, forKey: "FreeformBuyCtn")
        dicDoc.setValue(objLine.FreeformBuyUnit, forKey: "FreeformBuyUnit")
        dicDoc.setValue(objLine.FreeformPriceUnit, forKey: "FreeformPriceUnit")
        
        return dicDoc
    }
}
