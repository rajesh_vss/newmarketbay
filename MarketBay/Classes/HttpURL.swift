//
//  HttpURL.swift
//  MarketBay
//
//  Created by Apple on 19/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

public class HttpURL: NSObject
{
    public var pageCount = 2500
    
//    public var strDomain = "http://192.168.0.65:801/webapi/json/reply/"
    
    public var strDomain = "http://192.168.0.72:8014/webapi/json/reply/"
    
//    public var strDomain = "http://www.marketbay.com.au/webapi/json/reply/"
    
    public var strValidate = "ValidateUser"
    
    public var strGetUserInRoles = "GetUserInRoles"
    
    public var strGetEntityType = "GetEntityType"
    
    public var strGetEntity = "GetEntity"
    
    public var strGetProductType = "GetProductType"
    
    public var strGetContainers = "GetContainers"
    
    public var strGetProductVarieties = "GetProductVarieties"
    
    public var strGetTransPost = "GetTransPost"
    
    public var strGetDocLines = "GetDocLines"
    
    public var strSaveDocument = "SaveDocument"
    
    public var strGetDocTypes = "GetDocTypes"
    
    public var strGetProdSizeCount = "GetProdSizeCount"
    
    public var strGetSystemMessage = "GetSystemMessage"
    
    
//    public var strSignalR = "http://192.168.0.65:801/"
    
    public var strSignalR = "http://192.168.0.72:8014"
    
//    public var strSignalR = "http://www.marketbay.com.au/"
    
    public var strMessageHub = "MessageHub"
    
    public var strReceiveMessage = "ReceiveMessage"
    
    public var strSubscribeUser = "SubscribeUser"
    
    public var strUnSubscribeUser = "UnSubscribeUser"
    
    public var strSetDelivered = "SetDelivered"
    
    public var strSendBuyerRequestLog = "SendBuyerRequestLog"
}
