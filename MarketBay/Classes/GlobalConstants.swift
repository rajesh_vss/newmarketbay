//
//  GlobalConstants.swift
//  MarketBay
//
//  Created by Apple on 19/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class GlobalConstants: NSObject
{
    static let LastEntityTypeSyncDateTime = "LastEntityTypeSyncDateTime" // PrimaryKey : 1
    
    static let LastEntitiesSyncDateTime = "LastEntitiesSyncDateTime" // PrimaryKey : 2
    
    static let LastProductTypeSyncDateTime = "LastProductTypeSyncDateTime" // PrimaryKey : 3
    
    static let LastProductVarietiesSyncDateTime = "LastProductVarietiesSyncDateTime" // PrimaryKey : 4
    
    static let LastContainersSyncDateTime = "LastContainersSyncDateTime" // PrimaryKey : 5
    
    static let LastDocTypeSyncDateTime = "LastDocTypeSyncDateTime" // PrimaryKey : 6
    
    static let LastDocLinesSyncDateTime = "LastDocLinesSyncDateTime" // PrimaryKey : 7
    
    static let LastTransPostSyncDateTime = "LastTransPostSyncDateTime" // PrimaryKey : 8
    
    static let LastProdSizeCountSyncDateTime = "LastProdSizeCountSyncDateTime" // PrimaryKey : 9
    
    static let LastSystemMessageSyncDateTime = "LastSystemMessageSyncDateTime" // PrimaryKey : 9
    
    
    static let serverDateFormat:String = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    
    static let localDBDateFormat:String = "yyyy-MM-dd HH:mm:ss.SSS";
    
    static let localAppDateFormat:String = "dd-MM-yyyy";
    
    static let localAppTimeFormat:String = "hh:mm a";
    
    static let barButtonAttribute = [NSFontAttributeName: UIFont(name: "Ubuntu", size: 14)!]
    
    static let progressTitleColor = UIColor(red: 67.0/255.0, green: 162.0/255.0, blue: 216.0/255.0, alpha:1.0)
    
    static let NavigationBar = UIColor(red: 67.0/255.0, green: 162.0/255.0, blue: 216.0/255.0, alpha:1.0)
}
