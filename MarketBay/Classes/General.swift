//
//  General.swift
//  MarketBay
//
//  Created by Apple on 19/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class General: NSObject
{
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func dateFromJSON(string: NSString) -> NSDate
    {
        let serverDateFormat:String = "yyyy-MM-dd'T'HH:mm:ss.SSS";
        
        let dateFormat: NSDateFormatter = NSDateFormatter()
        dateFormat.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        dateFormat.dateFormat = serverDateFormat
        
        if ((dateFormat.dateFromString((string as String))) != nil )
        {
            let date: NSDate = dateFormat.dateFromString(string as String)!
            return   date
        }
        else
        {
            return NSDate()
        }
    }
    
    func ConvertDateFromService2LocalDB(dateFromService: String) -> String
    {
        let serDateFormat: NSDateFormatter = NSDateFormatter()
        serDateFormat.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        serDateFormat.dateFormat = GlobalConstants.serverDateFormat
        
        if ((serDateFormat.dateFromString((dateFromService as String))) != nil)
        {
            let serDate: NSDate = serDateFormat.dateFromString(dateFromService as String)!
            
            serDateFormat.dateFormat = GlobalConstants.localDBDateFormat
            
            return   serDateFormat.stringFromDate(serDate);
        }
        else
        {
            return dateFromService
        }
    }
    
    func ConvertDateFromLocalDB2App(dateFromLocalDB:String) -> String
    {
        
        let localDateFormat: NSDateFormatter = NSDateFormatter()
        localDateFormat.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        localDateFormat.dateFormat = GlobalConstants.localDBDateFormat
        
        if ((localDateFormat.dateFromString((dateFromLocalDB as String))) != nil )
        {
            let serDate: NSDate = localDateFormat.dateFromString(dateFromLocalDB as String)!
            
            localDateFormat.dateFormat = GlobalConstants.serverDateFormat
            
            return   localDateFormat.stringFromDate(serDate);
        }
        else
        {
            return dateFromLocalDB
        }
    }
    
    func generateCurrentServerTime() -> NSDate
    {
        let loginDateTime : NSDate = NSUserDefaults.standardUserDefaults().objectForKey("loginDateTime") as! NSDate
        
        let currentLocalTime : NSDate = NSDate()
        
        let timediff = currentLocalTime.timeIntervalSinceDate(loginDateTime)
        
        let dateFormat: NSDateFormatter = NSDateFormatter()
        dateFormat.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        
        let serverDateTime : NSDate = NSUserDefaults.standardUserDefaults().objectForKey("serverDateTime") as! NSDate
        
        return serverDateTime.dateByAddingTimeInterval(timediff) //currentServerTime
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(cString.startIndex.advancedBy(1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.grayColor()
        }
        
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
