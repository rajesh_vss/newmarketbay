//
//  Parser.swift
//  MarketBay
//
//  Created by Apple on 19/08/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class Parser: NSObject
{
    let general: General = General ()
    
    func parseDatatoUserRole(dataArr: NSArray) -> [UserRole]
    {
        var arrSeason:[UserRole] = [UserRole]()
        
        for index in 0 ..< dataArr.count
        {
            let entities = UserRole()
            let data = dataArr[index] as! NSDictionary
            
            entities.EntityId = data.valueForKey("EntityId") != nil ? data.valueForKey("EntityId") as! Int : 0
            entities.RoleName = data.valueForKey("RoleName") != nil ? data.valueForKey("RoleName") as! String : ""
            entities.RoleId = data.valueForKey("RoleId") != nil ? String(data.valueForKey("RoleId") as! String) : ""
            entities.LicenceExpiredDate = data.valueForKey("LicenceExpiredDate") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("LicenceExpiredDate") as! String))  : ""
            arrSeason.append(entities)
        }
        
        return arrSeason;
    }
    
    func parseDatatoEntityType(dataArr: NSArray) -> [EntityType]
    {
        var arrSeason:[EntityType] = [EntityType]()
        
        for index in 0 ..< dataArr.count
        {
            let entityType = EntityType()
            let data = dataArr[index] as! NSDictionary
            
            entityType.EntityTypeId = data.valueForKey("EntityTypeId") as! Int
            entityType.EntityType1 = String(data.valueForKey("EntityType1") as! String)
            
            arrSeason.append(entityType)
        }
        
        return arrSeason;
    }
    
    func parseDatatoEntities(dataArr: NSArray) -> [Entities]
    {
        var arrSeason:[Entities] = [Entities]()
        
        for index in 0 ..< dataArr.count
        {
            let entities = Entities()
            let data = dataArr[index] as! NSDictionary
            
            entities.ClientId = data.valueForKey("ClientId") != nil ? data.valueForKey("ClientId") as! Int : 0
            entities.EntityId = data.valueForKey("EntityId") != nil ? data.valueForKey("EntityId") as! Int : 0
            entities.EntityTypeID = data.valueForKey("EntityTypeID") != nil ? data.valueForKey("EntityTypeID") as! Int : 0
            entities.EntityType = data.valueForKey("EntityType") != nil ? String(data.valueForKey("EntityType") as! String) : ""
            entities.Name1 = data.valueForKey("Name1") != nil ? String(data.valueForKey("Name1") as! String) : ""
            entities.Name2 = data.valueForKey("Name2") != nil ? String(data.valueForKey("Name2") as! String) : ""
            entities.MarketCode = data.valueForKey("MarketCode") != nil ? String(data.valueForKey("MarketCode") as! String) : ""
            entities.IsActive = data.valueForKey("IsActive") != nil ? String(data.valueForKey("IsActive") as! Bool) : "false"
            entities.LastModified = data.valueForKey("LastModified") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("LastModified") as! String))  : ""
            entities.RepId = data.valueForKey("RepId") != nil ? data.valueForKey("RepId") as! Int : 0
            entities.RebatePercentage =  data.valueForKey("RebatePercentage") != nil ? data.valueForKey("RebatePercentage") as! Double : 0.0
            entities.HandlingFeePer = data.valueForKey("HandlingFeePer") != nil ? data.valueForKey("HandlingFeePer") as! Double : 0.0
            entities.Representative = data.valueForKey("Representative") != nil ? data.valueForKey("Representative") as! Int : 0
            entities.ClientEntity = data.valueForKey("ClientEntity") != nil ? data.valueForKey("ClientEntity") as! Int : 0
            arrSeason.append(entities)
        }
        
        return arrSeason;
    }
    
    func parseDatatoProdutType(dataArr: NSArray) -> [ProductType]
    {
        var arrSeason:[ProductType] = [ProductType]()
        
        for index in 0 ..< dataArr.count
        {
            let produtType = ProductType()
            let data = dataArr[index] as! NSDictionary
            
            produtType.ProdTypeId = data.valueForKey("ProdTypeId") as! Int
            produtType.ProdTypeDesc = String(data.valueForKey("ProdTypeDesc") as! String)
            produtType.ProdTypeShortName = data.valueForKey("ProdTypeShortName") != nil ? data.valueForKey("ProdTypeShortName") as! String  : ""
            produtType.ClassId = data.valueForKey("ClassId") as! Int
            produtType.LastModified = data.valueForKey("LastModified") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("LastModified") as! String))  : ""
            produtType.CreatedBy = data.valueForKey("CreatedBy") != nil ? String(data.valueForKey("CreatedBy") as! String) : ""
            
            arrSeason.append(produtType)
        }
        
        
        return arrSeason;
    }
    
    func parseDatatoProductVarieties(dataArr: NSArray) -> [ProductVarieties]
    {
        var arrSeason:[ProductVarieties] = [ProductVarieties]()
        
        for index in 0 ..< dataArr.count
        {
            let produtVerity = ProductVarieties()
            let data = dataArr[index] as! NSDictionary
            
            produtVerity.VarietyId = data.valueForKey("VarietyId") as! Int
            produtVerity.VarietyDesc =  data.valueForKey("VarietyDesc") != nil ? data.valueForKey("VarietyDesc") as! String  : ""
            produtVerity.ProdTypeId =  data.valueForKey("ProdTypeId") != nil ? data.valueForKey("ProdTypeId") as! Int  : 0
            produtVerity.EntityId =  data.valueForKey("EntityId") != nil ? data.valueForKey("EntityId") as! Int  : 0
            produtVerity.EntityReference =  data.valueForKey("EntityReference") != nil ? data.valueForKey("EntityReference") as! String  : ""
            produtVerity.EntityGroup =  data.valueForKey("EntityGroup") != nil ? data.valueForKey("EntityGroup") as! String  : ""
            produtVerity.CtnCid =  data.valueForKey("CtnCid") != nil ? data.valueForKey("CtnCid") as! Int  : 0
            produtVerity.UnitQty =  data.valueForKey("UnitQty") != nil ? data.valueForKey("UnitQty") as! Double  : 0.0
            produtVerity.UnitCid =  data.valueForKey("UnitCid") != nil ? data.valueForKey("UnitCid") as! Int  : 0
            produtVerity.TI =  data.valueForKey("TI") != nil ? data.valueForKey("TI") as! Int  : 0
            produtVerity.HI =  data.valueForKey("HI") != nil ? data.valueForKey("HI") as! Int  : 0
            produtVerity.PalletQty =  data.valueForKey("PalletQty") != nil ? data.valueForKey("PalletQty") as! Int : 0
            produtVerity.GTIN =  data.valueForKey("GTIN") != nil ? data.valueForKey("GTIN") as! String  : ""
            produtVerity.PackDate_Days =  data.valueForKey("PackDate_Days") != nil ? data.valueForKey("PackDate_Days") as! String  : ""
            produtVerity.UseByDate_Days =  data.valueForKey("UseByDate_Days") != nil ? data.valueForKey("UseByDate_Days") as! String  : ""
            produtVerity.ProdStandardId =  data.valueForKey("ProdStandardId") != nil ? data.valueForKey("ProdStandardId") as! Int  : 0
            produtVerity.CreatedBy =  data.valueForKey("CreatedBy") != nil ? data.valueForKey("CreatedBy") as! String  : ""
            produtVerity.LastModified = data.valueForKey("LastModified") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("LastModified") as! String))  : ""
            produtVerity.ProdStandardCode =  data.valueForKey("ProdStandardCode") != nil ? data.valueForKey("ProdStandardCode") as! String  : ""
            
            arrSeason.append(produtVerity)
        }
        
        
        return arrSeason;
    }
    
    func parseDatatoContainers(dataArr: NSArray) -> [Containers]
    {
        var arrSeason:[Containers] = [Containers]()
        
        for index in 0 ..< dataArr.count
        {
            let objCont = Containers()
            let data = dataArr[index] as! NSDictionary
            
            objCont.Cid = data.valueForKey("Cid") as! Int
            objCont.CidDesc = data.valueForKey("CidDesc") != nil ? data.valueForKey("CidDesc") as! String  : ""
            objCont.Deposit = data.valueForKey("Deposit") != nil ? data.valueForKey("Deposit") as! Double  : 0.0
            objCont.CreatedBy = data.valueForKey("CreatedBy") != nil ? data.valueForKey("CreatedBy") as! String  : ""
            objCont.LastModified = data.valueForKey("LastModified") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("LastModified") as! String))  : ""
            
            arrSeason.append(objCont)
        }
        
        return arrSeason;
    }
    
    func parseDatatoDocType(dataArr: NSArray) -> [DocType]
    {
        var arrSeason:[DocType] = [DocType]()
        
        for index in 0 ..< dataArr.count
        {
            let objCont = DocType()
            let data = dataArr[index] as! NSDictionary
            
            objCont.DocTypeId = data.valueForKey("DocTypeId") as! Int
            objCont.DocTypeDesc = data.valueForKey("DocTypeDesc") != nil ? data.valueForKey("DocTypeDesc") as! String  : ""
            objCont.XML_DocType = data.valueForKey("XML_DocType") != nil ? data.valueForKey("XML_DocType") as! String  : ""
            objCont.XSL_Stylesheet = data.valueForKey("XSL_Stylesheet") != nil ? data.valueForKey("XSL_Stylesheet") as! String  : ""
            objCont.isActive = data.valueForKey("isActive") != nil ? data.valueForKey("isActive") as! Bool  : false
            objCont.IsCheckbox = data.valueForKey("IsCheckbox") != nil ? data.valueForKey("IsCheckbox") as! Bool  : false
            objCont.LastModified = data.valueForKey("LastModified") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("LastModified") as! String))  : ""
            
            arrSeason.append(objCont)
        }
        
        return arrSeason;
    }
    
    func parseDatatoDocLines(dataArr: NSArray) -> [DocLines]
    {
        var arrSeason:[DocLines] = [DocLines]()
        
        for index in 0 ..< dataArr.count
        {
            let objline = DocLines()
            let data = dataArr[index] as! NSDictionary
            
            objline.LineId = data.valueForKey("LineId") as! Int
            objline.DocId = data.valueForKey("DocId") as! Int
            objline.DocTypeId = data.valueForKey("DocTypeId") != nil ? data.valueForKey("DocTypeId") as! Int  : 0
            objline.ProdTypeId = data.valueForKey("ProdTypeId") != nil ? data.valueForKey("ProdTypeId") as! Int  : 0
            objline.VarietyId = data.valueForKey("VarietyId") != nil ? data.valueForKey("VarietyId") as! Int  : 0
            objline.SupplierRef = data.valueForKey("SupplierRef") != nil ? data.valueForKey("SupplierRef") as! String  : ""
            objline.DeliveryRef = data.valueForKey("DeliveryRef") != nil ? String(data.valueForKey("DeliveryRef") as! Int) : ""
            objline.FreeformDesc = data.valueForKey("FreeformDesc") != nil ? data.valueForKey("FreeformDesc") as! String  : ""
            objline.CountSize = data.valueForKey("CountSize") != nil ? data.valueForKey("CountSize") as! Int  : 0
            objline.Grade = data.valueForKey("Grade") != nil ? data.valueForKey("Grade") as! String  : ""
            objline.Colour = data.valueForKey("Colour") != nil ? data.valueForKey("Colour") as! String  : ""
            objline.BuyCtnQty = data.valueForKey("BuyCtnQty") != nil ? data.valueForKey("BuyCtnQty") as! Int  : 0
            objline.BuyCtnCid = data.valueForKey("BuyCtnCid") != nil ? data.valueForKey("BuyCtnCid") as! Int  : 0
            objline.BuyUnitQty = data.valueForKey("BuyUnitQty") != nil ? data.valueForKey("BuyUnitQty") as! Int  : 0
            objline.BuyUnitCid = data.valueForKey("BuyUnitCid") != nil ? data.valueForKey("BuyUnitCid") as! Int  : 0
            objline.SellPrice = data.valueForKey("SellPrice") != nil ? data.valueForKey("SellPrice") as! Int  : 0
            objline.SellPriceCid = data.valueForKey("SellPriceCid") != nil ? data.valueForKey("SellPriceCid") as! Int  : 0
            objline.SupplierID = data.valueForKey("SupplierID") != nil ? data.valueForKey("SupplierID") as! Int  : 0
            objline.EntityReference = data.valueForKey("EntityReference") != nil ? data.valueForKey("EntityReference") as! String  : ""
            objline.BuyPrice = data.valueForKey("BuyPrice") != nil ? data.valueForKey("BuyPrice") as! Double  : 0
            objline.BuyPriceCid = data.valueForKey("BuyPriceCid") != nil ? data.valueForKey("BuyPriceCid") as! Int  : 0
            objline.SellCtnQty = data.valueForKey("SellCtnQty") != nil ? data.valueForKey("SellCtnQty") as! Int  : 0
            objline.SellCtnCid = data.valueForKey("SellCtnCid") != nil ? data.valueForKey("SellCtnCid") as! Int  : 0
            objline.SellUnitQty = data.valueForKey("SellUnitQty") != nil ? data.valueForKey("SellUnitQty") as! Int  : 0
            objline.SellUnitCid = data.valueForKey("SellUnitCid") != nil ? data.valueForKey("SellUnitCid") as! Int  : 0
            objline.PackDate = data.valueForKey("PackDate") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("PackDate") as! String))  : ""
            objline.UseByDate = data.valueForKey("UseByDate") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("UseByDate") as! String))  : ""
            objline.GrowerId = data.valueForKey("GrowerId") != nil ? data.valueForKey("GrowerId") as! Int  : 0
            objline.IndustryRegionId = data.valueForKey("IndustryRegionId") != nil ? data.valueForKey("IndustryRegionId") as! Int  : 0
            objline.Status = data.valueForKey("Status") != nil ? data.valueForKey("Status") as! Bool  : false
            objline.GTIN = data.valueForKey("GTIN") != nil ? data.valueForKey("GTIN") as! String  : ""
            objline.TI = data.valueForKey("TI") != nil ? data.valueForKey("TI") as! Int  : 0
            objline.HI = data.valueForKey("HI") != nil ? data.valueForKey("HI") as! Int  : 0
            objline.PalletQty = data.valueForKey("PalletQty") != nil ? data.valueForKey("PalletQty") as! Int  : 0
            objline.ParentDocLineId = data.valueForKey("ParentDocLineId") != nil ? data.valueForKey("ParentDocLineId") as! Int  : 0
            objline.CreatedBy = data.valueForKey("CreatedBy") != nil ? data.valueForKey("CreatedBy") as! String  : ""
            objline.LastModified = data.valueForKey("LastModified") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("LastModified") as! String))  : ""
            objline.OwnerId = data.valueForKey("OwnerId") != nil ? data.valueForKey("OwnerId") as! Int  : 0
            
            
            objline.OrderId = data.valueForKey("OrderId") != nil ? data.valueForKey("OrderId") as! Int  : 0
            objline.FreeformBuyCtn = data.valueForKey("FreeformBuyCtn") != nil ? data.valueForKey("FreeformBuyCtn") as! String  : ""
            objline.FreeformBuyUnit = data.valueForKey("FreeformBuyUnit") != nil ? data.valueForKey("FreeformBuyUnit") as! String  : ""
            objline.FreeformPriceUnit = data.valueForKey("FreeformPriceUnit") != nil ? data.valueForKey("FreeformPriceUnit") as! String  : ""
            
            arrSeason.append(objline)
        }
        
        
        return arrSeason;
    }
    
    func parseDatatoTransPost(dataArr: NSArray) -> [TransPost]
    {
        var arrSeason:[TransPost] = [TransPost]()
        
        for index in 0 ..< dataArr.count
        {
            let objPost = TransPost()
            let data = dataArr[index] as! NSDictionary
            
            objPost.TransPostId = data.valueForKey("TransPostId") != nil ? data.valueForKey("TransPostId") as! Int  : 0
            objPost.SupplierId = data.valueForKey("SupplierId") != nil ? data.valueForKey("SupplierId") as! Int  : 0
            objPost.LineId = data.valueForKey("LineId") != nil ? data.valueForKey("LineId") as! Int  : 0
            objPost.DocTypeId = data.valueForKey("DocTypeId") != nil ? data.valueForKey("DocTypeId") as! Int  : 0
            objPost.DocId = data.valueForKey("DocId") != nil ? data.valueForKey("DocId") as! Int  : 0
            objPost.CreateTime = data.valueForKey("CreateTime") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("CreateTime") as! String))  : ""
            objPost.SendTime = data.valueForKey("SendTime") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("SendTime") as! String))  : ""
            objPost.RespondTime = data.valueForKey("RespondTime") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("RespondTime") as! String))  : ""
            objPost.Flag = data.valueForKey("Flag") != nil ? data.valueForKey("Flag") as! String  : ""
            objPost.Status = data.valueForKey("Status") != nil ? data.valueForKey("Status") as! Bool  : false
            objPost.CreatedBy = data.valueForKey("CreatedBy") != nil ? data.valueForKey("CreatedBy") as! String  : ""
            objPost.LastModified = data.valueForKey("LastModified") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("LastModified") as! String))  : ""
            objPost.BuyerId = data.valueForKey("BuyerId") != nil ? data.valueForKey("BuyerId") as! Int  : 0
            objPost.DocSendLastStatus = data.valueForKey("DocSendLastStatus") != nil ? data.valueForKey("DocSendLastStatus") as! String  : ""
           
            objPost.isSendToSupplier = objPost.DocSendLastStatus == "C" || objPost.DocSendLastStatus == "S" || objPost.DocSendLastStatus == "E" ? 1 : -1
            
            objPost.isPublished = objPost.isSendToSupplier == 1 ? 1 : (objPost.DocSendLastStatus == "P" || objPost.DocSendLastStatus == "T" ? 1 : -1)
            
            objPost.isUpdatedToOnline = 0
            
            arrSeason.append(objPost)
        }
        
        return arrSeason;
    }
    
    func parseDatatoProdSizeCount(dataArr: NSArray) -> [ProdSizeCount]
    {
        var arrSeason:[ProdSizeCount] = [ProdSizeCount]()
        
        for index in 0 ..< dataArr.count
        {
            let objSize = ProdSizeCount()
            let data = dataArr[index] as! NSDictionary
            
            objSize.ProdSizeCountId = data.valueForKey("ProdSizeCountId") != nil ? data.valueForKey("ProdSizeCountId") as! String  : ""
            objSize.ProdTypeId = data.valueForKey("ProdTypeId") != nil ? data.valueForKey("ProdTypeId") as! Int  : 0
            objSize.SizeCount = data.valueForKey("SizeCount") != nil ? data.valueForKey("SizeCount") as! Int  : 0
            objSize.IsActive = data.valueForKey("IsActive") != nil ? data.valueForKey("IsActive") as! Bool  : false
            objSize.Cid = data.valueForKey("Cid") != nil ? data.valueForKey("Cid") as! Int  : 0
            objSize.ContainerCode = data.valueForKey("ContainerCode") != nil ? data.valueForKey("ContainerCode") as! String  : ""
            objSize.CreatedBy = data.valueForKey("CreatedBy") != nil ? data.valueForKey("CreatedBy") as! String  : ""
            objSize.LastModified = data.valueForKey("LastModified") != nil ? general.ConvertDateFromService2LocalDB((data.valueForKey("LastModified") as! String))  : ""
 
            arrSeason.append(objSize)
        }
        
        return arrSeason;
    }
    
    func parseDatatoSystemMessage(dataArr: NSArray) -> [SystemMessage]
    {
        var arrMessage : [SystemMessage] = [SystemMessage]()
        
        for index in 0 ..< dataArr.count
        {
            let smDetails = SystemMessage()
            
            let data = dataArr[index] as! NSDictionary
            
            smDetails.MessageNo = data.valueForKey("MessageNo") as! Int
            
            smDetails.SystemMessageType = data.valueForKey("SystemMessageType") != nil ? (data.valueForKey("SystemMessageType") as! String) : ""
            
            smDetails.SystemMessageDetails = data.valueForKey("SystemMessageDetails") != nil ? (data.valueForKey("SystemMessageDetails") as! String) + " [" + String(smDetails.MessageNo) + "]" : ""
            
            smDetails.Selection1 = data.valueForKey("Selection1") != nil ? (data.valueForKey("Selection1") as! String) : ""
            
            smDetails.Selection2 = data.valueForKey("Selection2") != nil ? (data.valueForKey("Selection2") as! String) : ""
            
            smDetails.Selection3 = data.valueForKey("Selection3") != nil ? (data.valueForKey("Selection3") as! String) : ""
            
            arrMessage.append(smDetails)
            
        }
        
        return arrMessage
    }
}
