//
//  ConvertClassToClass.swift
//  MarketBay
//
//  Created by Apple on 09/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class ConvertClassToClass: NSObject
{
    static func convertDocLineToDocLine(objDL : DocLines) -> DocLines
    {
        let newDL : DocLines = DocLines()
        
        newDL.LineId	= objDL.LineId
        newDL.DocId	= objDL.DocId
        newDL.DocTypeId	= objDL.DocTypeId
        newDL.ProdTypeId	= objDL.ProdTypeId
        newDL.VarietyId	= objDL.VarietyId
        newDL.SupplierRef	= objDL.SupplierRef
        newDL.DeliveryRef	= objDL.DeliveryRef
        newDL.FreeformDesc	= objDL.FreeformDesc
        newDL.CountSize	= objDL.CountSize
        newDL.Grade	= objDL.Grade
        newDL.Colour	= objDL.Colour
        newDL.BuyCtnQty	= objDL.BuyCtnQty
        newDL.BuyCtnCid	= objDL.BuyCtnCid
        newDL.BuyUnitQty	= objDL.BuyUnitQty
        newDL.BuyUnitCid	= objDL.BuyUnitCid
        newDL.SellPrice	= objDL.SellPrice
        newDL.SellPriceCid	= objDL.SellPriceCid
        newDL.SupplierID	= objDL.SupplierID
        newDL.EntityReference	= objDL.EntityReference
        newDL.BuyPrice	= objDL.BuyPrice
        newDL.BuyPriceCid	= objDL.BuyPriceCid
        newDL.SellCtnQty	= objDL.SellCtnQty
        newDL.SellCtnCid	= objDL.SellCtnCid
        newDL.SellUnitQty	= objDL.SellUnitQty
        newDL.SellUnitCid	= objDL.SellUnitCid
        newDL.PackDate	= objDL.PackDate
        newDL.UseByDate	= objDL.UseByDate
        newDL.GrowerId	= objDL.GrowerId
        newDL.IndustryRegionId	= objDL.IndustryRegionId
        newDL.Status	= objDL.Status
        newDL.GTIN	= objDL.GTIN
        newDL.TI	= objDL.TI
        newDL.HI	= objDL.HI
        newDL.PalletQty	= objDL.PalletQty
        newDL.ParentDocLineId	= objDL.ParentDocLineId
        newDL.CreatedBy	= objDL.CreatedBy
        newDL.LastModified	= objDL.LastModified
        newDL.OwnerId	= objDL.OwnerId
        
        newDL.OrderId	= objDL.OrderId
        newDL.FreeformPriceUnit	= objDL.FreeformPriceUnit
        newDL.FreeformBuyUnit	= objDL.FreeformBuyUnit
        newDL.FreeformBuyCtn	= objDL.FreeformBuyCtn
         
        return newDL
    }
}
